// Copyright 2022 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"archive/zip"
	"bytes"
	"context"
	"crypto/ed25519"
	"crypto/sha256"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"testing/fstest"
	"time"

	"filippo.io/age"
	"github.com/stretchr/testify/assert"
	"github.com/u-root/u-root/pkg/efivarfs"
	"system-transparency.org/stboot/host"
	"system-transparency.org/stboot/host/network"
	di "system-transparency.org/stboot/internal/dependency"
	"system-transparency.org/stboot/internal/test"
	"system-transparency.org/stboot/ospkg"
	"system-transparency.org/stboot/stlog"
	"system-transparency.org/stboot/trust"
)

func TestFetchOspkgNetwork(t *testing.T) {
	var desc = ospkg.Descriptor{
		Version:      1,
		PkgURL:       "test.zip",
		Certificates: [][]byte{{}},
		Signatures:   [][]byte{{}},
	}

	if !testing.Verbose() {
		stlog.SetLevel(stlog.ErrorLevel)
	}

	archive := dummyZip(t)

	t.Run("plain", func(t *testing.T) {
		muxPlain := http.NewServeMux()
		muxPlain.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			err := json.NewEncoder(w).Encode(desc)
			assert.NoError(t, err)
		})
		muxPlain.HandleFunc("/test.zip", func(w http.ResponseWriter, r *http.Request) {
			_, err := w.Write(archive)
			assert.NoError(t, err)
		})
		testFetchOspkgNetwork(t, desc, muxPlain, archive, nil)
	})

	t.Run("encrypted", func(t *testing.T) {
		id, err := age.GenerateX25519Identity()
		assert.NoError(t, err)
		muxEncrypt := http.NewServeMux()
		muxEncrypt.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			wc, err := age.Encrypt(w, id.Recipient())
			assert.NoError(t, err)
			err = json.NewEncoder(wc).Encode(desc)
			assert.NoError(t, err)
			err = wc.Close()
			assert.NoError(t, err)
		})
		muxEncrypt.HandleFunc("/test.zip", func(w http.ResponseWriter, r *http.Request) {
			wc, err := age.Encrypt(w, id.Recipient())
			assert.NoError(t, err)
			_, err = wc.Write(archive)
			assert.NoError(t, err)
			err = wc.Close()
			assert.NoError(t, err)
		})
		testFetchOspkgNetwork(t, desc, muxEncrypt, archive, []age.Identity{id})
	})
}

func testFetchOspkgNetwork(t *testing.T, desc ospkg.Descriptor, mux *http.ServeMux, archive []byte, ids []age.Identity) {
	t.Helper()

	// Setup httptest server
	svr := httptest.NewServer(mux)
	defer svr.Close()

	// Initialize client
	var roots []*x509.Certificate
	client := network.NewHTTPClient(roots, false, network.WithDecryption(ids))

	osPkgPtr := svr.URL
	cfg := &host.Config{OSPkgPointer: &osPkgPtr}

	osp, err := fetchOspkgNetwork(context.Background(), client, cfg)
	if err != nil {
		t.Fatal(err)
	}

	if got := *osp.Descriptor(); !reflect.DeepEqual(got, desc) {
		t.Errorf("got %+v, want %+v", got, desc)
	}

	got, err := osp.ArchiveBytes()
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(got, archive) {
		t.Errorf("got %v, want %v", got, archive)
	}
}

func TestFetchOspkgInitramfs(t *testing.T) {
	var descriptor = ospkg.Descriptor{
		Version:      1,
		PkgURL:       "test.zip",
		Certificates: [][]byte{{}},
		Signatures:   [][]byte{{}},
	}

	archive := dummyZip(t)

	var (
		dir            = t.TempDir()
		descriptorFile = filepath.Join(dir, "test.json")
		archiveFile    = filepath.Join(dir, "test.zip")
	)

	dBytes, err := json.Marshal(descriptor)
	if err != nil {
		t.Fatal(err)
	}

	err = os.WriteFile(descriptorFile, dBytes, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}

	err = os.WriteFile(archiveFile, archive, os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}

	osPkgPtr := "test.json"
	cfg := &host.Config{OSPkgPointer: &osPkgPtr}

	fsys := di.DefaultFilesystem(context.Background())

	osp, err := _fetchOspkgInitramfs(cfg, dir, fsys)
	if err != nil {
		t.Fatal(err)
	}

	if got := *osp.Descriptor(); !reflect.DeepEqual(got, descriptor) {
		t.Errorf("got %v, want %v", got, descriptor)
	}
	got, err := osp.ArchiveBytes()
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(got, archive) {
		t.Errorf("got %v, want %v", got, archive)
	}
}

func TestOSpkgFiles(t *testing.T) {
	tests := []struct {
		ospkgptr string
		desc     string
		archive  string
	}{
		{
			ospkgptr: "my-ospkg",
			desc:     "my-ospkg.json",
			archive:  "my-ospkg.zip",
		},
		{
			ospkgptr: "my-ospkg.zip",
			desc:     "my-ospkg.json",
			archive:  "my-ospkg.zip",
		},
		{
			ospkgptr: "my-ospkg.json",
			desc:     "my-ospkg.json",
			archive:  "my-ospkg.zip",
		},
		{
			ospkgptr: "some_folder/my-ospkg.zip",
			desc:     "some_folder/my-ospkg.json",
			archive:  "some_folder/my-ospkg.zip",
		},
	}

	for _, tt := range tests {
		t.Run(tt.ospkgptr, func(t *testing.T) {
			cfg := host.Config{
				OSPkgPointer: &tt.ospkgptr,
			}
			desc, archive := ospkgFiles(&cfg)
			if desc != tt.desc {
				t.Errorf("got %s, want %s", desc, tt.desc)
			}
			if archive != tt.archive {
				t.Errorf("got %s, want %s", archive, tt.archive)
			}
		})
	}
}

func mkZip(t *testing.T) []byte {
	t.Helper()

	mockFsys := test.NewMockFS()
	err := mockFsys.Add("/vmlinuz", &fstest.MapFile{Data: []byte("i'm a kernel"), Mode: 0644})
	assert.NoError(t, err)
	err = mockFsys.Add("/initramfs", &fstest.MapFile{Data: []byte("i'm an initial ram fs"), Mode: 0644})
	assert.NoError(t, err)

	ctx := context.Background()
	ctx = di.WithFilesystem(ctx, mockFsys)

	osp, err := ospkg.CreateOSPackageCtx(
		ctx,
		"test-label",
		"https://example.com/ospkg.zip",
		"/vmlinuz",
		"/initramfs",
		"console=ttyS0,115200n8",
	)
	assert.NoError(t, err)

	archiveZip, err := osp.ArchiveBytes()
	assert.NoError(t, err)

	return archiveZip
}

func TestGetImage(t *testing.T) {
	ctx := context.Background()

	root := test.MkKey(t, nil)
	www := test.MkKey(t, nil)
	httpsRoots := []*x509.Certificate{www.Cert}

	key1 := test.MkKey(t, &root)
	key2 := test.MkKey(t, &root)
	key3 := test.MkKey(t, &root)

	ospkgZip := mkZip(t)

	archivehash := sha256.Sum256(ospkgZip)
	desc := ospkg.Descriptor{
		Version: 1,
		PkgURL:  "https://example.com/ospkg.zip",
		Certificates: [][]byte{
			pem.EncodeToMemory(key1.Certpem),
			pem.EncodeToMemory(key2.Certpem),
			pem.EncodeToMemory(key3.Certpem),
		},
		Signatures: [][]byte{
			ed25519.Sign(key1.Private, archivehash[:]),
			ed25519.Sign(key2.Private, archivehash[:]),
			ed25519.Sign(key3.Private, archivehash[:]),
		},
	}
	descJSON, err := json.Marshal(desc)
	assert.NoError(t, err)

	mockFsys := test.NewMockFS()
	trustPolicy := trust.Policy{
		SignatureThreshold: 1,
		FetchMethod:        ospkg.FetchFromInitramfs,
	}
	assert.NoError(t, err)
	err = mockFsys.Add("/ospkg/provision.zip", &fstest.MapFile{Data: ospkgZip})
	assert.NoError(t, err)
	err = mockFsys.Add("/ospkg/provision.json", &fstest.MapFile{Data: descJSON})
	assert.NoError(t, err)

	mockEfiVar := di.NewFakeEfiVars()

	ctx = di.WithFilesystem(ctx, mockFsys)
	ctx = di.WithEFIVar(ctx, mockEfiVar)

	hostCfg := provisionHostConfig()

	t.Run("no UX identity", func(t *testing.T) {
		_, err = getImage(ctx, &trustPolicy, root.Cert, httpsRoots, nil, hostCfg, 20*time.Minute)
		assert.NoError(t, err)
	})

	t.Run("with UX identity", func(t *testing.T) {
		mockEfiVar := di.NewFakeEfiVars()

		err := efivarfs.SimpleWriteVariable(mockEfiVar, "UxIdentity-f401f2c1-b005-4be0-8cee-f2e5945bcbe7", 0, bytes.NewBuffer([]byte("test")))
		assert.NoError(t, err)

		ctx = di.WithEFIVar(ctx, mockEfiVar)

		_, err = getImage(ctx, &trustPolicy, root.Cert, httpsRoots, nil, hostCfg, 20*time.Minute)
		assert.NoError(t, err)
	})

	for _, f := range []string{"/ospkg/provision.zip", "/ospkg/provision.json"} {
		t.Run(fmt.Sprintf("no %s", f), func(t *testing.T) {
			myMockFs, err := mockFsys.Copy()
			assert.NoError(t, err)
			err = myMockFs.Remove(f)
			assert.NoError(t, err)
			ctx = di.WithFilesystem(ctx, myMockFs)

			_, err = getImage(ctx, &trustPolicy, root.Cert, httpsRoots, nil, hostCfg, 20*time.Minute)
			assert.Error(t, err)
		})
	}

	t.Run("optional https roots", func(t *testing.T) {
		ctx = di.WithFilesystem(ctx, mockFsys)

		_, err = getImage(ctx, &trustPolicy, root.Cert, nil, nil, hostCfg, 20*time.Minute)
		assert.NoError(t, err)
	})

	t.Run("missing https roots", func(t *testing.T) {
		myMockFs, err := mockFsys.Copy()
		assert.NoError(t, err)

		ipAddrMode := host.IPDynamic
		osPkgPtr := "https://example.org/ospkg"
		hostCfg := host.Config{
			IPAddrMode:   &ipAddrMode,
			OSPkgPointer: &osPkgPtr,
		}
		ctx = di.WithFilesystem(ctx, myMockFs)

		_, err = getImage(ctx, &trust.Policy{
			SignatureThreshold: 1,
			FetchMethod:        ospkg.FetchFromNetwork,
		}, root.Cert, nil, nil, hostCfg, 20*time.Minute)
		assert.Error(t, err)
	})
	t.Run("invalid signature threshold", func(t *testing.T) {
		ctx = di.WithFilesystem(ctx, mockFsys)

		_, err = getImage(ctx, &trust.Policy{
			SignatureThreshold: 4,
			FetchMethod:        ospkg.FetchFromInitramfs,
		}, root.Cert, httpsRoots, nil, hostCfg, 20*time.Minute)
		assert.Error(t, err)
	})
}

func TestCleanString(t *testing.T) {
	for _, table := range []struct {
		input, want string
	}{
		{"", ""},
		{"foo", "foo"},
		{"foo\tbar\n", "foobar"},
		{"DEL\x7f Ångstrom\u212b bidi\u2067 x", "DEL ngstrom bidi x"},
	} {
		if got, want := cleanString(table.input), table.want; got != want {
			t.Errorf("cleanString failed: in %q, got %q, want: %q",
				table.input, got, want)
		}
	}
}

func dummyZip(t *testing.T) []byte {
	buf := new(bytes.Buffer)
	zipWriter := zip.NewWriter(buf)
	if err := zipWriter.Close(); err != nil {
		t.Fatal(err)
	}
	return buf.Bytes()
}
