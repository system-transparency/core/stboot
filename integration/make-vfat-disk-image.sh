#! /bin/bash

set -eu

die () {
    echo "$@" >&2
    exit 1
}

while [[ $# -gt 0 ]]; do
    if [[ "$1" = "-o" ]] ; then
	shift
	[[ $# -gt 0 ]] || die "Missing argument for -o"
	[[ -z "${output+x}" ]] || die "Can only use -o once"
	output="$1"
    else
	[[ -z "${input+x}" ]] || die "Too many arguments"
	input="$1"
    fi
    shift
done

[[ "${input+x}" ]] || die "Input not specified"
[[ "${output+x}" ]] || die "Output not specified"

size=$(stat -c %s "${input}")
# Add some margin for fs metadata, then round upwards to MiB size.
fs_size_MiB=$(( ( size + 500000 + 1048575 ) / 1048576 ))

# Reserve 1 MiB at the start (for GPT partition table) and 1 MiB at the end
dd if=/dev/zero bs=1024 count=$(((2 + fs_size_MiB) * 1024)) of="${output}"
# Note that parted uses a 512 byte sector size
/sbin/parted -s -a opt -- "${output}" mklabel gpt mkpart EFI fat32 2048s -2048s set 1 boot on

# The filesystem partition starts at offset 1MiB. Note that mkfs.vfat
# wants the offset in units of 512 byte sectors, and the file system
# size (last argument) in units of 1 KiB.
/sbin/mkfs.vfat --offset 2048 "${output}" $(( 1024 * fs_size_MiB )) 2>&1 | \
    sed >&2 's/^\(Warning: block count mismatch:\)/(Expected) \1/'

MTOOLSRC=$(mktemp --tmpdir mtools.XXXXXX.conf)
trap 'rm ${MTOOLSRC}' EXIT

cat > "${MTOOLSRC}" <<EOF
drive a:
  file="${output}"
  offset=1048576
EOF

export MTOOLSRC

mmd a:EFI
mmd a:EFI/BOOT
mcopy "${input}" a:EFI/BOOT/BOOTX64.EFI
