#! /bin/bash

# Script to create a bootable iso image including a provisioning OS
# package, with the network setup of glasklar's test server.

set -eu

# Change directory to where script is located.
cd "$(dirname "$0")"

die() {
    echo "$@" >&2
    exit 1
}

run_qemu=no
stprov_network_args="dhcp"
stprov_url="http://st.bootlab.glasklarteknik.se:8000/debian-package.json"
# See https://git.glasklar.is/glasklar/services/bootlab/-/blob/main/stime.md
stprov_mac="-m 3c:ec:ef:29:60:2b"
CONSOLE=
while [[ $# -gt 0 ]]; do
    case "$1" in
	--qemu)
	    run_qemu=yes
	    stprov_url="http://10.0.2.50:80/debian-package.json"
	    stprov_mac=""
	    CONSOLE='console=ttyS0,115200n8'
	    ;;
	--static)
	    stprov_network_args="static --ip 91.223.231.242/29 --gateway 91.223.231.241 --dns 91.223.231.241"
	    ;;
	--help)
	    echo "Options:"
	    echo "  --qemu    Run qemu, and set kernel console to serial."
	    echo "  --static  Provision to use static network config, suitable for glasklar lab."
	    echo "  --help    Show this help."
	    exit
	    ;;
	*)
	    die "Unknown option: $1, try --help"
	    ;;
    esac
    shift
done

# Use local directory for built go tools.
GOBIN=$(pwd)/bin
export GOBIN

[ -d cache ] || mkdir cache

download() {
    local file=$1
    local url=$2
    local remote_file
    remote_file=$(basename "${url}")
    # Unfortunately, wget -O --timestamping doesn't work, instead,
    # make a symlink to desired name.
    wget --no-verbose --timestamping --directory-prefix=cache "${url}" &&
    ln -srf "cache/${remote_file}" "cache/${file}"
}

# Disable cgo, to get a statically linked executable.
(cd .. && CGO_ENABLED=0 go install)
go install system-transparency.org/stmgr
go install system-transparency.org/stprov/cmd/stprov
go install github.com/u-root/u-root
go install ./look-for
go install ./serve-http

download debian-package.zip  "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.zip"
download debian-package.json "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.json"
download root.cert       "https://git.glasklar.is/glasklar/trust/glasklar-ca/-/raw/main/QA-CA/ST-signing/Glasklar QA ST signing root cert.pem"

# Extract kernel from OS package, currently located in the zipfile's boot/.
unzip -oj cache/debian-package.zip '*qa-debian-bookworm-amd64.vmlinuz' -d cache/
ln -srf cache/qa-debian-bookworm-amd64.vmlinuz cache/stboot-vmlinuz
ln -srf cache/qa-debian-bookworm-amd64.vmlinuz cache/stprov-vmlinuz

rm -rf out
mkdir out

# Extract needed drivers from OS pacakge. The target test machien
# features intel, and broadcom network cards. Besides network drivers,
# we need usb keyboard, efivarfs and virt-rng (for qemu runs).
mkdir out/modules
unzip -p cache/debian-package.zip '*qa-debian-bookworm-amd64.cpio.gz' | gzip -d \
  | cpio -i -D out/modules/ -d --no-absolute-filenames \
	 'usr/lib/modules/*/kernel/drivers/net/*/e1000.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/igb.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/igc.ko' \
	 'usr/lib/modules/*/kernel/drivers/dca/dca.ko' \
	 'usr/lib/modules/*/kernel/drivers/i2c/*/i2c-algo-bit.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/tg3.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/libphy.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/bonding.ko' \
	 'usr/lib/modules/*/kernel/net/*/tls.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio_ring.ko' \
	 'usr/lib/modules/*/kernel/drivers/char/*/virtio-rng.ko' \
	 'usr/lib/modules/*/kernel/fs/efivarfs/efivarfs.ko' \
	 'usr/lib/modules/*/kernel/drivers/hid/usbhid/usbhid.ko' \
	 'usr/lib/modules/*/kernel/drivers/hid/hid-generic.ko' \
	 'usr/lib/modules/*/kernel/drivers/hid/hid.ko' \
	 'usr/lib/modules/*/kernel/drivers/usb/host/xhci-hcd.ko' \
	 'usr/lib/modules/*/kernel/drivers/usb/host/xhci-pci.ko' \
	 'usr/lib/modules/*/kernel/drivers/usb/core/usbcore.ko' \
	 'usr/lib/modules/*/kernel/drivers/usb/common/usb-common.ko' \
	 'usr/lib/modules/*/modules.dep'

cat > out/1-modules.conf <<EOF
e1000
igb
igc
tg3
virtio-rng
bonding
efivarfs
usbhid
hid-generic
xhci-hcd
xhci-pci
EOF

# Create a provisioning OS package, based on u-root and stprov.
uroot_version=$(go list -m -f '{{.Version}}' github.com/u-root/u-root)
[[ -d cache/u-root ]] ||
	git clone -c advice.detachedHead=false --depth 1 -b "$uroot_version" https://github.com/u-root/u-root cache/u-root &&
	(cd cache/u-root && go install)

cat > out/stprov.sh <<EOF
mount -t efivarfs none /sys/firmware/efi/efivars
stprov remote ${stprov_network_args} ${stprov_mac} --url "${stprov_url}"
echo
echo SUCCESS: provisioning
sleep 10
shutdown reboot
EOF

(cd cache/u-root &&
    ../../bin/u-root -o ../../out/stprov.cpio \
	-uinitcmd="/bin/sh stprov.sh" \
	-defaultsh="elvish"\
	-files ../../out/1-modules.conf:lib/modules-load.d/1-modules.conf \
	-files ../../out/modules/usr/lib/modules:lib/modules \
	-files ../../bin/stprov:bin/stprov \
	-files ../../out/stprov.sh:stprov.sh \
	-files ../../isrgrootx1.pem:etc/trust_policy/tls_roots.pem\
	./cmds/core/{init,elvish,mount,shutdown}
)

gzip -9 out/stprov.cpio && mv out/stprov.cpio.gz out/stprov-initramfs
./bin/stmgr ospkg create -cmdline="${CONSOLE}" \
			 -kernel=cache/stprov-vmlinuz -initramfs=out/stprov-initramfs \
			 -out out/stprov

# We need a key and self-signed cert for signing OS packages
./bin/stmgr keygen certificate -isCA -keyOut out/key.pem -certOut out/cert.pem
./bin/stmgr ospkg sign -key out/key.pem -cert out/cert.pem -ospkg out/stprov.json

# Wrap stboot + config + provisioning os package in an initramfs
mkdir out/tmp-initramfs
(cd out/tmp-initramfs
 ln -sr ../../bin/stboot init

 # Copy kernel modules
 mkdir -p lib/modules-load.d/
 # cpio --dereferense doesn't dereference symlinks to directories,
 # so copy tree instead.
 cp -r ../modules/usr/lib/modules/ lib/
 ln -sr ../1-modules.conf lib/modules-load.d/
 mkdir -p etc/trust_policy
 ln -sr ../../isrgrootx1.pem etc/trust_policy/tls_roots.pem
 cat > etc/trust_policy/trust_policy.json <<EOF
{"ospkg_signature_threshold": 1, "ospkg_fetch_method": "network"}
EOF
 ln -sr ../cert.pem etc/trust_policy/ospkg_signing_root.pem

 # Add provisioning package
 mkdir ospkg
 ln -sr ../stprov.json ospkg/provision.json
 ln -sr ../stprov.zip ospkg/provision.zip

 find . | cpio -o -H newc -R 0:0 --dereference
) > out/initramfs.tmp && gzip -9 out/initramfs.tmp && mv out/initramfs.tmp.gz out/initramfs

./bin/stmgr uki create -format iso -out out/stboot.iso -kernel=cache/stboot-vmlinuz -initramfs=out/initramfs \
	    -cmdline="${CONSOLE} -- --loglevel=debug"

# Populate web directory
mkdir out/www
ln -sr cache/debian-package.zip out/www
cp cache/debian-package.json out/www

# Re-sign OS package
./bin/stmgr ospkg sign -key out/key.pem -cert out/cert.pem -ospkg out/www/debian-package.json

if [[ ${run_qemu} != yes ]] ; then
    echo "To run test, make these two files available in the test lab:"
    echo
    echo "   out/stboot.iso"
    echo "      Cdrom image, to be mounted as boot device on the test machine."
    echo
    echo "   out/www/debian-package.json"
    echo "      OS package descriptor, to be made available at http://st.bootlab.glasklarteknik.se:8000/debian-package.json"
    exit 0
fi

# Initialize efi vars
cp /usr/share/OVMF/OVMF_VARS_4M.fd out/OVMF_vars.fd

# shellcheck disable=SC2317  # This is reachable via trap
function clean_up() {
    local qemu_pid
    qemu_pid=$(cat out/qemu.pid 2>/dev/null) || return 0

    # QEMU removes the pid file before exiting. There is a race where
    # we might read the pid file and attempt to kill the process too
    # late. Due to the short period, it's extremely unlikely that the
    # pid has already been reused for a different process.
    kill "${qemu_pid}"
}

trap clean_up EXIT

# Wait for expected line to appear, with 5 minute timeout per QEMU invocation.
for expect in "SUCCESS: provision" "amnesiac-debian login: " ; do
    # Enable kvm acceleration if available, with fallback to tcg.
    (qemu-system-x86_64  </dev/null \
        -accel kvm -accel tcg -nographic -no-reboot -pidfile out/qemu.pid \
	-object rng-random,filename=/dev/urandom,id=rng0 -device virtio-rng-pci,rng=rng0 \
	-nic 'type=user,guestfwd=tcp:10.0.2.50:80-cmd:./bin/serve-http -d out/www' \
	-drive "if=pflash,format=raw,readonly=on,file=/usr/share/OVMF/OVMF_CODE_4M.fd" \
	-drive "if=pflash,format=raw,file=out/OVMF_vars.fd" \
        -drive file=out/stboot.iso,format=raw,media=cdrom,readonly=on \
	-m 4G 2>&1 |
	   tee out/qemu.log & ) | timeout 5m ./bin/look-for "${expect}" ||
	die "Timeout, when waiting for '${expect}'"
    # Stop qemu, and wait for it to go away.
    clean_up
    while [[ -f out/qemu.pid ]] ; do sleep 2 ; done
done
