module testonly

go 1.22

require (
	github.com/u-root/u-root v0.14.0
	system-transparency.org/stmgr v0.5.1
	system-transparency.org/stprov v0.4.2
)

require (
	filippo.io/age v1.2.1 // indirect
	github.com/diskfs/go-diskfs v1.3.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/foxboron/go-uefi v0.0.0-20250207204325-69fb7dba244f // indirect
	github.com/go-ping/ping v1.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/hugelgupf/go-shlex v0.0.0-20200702092117-c80c9d0918fa // indirect
	github.com/hugelgupf/vmtest v0.0.0-20240216064925-0561770280a1 // indirect
	github.com/insomniacslk/dhcp v0.0.0-20231206064809-8c70d406f6d2 // indirect
	github.com/josharian/native v1.1.0 // indirect
	github.com/klauspost/compress v1.17.4 // indirect
	github.com/mdlayher/packet v1.1.2 // indirect
	github.com/mdlayher/socket v0.5.0 // indirect
	github.com/pierrec/lz4 v2.3.0+incompatible // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pkg/xattr v0.4.1 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/afero v1.9.3 // indirect
	github.com/u-root/gobusybox/src v0.0.0-20240225013946-a274a8d5d83a // indirect
	github.com/u-root/mkuimage v0.0.0-20240225063926-11a3bcc79c2a // indirect
	github.com/u-root/uio v0.0.0-20240224005618-d2acac8f3701 // indirect
	github.com/ulikunitz/xz v0.5.11 // indirect
	github.com/vishvananda/netlink v1.3.0 // indirect
	github.com/vishvananda/netns v0.0.4 // indirect
	golang.org/x/crypto v0.33.0 // indirect
	golang.org/x/exp v0.0.0-20240222234643-814bf88cf225 // indirect
	golang.org/x/mod v0.20.0 // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/text v0.22.0 // indirect
	golang.org/x/tools v0.24.0 // indirect
	gopkg.in/djherbis/times.v1 v1.2.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	mvdan.cc/sh/v3 v3.7.0 // indirect
	sigsum.org/sigsum-go v0.10.1 // indirect
	system-transparency.org/stboot v0.5.2 // indirect
)
