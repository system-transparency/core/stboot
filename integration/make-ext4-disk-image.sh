#! /bin/bash

set -eu

die () {
    echo "$@" >&2
    exit 1
}

dir=""
name="fs"

while [[ $# -gt 0 ]]; do
    case "$1" in
	"-o")
	    shift
	    [[ $# -gt 0 ]] || die "Missing argument for -o"
	    [[ -z "${output+x}" ]] || die "Can only use -o once"
	    output="$1"
	    ;;
	"-n")
	    shift
	    [[ $# -gt 0 ]] || die "Missing argument for -n"
	    name="$1"
	    ;;
	"-d")
	    shift
	    [[ $# -gt 0 ]] || die "Missing argument for -d"
	    dir="$1"
	    ;;
	*)
	    die "Unknown argument $1"
    esac
    shift
done

[[ "${output+x}" ]] || die "Output not specified"

mkfs_opts=(-q -E offset=1048576)

if [[ -n "${dir}" ]] ; then
    size=$(du -s -B 1024 "$dir" | cut -f1)
    # Add some margin for fs metadata, then round upwards to MiB size.
    fs_size_MiB=$(( ( size + 200 + 1023 ) / 1024 ))
    mkfs_opts+=(-d "${dir}")

    if [[ "${fs_size_MiB}" -lt 2 ]] ; then
	# mkfs.ext4 refuses to add a journal for small filesystems,
	# and warns about that. So don't request a journal.
	mkfs_opts+=(-O ^has_journal)
    fi
else
    fs_size_MiB=10
fi

# Reserve 1 MiB at the start (for GPT partition table) and 1 MiB at the end
dd if=/dev/zero bs=1024 count=$(((2 + fs_size_MiB) * 1024)) of="${output}"

# Note that parted uses a 512 byte sector size
/sbin/parted -s -a opt -- "${output}" mklabel gpt mkpart "${name}" ext4 2048s -2048s

# Note: Depends on any local settings in /etc/mke2fs.conf
/sbin/mkfs.ext4 "${mkfs_opts[@]}" "${output}" $(( 1024 * fs_size_MiB ))
