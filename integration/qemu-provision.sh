#! /bin/bash

set -eu

# Change directory to where script is located.
cd "$(dirname "$0")"

# Use local directory for built go tools.
GOBIN=$(pwd)/bin
export GOBIN

[ -d cache ] || mkdir cache

die() {
    echo "$@" >&2
    exit 1
}

interactive=no
while [[ $# -gt 0 ]]; do
    case "$1" in
	--interactive|-i)
	    interactive=yes
	    ;;
	--help)
	    echo "Options:"
	    echo "  --interactive, -i   Run final qemu with interactive terminal."
	    echo "  --help              Show this help."
	    exit
	    ;;
	*)
	    die "Unknown option: $1, try --help"
	    ;;
    esac
    shift
done

download() {
    local file=$1
    local url=$2
    local remote_file
    remote_file=$(basename "${url}")
    # Unfortunately, wget -O --timestamping doesn't work, instead,
    # make a symlink to desired name.
    wget --no-verbose --timestamping --directory-prefix=cache "${url}" &&
    ln -srf "cache/${remote_file}" "cache/${file}"
}

# Disable cgo, to get a statically linked executable.
(cd .. && CGO_ENABLED=0 go install)
go install system-transparency.org/stmgr
go install system-transparency.org/stprov/cmd/stprov
go install github.com/u-root/u-root
go install ./look-for
go install ./serve-http/serve-http.go

download debian-package.zip  "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.zip"
download debian-package.json "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.json"

# Extract kernel from OS package, currently located in the zipfile's boot/.
unzip -oj cache/debian-package.zip '*qa-debian-bookworm-amd64.vmlinuz' -d cache/
ln -srf cache/qa-debian-bookworm-amd64.vmlinuz cache/stboot-vmlinuz
ln -srf cache/qa-debian-bookworm-amd64.vmlinuz cache/stprov-vmlinuz

rm -rf out
mkdir out

# Extract intel e1000 network, virtio-rng, and efivarfs drivers from
# OS package. Corresponding virtual hardware is added on the qemu
# command line.
mkdir out/modules
unzip -p cache/debian-package.zip '*qa-debian-bookworm-amd64.cpio.gz' | gzip -d \
  | cpio -i -D out/modules/ -d --no-absolute-filenames \
	 'usr/lib/modules/*/kernel/drivers/net/*/e1000.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio_ring.ko' \
	 'usr/lib/modules/*/kernel/drivers/char/*/virtio-rng.ko' \
         'usr/lib/modules/*/kernel/fs/efivarfs/efivarfs.ko' \
	 'usr/lib/modules/*/modules.dep'

cat > out/1-modules.conf <<EOF
e1000
virtio-rng
efivarfs
EOF

# Create a provisioning OS package, based on u-root and stprov.
uroot_version=$(go list -m -f '{{.Version}}' github.com/u-root/u-root)
[[ -d cache/u-root ]] ||
	git clone -c advice.detachedHead=false --depth 1 -b "$uroot_version" https://github.com/u-root/u-root cache/u-root &&
	(cd cache/u-root && go install)

cat > out/stprov.sh <<EOF
mount -t efivarfs none /sys/firmware/efi/efivars
stprov remote dhcp --url http://10.0.2.50:80/debian-package.json
echo
echo SUCCESS: provisioning
shutdown
EOF

(cd cache/u-root &&
    ../../bin/u-root -o ../../out/stprov.cpio \
	-uinitcmd="/bin/sh stprov.sh" \
	-defaultsh="elvish"\
	-files ../../out/1-modules.conf:lib/modules-load.d/1-modules.conf \
	-files ../../out/modules/usr/lib/modules:lib/modules \
	-files ../../bin/stprov:bin/stprov \
	-files ../../out/stprov.sh:stprov.sh \
	-files ../../isrgrootx1.pem:etc/trust_policy/tls_roots.pem\
	./cmds/core/{init,elvish,mount,shutdown}
)

gzip -9 out/stprov.cpio && mv out/stprov.cpio.gz out/stprov-initramfs
./bin/stmgr ospkg create -cmdline='console=ttyS0,115200n8' \
			 -kernel=cache/stprov-vmlinuz -initramfs=out/stprov-initramfs \
			 -out out/stprov

# We need a key and self-signed cert for signing OS packages
./bin/stmgr keygen certificate -isCA -keyOut out/key.pem -certOut out/cert.pem
./bin/stmgr ospkg sign -key out/key.pem -cert out/cert.pem -ospkg out/stprov.json

# Wrap stboot + config + provisioning os package in an initramfs
mkdir out/tmp-initramfs
(cd out/tmp-initramfs
 ln -sr ../../bin/stboot init

 # Copy kernel modules
 mkdir -p lib/modules-load.d/
 # cpio --dereferense doesn't dereference symlinks to directories,
 # so copy tree instead.
 cp -r ../modules/usr/lib/modules/ lib/
 ln -sr ../1-modules.conf lib/modules-load.d/
 mkdir -p etc/trust_policy
 ln -sr ../../isrgrootx1.pem etc/trust_policy/tls_roots.pem
 cat > etc/trust_policy/trust_policy.json <<EOF
{"ospkg_signature_threshold": 1, "ospkg_fetch_method": "network"}
EOF
 ln -sr ../cert.pem etc/trust_policy/ospkg_signing_root.pem

 # Add provisioning package
 mkdir ospkg
 ln -sr ../stprov.json ospkg/provision.json
 ln -sr ../stprov.zip ospkg/provision.zip

 find . | cpio -o -H newc -R 0:0 --dereference
) > out/initramfs.tmp && gzip -9 out/initramfs.tmp && mv out/initramfs.tmp.gz out/stboot-initramfs

./bin/stmgr uki create -format uki -out out/stboot.uki -kernel=cache/stboot-vmlinuz -initramfs=out/stboot-initramfs \
	    -cmdline='console=ttyS0,115200n8 -- --loglevel=debug'

./make-vfat-disk-image.sh -o out/disk.img out/stboot.uki

# Populate web directory
mkdir out/www
ln -sr cache/debian-package.zip out/www
# Use a relative url in the descriptor
jq < cache/debian-package.json '.os_pkg_url = "debian-package.zip"' > out/www/debian-package.json

# Re-sign OS package
./bin/stmgr ospkg sign -key out/key.pem -cert out/cert.pem -ospkg out/www/debian-package.json

# Initialize efi vars
cp /usr/share/OVMF/OVMF_VARS_4M.fd out/OVMF_vars.fd

function clean_up() {
    local qemu_pid
    qemu_pid=$(cat out/qemu.pid 2>/dev/null) || return 0

    # QEMU removes the pid file before exiting. There is a race where
    # we might read the pid file and attempt to kill the process too
    # late. Due to the short period, it's extremely unlikely that the
    # pid has already been reused for a different process.
    kill "${qemu_pid}"
}

trap clean_up EXIT

run_qemu() {
    qemu-system-x86_64 \
        -accel kvm -accel tcg -nographic -no-reboot -pidfile out/qemu.pid \
	-object rng-random,filename=/dev/urandom,id=rng0 -device virtio-rng-pci,rng=rng0 \
	-drive "if=pflash,format=raw,readonly=on,file=/usr/share/OVMF/OVMF_CODE_4M.fd" \
	-drive "if=pflash,format=raw,file=out/OVMF_vars.fd" \
	-drive file=out/disk.img,format=raw \
	-m 4G "$@"
}

# Wait for expected line to appear, with 5 minute timeout per QEMU invocation.
for expect in "SUCCESS: provision" "amnesiac-debian login: " ; do
    # Enable kvm acceleration if available, with fallback to tcg.
    (run_qemu </dev/null \
	-nic 'type=user,guestfwd=tcp:10.0.2.50:80-cmd:./bin/serve-http -d out/www' 2>&1 |
	   tee -a out/qemu.log & ) | timeout 5m ./bin/look-for "${expect}" ||
	die "Timeout, when waiting for '${expect}'"
    # Stop qemu, and wait for it to go away.
    clean_up
    while [[ -f out/qemu.pid ]] ; do sleep 2 ; done
done

if [[ "$interactive" = yes ]] ; then
    echo >&2 "Starting QEMU in the foreground, try to interrupt with Ctrl-C"
    run_qemu -nic 'type=user,guestfwd=tcp:10.0.2.50:80-cmd:./bin/serve-http -d out/www' 2>&1 \
	| tee -a out/qemu.log
else
    # Test with a misconfigured web server providing the OS package.
    # Expect to see prompt for entering provisioning mode.
    mkfifo out/qemu.serial.in out/qemu.serial.out

    run_qemu </dev/null -nic 'type=user,guestfwd=tcp:10.0.2.50:80-cmd:./bin/serve-http -d out/www2' \
			-serial pipe:out/qemu.serial >> out/qemu.log 2>&1 &

    tee < out/qemu.serial.out -a out/qemu.log | (
	timeout 5m ./bin/look-for "Enter provisioning mode? (Y/n)" || die "Timeout, when waiting for provisioning mode Y/n prompt"
	# Make web service functional
	ln -sr out/www out/www2
	echo Yes > out/qemu.serial.in
	timeout 5m ./bin/look-for "SUCCESS: provision" || die "Timeout, when waiting for provisioning"
	clean_up
    )

    # Test interrupting stboot with with Ctrl-C.
    # Expect to see prompt for entering provisioning mode.
    run_qemu </dev/null -nic 'type=user,guestfwd=tcp:10.0.2.50:80-cmd:./bin/serve-http -d out/www' \
			-serial pipe:out/qemu.serial >> out/qemu.log 2>&1 &

    tee < out/qemu.serial.out -a out/qemu.log | (
	timeout 5m ./bin/look-for -i ']' " Press Ctrl-C to interrupt normal boot" || die "Timeout, when waiting Ctrl-C message"
	# Type Ctrl-C after 2s; stboot should wait at least 5s before booting.
	sleep 2 ; printf '\x03' > out/qemu.serial.in
	timeout 5m ./bin/look-for "Enter provisioning mode? (Y/n)" || die "Timeout, when waiting for provisioning mode Y/n prompt"
	echo Yes > out/qemu.serial.in
	timeout 5m ./bin/look-for "SUCCESS: provision" || die "Timeout, when waiting for provisioning"
	clean_up
    )
fi
