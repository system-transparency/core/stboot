#! /bin/bash

# Script to create a bootable iso image that netboots via bonding,
# with the network setup of glasklar's test server.

set -eu

# Change directory to where script is located.
cd "$(dirname "$0")"

die() {
    echo "$@" >&2
    exit 1
}

run_qemu=no
network_mode=static
dns_setting='"dns":["91.223.231.241"],' # Same as gateway.

while [[ $# -gt 0 ]]; do
    case "$1" in
	--qemu)
	    run_qemu=yes
	    ;;
	--dhcp)
	    network_mode=dhcp
	    dns_setting='' # Use resolver from dhcp
	    ;;
	--help)
	    echo "Options:"
	    echo "  --qemu    Run qemu, and set kernel console to serial."
	    echo "  --dhcp    Configure stboot to use dhcp."
	    echo "  --help    Show this help."
	    exit
	    ;;
	*)
	    die "Unknown option: $1, try --help"
	    ;;
    esac
    shift
done

# Use local directory for built go tools.
GOBIN=$(pwd)/bin
export GOBIN

[ -d cache ] || mkdir cache

download() {
    local file=$1
    local url=$2
    local remote_file
    remote_file=$(basename "${url}")
    # Unfortunately, wget -O --timestamping doesn't work, instead,
    # make a symlink to desired name.
    wget --no-verbose --timestamping --directory-prefix=cache "${url}" &&
    ln -srf "cache/${remote_file}" "cache/${file}"
}

# Disable cgo, to get a statically linked executable.
(cd .. && CGO_ENABLED=0 go install)
go install system-transparency.org/stmgr
go install ./look-for

download debian-package.zip  "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.zip"
download root.cert       "https://git.glasklar.is/glasklar/trust/glasklar-ca/-/raw/main/QA-CA/ST-signing/Glasklar QA ST signing root cert.pem"

# Extract kernel from OS package, currently located in the zipfile's boot/.
unzip -oj cache/debian-package.zip '*qa-debian-bookworm-amd64.vmlinuz' -d cache/
ln -srf cache/qa-debian-bookworm-amd64.vmlinuz cache/stboot-vmlinuz

rm -rf out
mkdir out

# Extract needed drivers from OS pacakge. The target test machien
# features intel, and broadcom network cards. Besides network drivers,
# we need bonding, efivarfs and virt-rng (for qemu runs).
mkdir out/modules
unzip -p cache/debian-package.zip '*qa-debian-bookworm-amd64.cpio.gz' | gzip -d \
  | cpio -i -D out/modules/ -d --no-absolute-filenames \
	 'usr/lib/modules/*/kernel/drivers/net/*/e1000.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/igb.ko' \
	 'usr/lib/modules/*/kernel/drivers/dca/dca.ko' \
	 'usr/lib/modules/*/kernel/drivers/i2c/*/i2c-algo-bit.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/tg3.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/libphy.ko' \
	 'usr/lib/modules/*/kernel/drivers/net/*/bonding.ko' \
	 'usr/lib/modules/*/kernel/net/*/tls.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio_ring.ko' \
	 'usr/lib/modules/*/kernel/drivers/char/*/virtio-rng.ko' \
	 'usr/lib/modules/*/kernel/fs/efivarfs/efivarfs.ko' \
	 'usr/lib/modules/*/modules.dep'

# Wrap stboot + config + example-os in an initramfs
mkdir out/tmp-initramfs
(cd out/tmp-initramfs
 ln -sr ../../bin/stboot init

 # Copy kernel modules
 mkdir -p lib/modules-load.d/
 # cpio --dereferense doesn't dereference symlinks to directories,
 # so copy tree instead.
 cp -r ../modules/usr/lib/modules/ lib/
 cat > lib/modules-load.d/1-modules.conf <<EOF
e1000
igb
tg3
virtio-rng
bonding
efivarfs
EOF

 mkdir -p etc/trust_policy
 ln -sr ../../isrgrootx1.pem etc/trust_policy/tls_roots.pem
 cat > etc/trust_policy/trust_policy.json <<EOF
{"ospkg_signature_threshold": 1, "ospkg_fetch_method": "network"}
EOF
 ln -sr ../../cache/root.cert etc/trust_policy/ospkg_signing_root.pem
 # We get traditional names ethX, not enp1s0f0, enp1s0f1
 cat > etc/host_configuration.json <<EOF
{
  "network_mode":"${network_mode}",
  "host_ip":"91.223.231.242/29",
  "gateway":"91.223.231.241",
  ${dns_setting}
  "network_interfaces": [
    {"interface_name": "eth0", "mac_address": "00:0a:f7:2a:59:bc" },
    {"interface_name": "eth2", "mac_address": "00:0a:f7:2a:59:bd" }
  ],
  "bonding_mode": "802.3ad",
  "bond_name": "bond0",
  "ospkg_pointer": "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.json"
}
EOF
 find . | cpio -o -H newc -R 0:0 --dereference
) > out/initramfs.tmp && gzip -9 out/initramfs.tmp && mv out/initramfs.tmp.gz out/initramfs

CONSOLE=
if [[ ${run_qemu} = yes ]] ; then
    CONSOLE='console=ttyS0,115200n8'
fi

./bin/stmgr uki create -format iso -out out/stboot.iso -kernel=cache/stboot-vmlinuz -initramfs=out/initramfs \
	    -cmdline="${CONSOLE} -- --loglevel=debug"

[[ ${run_qemu} = yes ]] || exit 0

# shellcheck disable=SC2317  # This is reachable via trap
function clean_up() {
    local qemu_pid
    qemu_pid=$(cat out/qemu.pid 2>/dev/null) || return 0

    # QEMU removes the pid file before exiting. There is a race where
    # we might read the pid file and attempt to kill the process too
    # late. Due to the short period, it's extremely unlikely that the
    # pid has already been reused for a different process.
    kill "${qemu_pid}"
}

trap clean_up EXIT

# This qemu environment lacks a network switch with bonding support,
# but should be able to boot far enough to get stboot running, and add
# eth0 to the bonded interface.

# Enable kvm acceleration if available, with fallback to tcg.
timeout 5m qemu-system-x86_64 </dev/null \
  -accel kvm -accel tcg -nographic -no-reboot -pidfile out/qemu.pid \
  -bios /usr/share/ovmf/OVMF.fd \
  -object rng-random,filename=/dev/urandom,id=rng0 -device virtio-rng-pci,rng=rng0 \
  -nic 'type=user' \
  -drive file=out/stboot.iso,format=raw,media=cdrom,readonly=on \
  -m 4G 2>&1 | tee out/qemu.log
