#! /bin/bash

set -eu

# Change directory to where script is located.
cd "$(dirname "$0")"

# Use local directory for built go tools.
GOBIN=$(pwd)/bin
export GOBIN

[ -d cache ] || mkdir cache

die() {
    echo "$@" >&2
    exit 1
}

download() {
    local file=$1
    local url=$2
    local remote_file
    remote_file=$(basename "${url}")
    # Unfortunately, wget -O --timestamping doesn't work, instead,
    # make a symlink to desired name.
    wget --no-verbose --timestamping --directory-prefix=cache "${url}" &&
    ln -srf "cache/${remote_file}" "cache/${file}"
}

# Disable cgo, to get a statically linked executable.
(cd .. && CGO_ENABLED=0 go install)
go install system-transparency.org/stmgr
go install ./look-for
go install ./serve-http/serve-http.go

download debian-package.zip  "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.zip"
download debian-package.json "https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.json"
download root.cert       "https://git.glasklar.is/glasklar/trust/glasklar-ca/-/raw/main/QA-CA/ST-signing/Glasklar QA ST signing root cert.pem"

# Extract kernel from OS package, currently located in the zipfile's boot/.
unzip -oj cache/debian-package.zip '*qa-debian-bookworm-amd64.vmlinuz' -d cache/
ln -srf cache/qa-debian-bookworm-amd64.vmlinuz cache/stboot-vmlinuz

rm -rf out
mkdir out

# Extract intel e1000 network, virtio-rng, and efivarfs drivers from
# OS package. Corresponding virtual hardware is added on the qemu
# command line.
mkdir out/modules
unzip -p cache/debian-package.zip '*qa-debian-bookworm-amd64.cpio.gz' | gzip -d \
  | cpio -i -D out/modules/ -d --no-absolute-filenames \
	 'usr/lib/modules/*/kernel/drivers/net/*/e1000.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio.ko' \
	 'usr/lib/modules/*/kernel/drivers/virtio/virtio_ring.ko' \
	 'usr/lib/modules/*/kernel/drivers/char/*/virtio-rng.ko' \
	 'usr/lib/modules/*/kernel/fs/efivarfs/efivarfs.ko' \
	 'usr/lib/modules/*/modules.dep'

# Wrap stboot + config + example-os in an initramfs
mkdir out/tmp-initramfs
(cd out/tmp-initramfs
 ln -sr ../../bin/stboot init

 # Copy kernel modules
 mkdir -p lib/modules-load.d/
 # cpio --dereferense doesn't dereference symlinks to directories,
 # so copy tree instead.
 cp -r ../modules/usr/lib/modules/ lib/
 cat > lib/modules-load.d/1-modules.conf <<EOF
e1000
virtio-rng
efivarfs
EOF

 mkdir -p etc/trust_policy
 cat > etc/trust_policy/trust_policy.json <<EOF
{"ospkg_signature_threshold": 1, "ospkg_fetch_method": "network"}
EOF
 ln -sr ../../cache/root.cert etc/trust_policy/ospkg_signing_root.pem
 cat > etc/host_configuration.json <<EOF
{
  "network_mode":"dhcp",
  "ospkg_pointer": "http://10.0.2.50:80/debian-package.json"
}
EOF
 find . | cpio -o -H newc -R 0:0 --dereference
) > out/initramfs.tmp && gzip -9 out/initramfs.tmp && mv out/initramfs.tmp.gz out/initramfs

./bin/stmgr uki create -format uki -out out/stboot.uki -kernel=cache/stboot-vmlinuz -initramfs=out/initramfs \
	    -cmdline='console=ttyS0,115200n8 -- --loglevel=debug'

./make-vfat-disk-image.sh -o out/disk.img out/stboot.uki

# Populate web directory
mkdir out/www
ln -sr cache/debian-package.zip out/www
# Use a relative url in the descriptor
jq < cache/debian-package.json '.os_pkg_url = "debian-package.zip"' > out/www/debian-package.json

# shellcheck disable=SC2317  # This is reachable via trap
function clean_up() {
    local qemu_pid
    qemu_pid=$(cat out/qemu.pid 2>/dev/null) || return 0

    # QEMU removes the pid file before exiting. There is a race where
    # we might read the pid file and attempt to kill the process too
    # late. Due to the short period, it's extremely unlikely that the
    # pid has already been reused for a different process.
    kill "${qemu_pid}"
}

trap clean_up EXIT

# Wait for login line to appear, times out after about 10 minutes.
# Enable kvm acceleration if available, with fallback to tcg.
(qemu-system-x86_64 </dev/null \
  -accel kvm -accel tcg -nographic -no-reboot -pidfile out/qemu.pid \
  -bios /usr/share/ovmf/OVMF.fd \
  -object rng-random,filename=/dev/urandom,id=rng0 -device virtio-rng-pci,rng=rng0 \
  -nic 'type=user,guestfwd=tcp:10.0.2.50:80-cmd:./bin/serve-http -d out/www' \
  -drive file=out/disk.img,format=raw \
  -m 4G 2>&1 | tee out/qemu.log & ) | timeout 5m ./bin/look-for "amnesiac-debian login: " ||
  die 'Timeout, no login prompt'
