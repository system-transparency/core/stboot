package opts

import (
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"os"
	"time"

	"filippo.io/age"

	"system-transparency.org/stboot/stlog"
	"system-transparency.org/stboot/trust"
)

func ReadTrustPolicy(filename string) (*trust.Policy, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("opening trust policy failed: %w", err)
	}
	defer f.Close()

	trustPolicy := trust.Policy{}
	if err := json.NewDecoder(f).Decode(&trustPolicy); err != nil {
		return nil, err
	}
	return &trustPolicy, nil
}

// Read a certificate file, checking and logging validity dates. Skip
// invalid certs, but return error if no valid certs are found.
func ReadCertsFile(filename string, now time.Time) ([]*x509.Certificate, error) {
	pemBytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("reading cert file failed: %w", err)
	}

	var certs []*x509.Certificate

	for i := 0; len(pemBytes) > 0; i++ {
		block, rest := pem.Decode(pemBytes)
		if block == nil {
			break
		}
		pemBytes = rest

		if block.Type != "CERTIFICATE" {
			stlog.Error("Reading %q, unexpected PEM block of type %q", filename, block.Type)
			continue
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			stlog.Error("Reading %q, cert #%d failed: %v", filename, i, err)
			continue
		}

		if now.Before(cert.NotBefore) {
			stlog.Error("Reading %q, cert #%d not yet valid, valid from: %s", filename, i, cert.NotBefore)
			continue
		}
		if now.After(cert.NotAfter) {
			stlog.Error("Reading %q, cert #%d expired at: %s", filename, i, cert.NotAfter)
			continue
		}
		if now.Add(7 * 24 * time.Hour).After(cert.NotAfter) {
			stlog.Warn("Reading %q, cert #%d expires in less than a week, at: %s", filename, i, cert.NotAfter)
		} else {
			stlog.Info("Reading %q, cert #%d expires at: %s", filename, i, cert.NotAfter)
		}

		certs = append(certs, cert)
	}

	if len(certs) == 0 {
		return nil, errors.New("no valid certificates found")
	}
	return certs, nil
}

// Like ReadCertsFile, but if the file doesn't exist, return nil,
// without error.
func ReadOptionalCertsFile(filename string, now time.Time) ([]*x509.Certificate, error) {
	certs, err := ReadCertsFile(filename, now)
	if errors.Is(err, os.ErrNotExist) {
		return nil, nil
	}
	return certs, err
}

// Returns nil, without error, if file doesn't exist.
func ReadDecryptionIdentities(filename string) ([]age.Identity, error) {
	file, err := os.Open(filename)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, nil
		}
		return nil, fmt.Errorf("reading decryption identities failed: %w", err)
	}
	defer file.Close()
	return age.ParseIdentities(file)
}
