package opts

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"log"
	"math/big"
	"os"
	"path/filepath"
	"strings"
	"time"

	"testing"
)

func TestReadTrustPolicy(t *testing.T) {
	tests := []struct {
		name, file string
		wantErr    bool
	}{
		{
			name: "Successful loading",
			file: "testdata/trust_policy_good_all_set.json",
		},
		{
			name:    "Empty",
			file:    "testdata/empty",
			wantErr: true,
		},
		{
			name:    "Bad content",
			file:    "testdata/trust_policy_bad_unset.json",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			trustPolicy, err := ReadTrustPolicy(tt.file)

			if tt.wantErr {
				if err != nil {
					t.Logf("%s: err (expected): %v", tt.name, err)
				} else {
					t.Errorf("%s: invalid input, but no error", tt.name)
				}
			} else if err != nil {
				t.Errorf("%s: failed: %v", tt.name, err)
			} else if trustPolicy == nil {
				t.Errorf("%s: failed, nil trustPolicy", tt.name)
			}
		})
	}
}

func TestReadCertsFile(t *testing.T) {
	log.SetFlags(0)

	now := time.Date(2025, 1, 1, 0, 0, 0, 0, time.UTC)

	validCert := newTestCACert(t, now.Add(-24*time.Hour), now.Add(90*24*time.Hour))
	barelyValidCert := newTestCACert(t, now.Add(-24*time.Hour), now.Add(time.Hour))
	expiredCert := newTestCACert(t, now.Add(-90*24*time.Hour), now.Add(-time.Hour))
	notYetValidCert := newTestCACert(t, now.Add(time.Hour), now.Add(90*24*time.Hour))
	notACert := bytes.ReplaceAll(validCert, []byte("CERTIFICATE"), []byte("FOO"))
	badBase64Cert := bytes.Join([][]byte{validCert[:50], validCert[51:]}, nil) // Drop a base64 digits
	badCert := bytes.Join([][]byte{validCert[:50], validCert[54:]}, nil)       // Drop 4 base64 digits, 3 raw bytes
	tempDir := t.TempDir()

	for _, tt := range []struct {
		desc      string
		input     []byte
		wantCount int // -1 for error
		wantLog   []string
	}{
		{desc: "empty", wantCount: -1},
		{
			desc:      "single valid",
			input:     validCert,
			wantCount: 1,
			wantLog: []string{
				"cert #0 expires at:",
			},
		},
		{
			desc:      "mix of valid and bad certs",
			input:     bytes.Join([][]byte{validCert, expiredCert, notYetValidCert, notACert, badBase64Cert, badCert, barelyValidCert}, nil),
			wantCount: 2,
			wantLog: []string{
				"cert #0 expires at:",
				"cert #1 expired at:",
				"cert #2 not yet valid",
				"unexpected PEM block of type \"FOO\"",
				"cert #4 failed: x509",
				// badBase64Cert is silently ignored,
				// since pem.Decode silently discards
				// all blocks where base64 decoding
				// fails.
				"cert #5 expires in less than a week",
			},
		},
	} {
		fileName := filepath.Join(tempDir, "certs")
		if err := os.WriteFile(fileName, tt.input, 0666); err != nil {
			t.Fatal(err)
		}

		buf := divertLog(t)
		certs, err := ReadCertsFile(fileName, now)
		if err != nil {
			if tt.wantCount >= 0 {
				t.Errorf("%s: Failed: %v", tt.desc, err)
			}
			continue
		}
		if tt.wantCount < 0 {
			t.Errorf("%s: Expected error, got: %v", tt.desc, certs)
			continue
		}
		if got := len(certs); got != tt.wantCount {
			t.Errorf("%s: Got %d certs, wanted %d", tt.desc, got, tt.wantCount)
			continue
		}

		// Check expectations on logged messages.
		logLines := strings.Split(buf.String(), "\n")
		if len(logLines) == 0 || logLines[len(logLines)-1] != "" {
			t.Errorf("%s: Failed to split log into proper lines, got %v", tt.desc, logLines)
			continue
		}
		logLines = logLines[:len(logLines)-1]
		for i, line := range logLines {
			if i < len(tt.wantLog) && !strings.Contains(line, tt.wantLog[i]) {
				t.Errorf("%s: missing message %q in log line %d: %q",
					tt.desc, tt.wantLog[i], i, line)
			}
			if i >= len(tt.wantLog) {
				t.Errorf("%s: unexpected message in log line %d: %q",
					tt.desc, i, line)
			}
		}
	}
}

func TestReadOptionalCertsFile(t *testing.T) {
	tests := []struct {
		name, file string
		wantCount  int
		wantErr    bool
	}{
		{
			name:      "Successful loading",
			file:      "testdata/cert.pem",
			wantCount: 1,
		},
		{
			name:      "Multiple certificates",
			file:      "testdata/certs.pem",
			wantCount: 2,
		},
		{
			name:      "No file",
			file:      "testdata/no-such-file.pem",
			wantCount: 0,
		},
		{
			name:    "empty PEM",
			file:    "testdata/empty",
			wantErr: true,
		},
		{
			name:    "bad PEM type",
			file:    "testdata/key.pem",
			wantErr: true,
		},
		{
			name:    "bad PEM bytes",
			file:    "testdata/cert_bad_pem.pem",
			wantErr: true,
		},
		{
			name:    "bad certificate",
			file:    "testdata/cert_bad_cert.pem",
			wantErr: true,
		},
	}
	now := time.Date(2030, 1, 1, 0, 0, 0, 0, time.UTC)
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			certs, err := ReadOptionalCertsFile(tt.file, now)
			if tt.wantErr {
				if err != nil {
					t.Logf("%s: err (expected): %v", tt.name, err)
				} else {
					t.Errorf("%s: invalid input, but no error", tt.name)
				}
			} else if err != nil {
				t.Errorf("%s: failed: %v", tt.name, err)
			} else if got := len(certs); got != tt.wantCount {
				t.Errorf("%s: failed, wantCount %d, got length %d", tt.name, tt.wantCount, got)
			}
		})
	}
}

// Create a new PEM-encoded self-signed cert with given validity.
func newTestCACert(t *testing.T, notBefore, notAfter time.Time) []byte {
	publicKey, signer, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	serialNumber, err := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 128))
	if err != nil {
		t.Fatal(err)
	}
	template := x509.Certificate{
		SerialNumber:          serialNumber,
		KeyUsage:              x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
		IsCA:                  true,
		NotBefore:             notBefore,
		NotAfter:              notAfter,
	}
	cert, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey, signer)
	if err != nil {
		t.Fatal(err)
	}
	return pem.EncodeToMemory(&pem.Block{
		Type:  "CERTIFICATE",
		Bytes: cert,
	})
}

// Divert logging to a buffer, for the duration of current test.
func divertLog(t *testing.T) *bytes.Buffer {
	old := log.Writer()
	buf := bytes.Buffer{}
	log.SetOutput(&buf)
	t.Cleanup(func() { log.SetOutput(old) })
	return &buf
}

func TestReadDecryptionIdentities(t *testing.T) {
	tests := []struct {
		name, file string
		wantCount  int
		wantErr    bool
	}{
		{
			name:      "Successful loading",
			file:      "testdata/decryption_identities_one",
			wantCount: 1,
		},
		{
			name:      "Multiple identities",
			file:      "testdata/decryption_identities_two",
			wantCount: 2,
		},
		{
			name:      "No file",
			file:      "testdata/decryption_identities_no-such-file",
			wantCount: 0,
		},
		{
			name:    "Empty file",
			file:    "testdata/empty",
			wantErr: true,
		},
		{
			name:    "Bad identities",
			file:    "testdata/decryption_identities_bad",
			wantErr: true,
		},
		{
			name:    "SSH ed25519 identity",
			file:    "testdata/decryption_identities_ssh_ed25519",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ids, err := ReadDecryptionIdentities(tt.file)
			if tt.wantErr {
				if err != nil {
					t.Logf("%s: err (expected): %v", tt.name, err)
				} else {
					t.Errorf("%s: invalid input, but no error", tt.name)
				}
			} else if err != nil {
				t.Errorf("%s: failed: %v", tt.name, err)
			} else if got := len(ids); got != tt.wantCount {
				t.Errorf("%s: failed, wantCount %d, got length %d", tt.name, tt.wantCount, got)
			}
		})
	}
}
