# stboot

The System Transparency boot loader

The stboot bootloader is typically installed as the init program inside
an initramfs. It can download an operating system package (OS package),
verify that it is signed according to the configured trust policy, and
then boot it.

## Building stboot

Being a go program, stboot is built using the go tools. To ensure that
the resulting executable is statically linked, `CGO_ENABLED=0` should
be set in the environment, e.g., `CGO_ENABLED=0 go install`.

There are several ways to get and build stboot. The first two can
benefit from verification of [release signatures][].

1. Clone the [stboot git repository][] and checkout one of the signed
   release tags and build.

2. Get a signed ST [collection release][] archive, unpack, and build
   in the stboot subdirectory.

3. Let the go tools download stboot, e.g.,
   `CGO_ENABLED=0 go install system-transparency.org/stboot@v0.4.3`.
   This method relies on go's checksum database for integrity, but
   there is no easy way to verify release signatures.

[stboot git repository]: https://git.glasklar.is/system-transparency/core/stboot.git
[collection release]: https://docs.system-transparency.org/st-1.1.0/docs/releases/collection-release/
[release signatures]: https://docs.system-transparency.org/st-1.1.0/docs/releases/signatures/

### Embedded stboot version information

The build process embeds an stboot version. The version is displayed
when booting using stboot, and in response to the `stboot --version`
option. In most cases this is set automatically based on go module
version or git revision. However, for installation method (2) above,
neither module version nor git revision is available.

The version can be overridden by passing a linker flag to assign a
non-empty string to the symbol `main.ConfiguredVersion`. E.g., when
building the st-1.2.0 collection release, it's suggested to build
using
`CGO_ENABLED=0 go build --ldflags="-X main.ConfiguredVersion=st-1.2.0`.

## Example usage

### A minimal example starting stboot in QEMU

Build stboot, as a static executable in the current directory
```
GOBIN="$(pwd)" CGO_ENABLED=0 go install system-transparency.org/stboot@latest
```

Build an initramfs (uncompressed, for simplicity) containing only the
stboot executable, under the name `init`
```
ln -s stboot init &&
  echo init | cpio -o -H newc -R 0:0 --dereference > initramfs
```
Test your initramfs (assuming your OS kernel at `/boot/vmlinuz`):
```
qemu-system-x86_64 -kernel /boot/vmlinuz -nographic --no-reboot -append "console=ttyS0,115200" -initrd initramfs -m 2048 --enable-kvm
```

You should see something like this:
```
[...]
[    0.459360] Run /init as init process
stboot: 2024/12/17 11:08:41 [INFO] Running as pid 1
stboot: 2024/12/17 11:08:41 [INFO] 
  _____ _______   _____   ____   ____________
 / ____|__   __|  |  _ \ / __ \ / __ \__   __|
| (___    | |     | |_) | |  | | |  | | | |
 \___ \   | |     |  _ <| |  | | |  | | | |
 ____) |  | |     | |_) | |__| | |__| | | |
|_____/   |_|     |____/ \____/ \____/  |_|

stboot: 2024/12/17 11:08:41 [INFO] version: v0.5.0
stboot: 2024/12/17 11:08:41 [ERROR] trust policy: opening trust policy failed: open /etc/trust_policy/trust_policy.json: no such file or directory
```
Due to missing configuration files, stboot fails. After a short delay,
it will attempt to reboot, and qemu exits.

## More complete examples

See this [integration test][] for a script that:

  1. Creates an initramfs including stboot, needed configuration, and a
     signed Debian OS package.
  2. Wraps it as a disk image that can be booted by UEFI firmware.
  3. Boots a QEMU machine from the resulting disk image.

The stimages repository includes additional tooling, see [stimages
build-stboot][] for a more configurable script that creates an stboot
image, configured for network boot.

[integration test]: ./integration/qemu-boot-from-disk.sh
[stimages build-stboot]: https://git.glasklar.is/system-transparency/core/stimages/-/blob/main/contrib/stboot/build-stboot

## Learn more

See [stboot documentation][] for more information about what stboot does
during boot. Refer to and see [ST documentation][] for specifications as
well as documentation for other related tools.

[stboot documentation]: ./docs/stboot-system.md
[ST documentation]: https://docs.system-transparency.org
