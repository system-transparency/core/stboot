// wctx package defines missing context-aware primitives
package wctx

import (
	"context"
	"io"
	"time"
)

// Suggested at
// https://pace.dev/blog/2020/02/03/context-aware-ioreader-for-golang-by-mat-ryer.html,
// has the drawback that we will only examine the context between read
// calls; a long blocking read will not be cancelled.
type readerWithContext struct {
	ctx context.Context
	r   io.Reader
}

func (r *readerWithContext) Read(buf []byte) (int, error) {
	if err := r.ctx.Err(); err != nil {
		return 0, err
	}
	return r.r.Read(buf)
}

func ReadAll(ctx context.Context, r io.Reader) ([]byte, error) {
	buf, err := io.ReadAll(&readerWithContext{ctx: ctx, r: r})
	if err != nil {
		return nil, err
	}
	if err := ctx.Err(); err != nil {
		return nil, err
	}
	return buf, nil
}

func SleepOn(ctx context.Context, c <-chan time.Time) error {
	select {
	case <-c:
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

func Sleep(ctx context.Context, d time.Duration) error {
	t := time.NewTimer(d)
	defer t.Stop()
	return SleepOn(ctx, t.C)
}
