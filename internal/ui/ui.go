package ui

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"time"
)

var ErrTimeout = errors.New("timeout")

// NOTE: On timeout, this function leaks a goroutine which will
// consume some data from the input file, as it becomes available.
// TODO: Implement a proper Read operation with a Context argument.
func HackReadLineWithTimeout(f io.Reader, timeout time.Duration) (string, error) {
	// Capacity one, so that a send after timeout will not block.
	c := make(chan string, 1)

	var readErr error // Shared between goroutines, synchronized via channel close.
	go func() {
		defer close(c)
		scanner := bufio.NewScanner(f)
		if scanner.Scan() {
			c <- scanner.Text()
		} else {
			readErr = scanner.Err()
			if readErr == nil {
				readErr = io.EOF
			}
		}
	}()

	select {
	case response, ok := <-c:
		if !ok {
			return "", fmt.Errorf("reading response failed: %w", readErr)
		}
		return response, nil
	case <-time.After(timeout):
		return "", ErrTimeout
	}
}
