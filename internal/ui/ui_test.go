package ui

import (
	"context"

	"os"
	"sync"
	"time"

	"testing"
)

func TestReadLineWithTimeout(t *testing.T) {
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatal(err)
	}
	defer r.Close()
	defer w.Close()

	var wg sync.WaitGroup
	defer wg.Wait()

	wg.Add(1)
	go func() {
		defer wg.Done()
		time.Sleep(200 * time.Millisecond)
		if _, err := w.WriteString("foo\n"); err != nil {
			t.Logf("WriteString failed: %v", err)
		}
	}()
	got, err := HackReadLineWithTimeout(r, 10*time.Second)
	if err != nil {
		t.Errorf("ReadLineWithTimeout failed: %v", err)
	} else if want := "foo"; got != want {
		t.Errorf("unexpected response: got %q, want %q", got, want)
	}
}

func TestReadLineWithTimeoutTimesOut(t *testing.T) {
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatal(err)
	}
	defer r.Close()
	defer w.Close()

	var wg sync.WaitGroup
	defer wg.Wait()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	wg.Add(1)
	go func() {
		defer wg.Done()

		select {
		case <-time.After(10 * time.Second):
			if _, err := w.WriteString("should not happen\n"); err != nil {
				t.Logf("WriteString failed: %v", err)
			}
		case <-ctx.Done():
		}
	}()
	got, err := HackReadLineWithTimeout(r, 500*time.Millisecond)
	if err != ErrTimeout {
		t.Errorf("ReadLineWithTimeout didn't timeout: got %q, %v", got, err)
	}
}
