package test

import (
	"io/fs"
	"path/filepath"
	"testing/fstest"
)

type MockFS struct {
	inner fstest.MapFS
}

var _ fs.FS = (*MockFS)(nil)

func NewMockFS() *MockFS {
	return &MockFS{inner: make(fstest.MapFS)}
}

func (fs *MockFS) Copy() (*MockFS, error) {
	cpy := NewMockFS()
	for k, v := range fs.inner {
		err := cpy.Add(filepath.Join("/", k), v)
		if err != nil {
			return nil, err
		}
	}

	return cpy, nil
}

func (fs *MockFS) Add(name string, file *fstest.MapFile) error {
	p, err := filepath.Rel("/", name)
	if err != nil {
		return err
	}

	fs.inner[p] = file

	return nil
}

func (fs *MockFS) Remove(name string) error {
	p, err := filepath.Rel("/", name)
	if err != nil {
		return err
	}

	delete(fs.inner, p)

	return nil
}

func (fs *MockFS) Open(name string) (fs.File, error) {
	p, err := filepath.Rel("/", name)
	if err != nil {
		return nil, err
	}

	return fs.inner.Open(p)
}
