package test

import (
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
)

func TestMockFs(t *testing.T) {
	m := NewMockFS()

	err := m.Add("/foo", &fstest.MapFile{Data: []byte("foo")})
	assert.NoError(t, err)
	err = m.Add("/bar", &fstest.MapFile{Data: []byte("bar")})
	assert.NoError(t, err)

	_, err = m.Open("/foo")
	assert.NoError(t, err)

	_, err = m.Open("/bar")
	assert.NoError(t, err)

	_, err = m.Open("/baz")
	assert.Error(t, err)

	_, err = m.Open("foo")
	assert.Error(t, err)

	err = m.Remove("/foo")
	assert.NoError(t, err)
	_, err = m.Open("/foo")
	assert.Error(t, err)

	c, err := m.Copy()
	assert.NoError(t, err)

	_, err = c.Open("/foo")
	assert.Error(t, err)

	_, err = c.Open("/bar")
	assert.NoError(t, err)

	_, err = c.Open("/baz")
	assert.Error(t, err)
}
