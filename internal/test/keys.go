package test

import (
	"crypto/ed25519"
	"crypto/rand"
	"crypto/x509"
	"encoding/pem"
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type Key struct {
	Public  ed25519.PublicKey
	Private ed25519.PrivateKey
	Cert    *x509.Certificate
	Certpem *pem.Block
	Keypem  *pem.Block
}

func MkKeyWithExpiry(t *testing.T, cakey *Key, notBefore, notAfter time.Time) Key {
	t.Helper()

	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	assert.NoError(t, err)

	tmpl := x509.Certificate{
		SignatureAlgorithm: x509.PureEd25519,
		SerialNumber:       big.NewInt(1),
		PublicKey:          pub,
		NotBefore:          notBefore,
		NotAfter:           notAfter,
	}

	if cakey == nil {
		tmpl.IsCA = true
		tmpl.BasicConstraintsValid = true
	}

	var cert []byte
	if cakey == nil {
		cert, err = x509.CreateCertificate(rand.Reader, &tmpl, &tmpl, pub, priv)
	} else {
		cert, err = x509.CreateCertificate(rand.Reader, &tmpl, cakey.Cert, pub, cakey.Private)
	}

	assert.NoError(t, err)

	pkcs8, err := x509.MarshalPKCS8PrivateKey(priv)
	assert.NoError(t, err)

	certpem := &pem.Block{
		Type: "CERTIFICATE", Bytes: cert,
	}
	keypem := &pem.Block{
		Type: "PRIVATE KEY", Bytes: pkcs8,
	}

	certp, err := x509.ParseCertificate(cert)
	assert.NoError(t, err)

	key := Key{
		Public:  pub,
		Private: priv,
		Cert:    certp,
		Certpem: certpem,
		Keypem:  keypem,
	}

	return key
}

func MkKey(t *testing.T, cakey *Key) Key {
	now := time.Now()
	tomorrow := now.Add(24 * time.Hour)

	return MkKeyWithExpiry(t, cakey, now, tomorrow)
}
