package dependency

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBaseRemoval(t *testing.T) {
	f := new(PassthroughFS)

	ff, err := f.Open("/dev/null")
	ff.Close()
	assert.NoError(t, err)

	ff, err = f.Open("//////bin////../bin/sh")
	ff.Close()
	assert.NoError(t, err)

	_, err = f.Open("./dev/null")
	assert.Error(t, err)
}
