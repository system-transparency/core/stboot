package dependency

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/u-root/u-root/pkg/efivarfs"
)

var (
	guid1 = uuid.MustParse("00000000-0000-0000-0000-000000000000")
	guid2 = uuid.MustParse("00000000-0000-0000-0000-000000000001")
)

func setup(t *testing.T) *FakeEfiVars {
	t.Helper()

	var efivars FakeEfiVars
	efivars.Content = make(map[efivarfs.VariableDescriptor][]byte)
	efivars.Attributes = make(map[efivarfs.VariableDescriptor]efivarfs.VariableAttributes)

	for uuidx, uu := range []uuid.UUID{guid1, guid2} {
		for i := 0; i < 10; i++ {
			desc := efivarfs.VariableDescriptor{
				GUID: uu,
				Name: fmt.Sprintf("var%d", i+(10*uuidx)),
			}
			efivars.Content[desc] = []byte(fmt.Sprintf("content%d", i+(10*uuidx)))
			efivars.Attributes[desc] = efivarfs.VariableAttributes(0)
		}
	}

	return &efivars
}

func TestGet(t *testing.T) {
	vars := setup(t)

	desc1 := efivarfs.VariableDescriptor{
		GUID: guid1,
		Name: "var1",
	}

	attr, buf, err := vars.Get(desc1)
	if err != nil {
		t.Errorf("vars.Get(desc1) = %v, want nil", err)
	}

	if string(buf) != "content1" {
		t.Errorf("vars.Get(desc1) = %v, want %v", string(buf), "content1")
	}

	if attr != 0 {
		t.Errorf("vars.Get(desc1) = %v, want %v", attr, 0)
	}

	desc2 := efivarfs.VariableDescriptor{
		GUID: guid2,
		Name: "var1",
	}

	_, _, err = vars.Get(desc2)
	if err == nil {
		t.Errorf("vars.Get(desc2) = %v, want %v", err, efivarfs.ErrVarNotExist)
	}

	delete(vars.Attributes, desc1)

	_, _, err = vars.Get(desc1)
	if err == nil {
		t.Errorf("vars.Get(desc1) = %v, want %v", err, efivarfs.ErrVarNotExist)
	}
}

func TestList(t *testing.T) {
	vars := setup(t)

	descs, err := vars.List()
	if err != nil {
		t.Errorf("vars.List() = %v, want nil", err)
	}

	if len(descs) != 20 {
		t.Errorf("vars.List() = %v, want %v", len(descs), 20)
	}
}

func TestSet(t *testing.T) {
	vars := setup(t)

	desc1 := efivarfs.VariableDescriptor{
		GUID: guid1,
		Name: "var1",
	}

	err := vars.Set(desc1, efivarfs.VariableAttributes(0), []byte("CONTENT1"))
	if err != nil {
		t.Errorf("vars.Set(desc1, efivarfs.VariableAttributes(0), []byte(\"CONTENT1\")) = %v, want nil", err)
	}

	_, buf, err := vars.Get(desc1)
	if err != nil {
		t.Errorf("vars.Get(desc1) = %v, want nil", err)
	}

	if string(buf) != "CONTENT1" {
		t.Errorf("vars.Get(desc1) = %v, want %v", string(buf), "CONTENT1")
	}

	desc1.GUID = guid2

	err = vars.Set(desc1, efivarfs.VariableAttributes(0), []byte("CONTENT1"))
	if err != nil {
		t.Errorf("vars.Set(desc1, efivarfs.VariableAttributes(0), []byte(\"CONTENT1\")) = %v, want nil", err)
	}

	_, buf, err = vars.Get(desc1)
	if err != nil {
		t.Errorf("vars.Get(desc1) = %v, want nil", err)
	}

	if string(buf) != "CONTENT1" {
		t.Errorf("vars.Get(desc1) = %v, want %v", string(buf), "CONTENT1")
	}
}

func TestRemove(t *testing.T) {
	vars := setup(t)

	desc1 := efivarfs.VariableDescriptor{
		GUID: guid1,
		Name: "var1",
	}

	err := vars.Remove(desc1)
	if err != nil {
		t.Errorf("vars.Remove(desc1) = %v, want nil", err)
	}

	_, _, err = vars.Get(desc1)
	if err == nil {
		t.Errorf("vars.Get(desc1) = %v, want %v", err, efivarfs.ErrVarNotExist)
	}

	err = vars.Remove(desc1)
	if err != nil {
		t.Errorf("vars.Remove(desc1) = %v, want nil", err)
	}
}
