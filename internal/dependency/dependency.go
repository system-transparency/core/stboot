package dependency

import (
	"context"
	"errors"
	"io/fs"

	"github.com/u-root/u-root/pkg/efivarfs"
)

type contextKey string

const (
	filesystemContextKey = contextKey("filesystem")
	efivarContextKey     = contextKey("efivar")
	tpmContextKey        = contextKey("tpm")
)

var (
	ErrWrongType = errors.New("wrong type")
)

type FilesystemConstructor func() (fs.FS, error)
type EfiVarsConstructor func() (efivarfs.EFIVar, error)

// FilesystemContext returns a context with the given filesystem.
func WithFilesystem(ctx context.Context, fsys fs.FS) context.Context {
	return context.WithValue(ctx, filesystemContextKey, fsys)
}

// FilesystemFromContext returns the filesystem from the given context.
func Filesystem(ctx context.Context, ctor FilesystemConstructor) (fs.FS, error) {
	maybe := ctx.Value(filesystemContextKey)
	if maybe == nil {
		return ctor()
	}

	ret, ok := maybe.(fs.FS)
	if !ok {
		return nil, ErrWrongType
	}

	return ret, nil
}

func DefaultFilesystem(ctx context.Context) fs.FS {
	fd, err := Filesystem(ctx, func() (fs.FS, error) { return new(PassthroughFS), nil })
	if err != nil {
		panic(err)
	}

	return fd
}

// EFIVarContext returns a context with the given efivar.
func WithEFIVar(ctx context.Context, efivar efivarfs.EFIVar) context.Context {
	return context.WithValue(ctx, efivarContextKey, efivar)
}

// EFIVarFromContext returns the efivar from the given context.
func EFIVar(ctx context.Context, ctor EfiVarsConstructor) (efivarfs.EFIVar, error) {
	maybe := ctx.Value(efivarContextKey)
	if maybe == nil {
		return ctor()
	}

	ret, ok := maybe.(efivarfs.EFIVar)
	if !ok {
		return nil, ErrWrongType
	}

	return ret, nil
}

func DefaultEFIVar(ctx context.Context) (efivarfs.EFIVar, error) {
	return EFIVar(ctx, func() (efivarfs.EFIVar, error) { return efivarfs.New() })
}
