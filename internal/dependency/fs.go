package dependency

import (
	"io/fs"
	"os"
)

type PassthroughFS struct{}

var _ fs.FS = (*PassthroughFS)(nil)

func (*PassthroughFS) Open(name string) (fs.File, error) {
	return os.Open(name)
}
