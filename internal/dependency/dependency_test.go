package dependency

import (
	"context"
	"errors"
	"io/fs"
	"os"
	"path/filepath"
	"testing"
	"testing/fstest"

	"github.com/u-root/u-root/pkg/efivarfs"
)

func TestFilesystem(t *testing.T) {
	ctx := context.Background()
	defaultFsys := new(PassthroughFS)
	ctor := func() (fs.FS, error) { return defaultFsys, nil }

	fsys, err := Filesystem(ctx, ctor)
	if err != nil {
		t.Errorf("Filesystem(ctx, defaultFsys) = %v, want %v", err, nil)
	}

	if fsys != defaultFsys {
		t.Errorf("Filesystem(ctx, defaultFsys) = %v, want %v", fsys, defaultFsys)
	}

	mockFsys := new(fstest.MapFS)
	ctx = WithFilesystem(ctx, mockFsys)

	fsys, err = Filesystem(ctx, ctor)
	if err != nil {
		t.Errorf("Filesystem(ctx, defaultFsys) = %v, want %v", err, nil)
	}

	if fsys != mockFsys {
		t.Errorf("Filesystem(ctx, defaultFsys) = %v, want %v", fsys, mockFsys)
	}
}

func TestDefaultFilesystem(t *testing.T) {
	dir, err := os.MkdirTemp("", "stboot-default-filesystem-test")
	if err != nil {
		t.Fatal(err)
	}

	defer os.RemoveAll(dir)
	t.Logf("dir: %s", dir)

	fname := filepath.Join(dir, "foo")
	if err := os.WriteFile(fname, []byte("bar"), 0600); err != nil {
		t.Fatalf("failed to write test file: %v", err)
	}

	fsys := DefaultFilesystem(context.Background())
	t.Logf("fsys: %#v", fsys)

	data, err := fs.ReadFile(fsys, fname)
	if err != nil {
		t.Fatalf("read failed: %v", err)
	}

	if string(data) != "bar" {
		t.Errorf("unexpected file contents: %q", data)
	}
}

func TestEfiVar(t *testing.T) {
	ctx := context.Background()
	defaultEfiVar := new(FakeEfiVars)
	ctor := func() (efivarfs.EFIVar, error) { return defaultEfiVar, nil }

	efivar, err := EFIVar(ctx, ctor)
	if err != nil {
		t.Errorf("EFIVar(ctx, defaultEfiVar) = %v, want %v", err, nil)
	}

	if efivar != defaultEfiVar {
		t.Errorf("EFIVar(ctx, defaultEfiVar) = %v, want %v", efivar, defaultEfiVar)
	}

	mockEfiVar := new(FakeEfiVars)
	ctx = WithEFIVar(ctx, mockEfiVar)

	efivar, err = EFIVar(ctx, ctor)
	if err != nil {
		t.Errorf("EFIVar(ctx, defaultEfiVar) = %v, want %v", err, nil)
	}

	if efivar != mockEfiVar {
		t.Errorf("EFIVar(ctx, defaultEfiVar) = %v, want %v", efivar, mockEfiVar)
	}
}

func TestFailingCtor(t *testing.T) {
	ctx := context.Background()
	fsCtor := func() (fs.FS, error) { return nil, os.ErrNotExist }

	_, err := Filesystem(ctx, fsCtor)
	if !errors.Is(err, os.ErrNotExist) {
		t.Errorf("Filesystem(ctx, defaultFsys) = %v, want %v", err, os.ErrNotExist)
	}

	efivarCtor := func() (efivarfs.EFIVar, error) { return nil, os.ErrNotExist }

	_, err = EFIVar(ctx, efivarCtor)
	if !errors.Is(err, os.ErrNotExist) {
		t.Errorf("EFIVar(ctx, defaultEfiVar) = %v, want %v", err, os.ErrNotExist)
	}
}

func TestDefaults(t *testing.T) {
	ctx := context.Background()

	t.Run("Filesystem", func(t *testing.T) {
		fsys := DefaultFilesystem(ctx)
		if _, ok := fsys.(*PassthroughFS); !ok {
			t.Errorf("DefaultFilesystem(ctx) = %v, want %v", fsys, new(PassthroughFS))
		}
	})

	t.Run("EFIVar", func(t *testing.T) {
		efivar, err := DefaultEFIVar(ctx)
		if errors.Is(err, efivarfs.ErrNoFS) {
			t.Skip("EFI variables not supported on this system")
		} else {
			if err != nil {
				t.Errorf("DefaultEFIVar(ctx) = %v, want nil", err)
			}

			if efivar == nil {
				t.Errorf("DefaultEFIVar(ctx) = %v, want non-nil", efivar)
			}
		}
	})
}
