package dependency

import (
	"github.com/u-root/u-root/pkg/efivarfs"
)

type FakeEfiVars struct {
	Content    map[efivarfs.VariableDescriptor][]byte
	Attributes map[efivarfs.VariableDescriptor]efivarfs.VariableAttributes
}

func NewFakeEfiVars() *FakeEfiVars {
	return &FakeEfiVars{
		Content:    make(map[efivarfs.VariableDescriptor][]byte),
		Attributes: make(map[efivarfs.VariableDescriptor]efivarfs.VariableAttributes),
	}
}

func (e *FakeEfiVars) Get(desc efivarfs.VariableDescriptor) (efivarfs.VariableAttributes, []byte, error) {
	content, cok := e.Content[desc]
	if !cok {
		return 0, nil, efivarfs.ErrVarNotExist
	}

	attributes, aok := e.Attributes[desc]
	if !aok {
		return 0, nil, efivarfs.ErrVarNotExist
	}

	return attributes, content, nil
}

func (e *FakeEfiVars) List() ([]efivarfs.VariableDescriptor, error) {
	descriptors := make([]efivarfs.VariableDescriptor, 0, len(e.Content))
	for desc := range e.Content {
		descriptors = append(descriptors, desc)
	}

	return descriptors, nil
}

func (e *FakeEfiVars) Remove(desc efivarfs.VariableDescriptor) error {
	delete(e.Content, desc)
	delete(e.Attributes, desc)

	return nil
}

func (e *FakeEfiVars) Set(desc efivarfs.VariableDescriptor, attrs efivarfs.VariableAttributes, data []byte) error {
	e.Content[desc] = data
	e.Attributes[desc] = attrs

	return nil
}

var _ efivarfs.EFIVar = (*FakeEfiVars)(nil)
