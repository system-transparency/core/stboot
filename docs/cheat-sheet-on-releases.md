# Cheat-sheet on releases

This document is intended for maintainers that make releases.

## Checklist

Making a release:

  - [ ] All direct dependencies are up to date, or are explicitly kept
    at older versions we want.  Look for updates with `go list -m -u
    example.org/MODULE`.
  - [ ] README.md, RELEASES.md, and docs/stboot-system.md are up-to-date
  - [ ] Ensure release tests pass, see [test docs](./testing-stboot.md)
  - [ ] Ensure that the stimages/test/getting-started.sh script uses
        relevant versions of stboot and other tools. Check that the
        script works in podman, and that the introductory build guide
        is consistent with this script.
  - [ ] Any additional tests needed for getting confidence in
        compatibility statements in NEWS
  - [ ] After finalizing the release documentation (in particular the
        NEWS file), create a new signed tag. Usually, this means
        incrementing the third number for the most recent tag that was
        used during our interoperability tests
  - [ ] Send announcement email

## RELEASES-file checklist

  - [ ] What in the repository is released and supported
  - [ ] The overall release process is described, e.g., where are releases
    announced, how often do we make releases, what type of releases, etc.
  - [ ] The expectation we as maintainers have on users is described
  - [ ] The expectations users can have on us as maintainers is
    described, e.g., what we intend to (not) break in the future or any
    relevant pointers on how we ensure that things are "working".

## NEWS-file checklist

  - [ ] The previous NEWS entry is for the previous release
  - [ ] Explain what changed
  - [ ] Detailed instructions on how to upgrade on breaking changes
  - [ ] List interoperable repositories and tools, specify commits or tags
  - [ ] List implemented reference specifications, specify commits or tags

## Announcement email template

```
The ST team is happy to announce a new release of the stboot bootloader,
tag v0.X.X, which succeeds the previous release at tag v0.Y.Y. The
source code for this release is available from the git repository:

  git clone -b v0.X.X https://git.glasklar.is/system-transparency/core/stboot.git

Authoritative ST release signing keys are published at
https://www.system-transparency.org/keys, and the tag signature can be
verified using the command

git -c gpg.format=ssh -c gpg.ssh.allowedSignersFile=allowed-ST-release-signers \
  tag --verify v0.X.X

The expectations and intended use of the stboot bootloader is documented
in the repository's RELEASES file.  This RELEASES file also contains
more information concerning the overall release process, see:

  https://git.glasklar.is/system-transparency/core/stboot/-/blob/main/RELEASES.md

Learn about what's new in a release from the repository's NEWS file.  An
excerpt from the latest NEWS-file entry is listed below for convenience.

If you find any bugs, please report them on the System Transparency
discuss list or open an issue on GitLab in the stboot repository:

  https://lists.system-transparency.org/mailman3/postorius/lists/st-discuss.lists.system-transparency.org/
  https://git.glasklar.is/system-transparency/core/stboot/-/issues

Cheers,
The ST team

<COPY-PASTE EXCERPT OF LATEST NEWS FILE ENTRY HERE>
```
