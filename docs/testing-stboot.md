# Testing of stboot

This file documents how stboot is tested:

  1. Unit tests: runs locally in any Go environment (`go test ./...`).
  2. QEMU tests: runs locally provided that needed dependencies are
     available, see `.gitlab-ci.yml` and the scripts in `integration/`.
  3. Hardware tests: similar to the QEMU tests, expect that the scripts
     in `integration/` are adapted for running on real hardware.

Unit tests and (most) QEMU tests are run in the stboot CI.  Real
hardware tests are run manually and less frequently, mainly as part of
release testing on a Supermicro X11SCL-F in Glasklar's lab setup.

Before cutting an stboot release, we check that (1)--(2) work locally
and that (3) additionally works in Glasklar's lab setup.  The remainder
of this document describes the QEMU tests in more detail, and how these
tests are meant to be tweaked for quality assurance on real hardware.

## QEMU tests

The `integration/` subdirectory contains scripts for booting stboot in
QEMU (`qemu-system-x86_64`) using different deployment scenarios.  An
overview of these scripts and deployment scenarios are described below.

A script's test is considered successful if it is possible to reach an
OS-package login prompt.  The password of the root user is "qa".  See
further configuration details at
https://git.glasklar.is/system-transparency/project/qa-images.  The
downloaded OS packages are published at https://st.glasklar.is/st/qa/.

### integration/qemu-boot-from-disk.sh

This script puts an OS package in the same initramfs as stboot,
emulating the way stboot could be booted from an installation on local
disk or from a USB stick. It also creates a separate "stdata" image
intended for local configuration.  However, the "stdata" image is not
used by the OS package in this test (i.e., it is ignored for now).

### integration/qemu-boot-from-net.sh

This script configures stboot to load an OS package from the network,
emulating a system where stboot has been installed on a local disk or
USB stick. The OS package is served using QEMU's guestfwd feature.  This
feature appears broken in some QEMU versions, see `./gitlab-ci.yml`.

### integration/qemu-iso-boot-from-net.sh

This scripts prepares an ISO image with stboot, configured to boot by
fetching an OS package from https://st.glasklar.is/st/qa/. For example,
this emulates the setup of disk-less server systems where the boot media
is a virtual CD ROM drive provided by the server's Baseboard Management
Controller (BMC).

### integration/qemu-provision.sh

This scripts prepares an ISO image with stboot, without any host
config, but including a provisioning OS package. It is booted multiple
times in QEMU, emulating EFI NVRAM variables to keep state between
boots. On initial boot, stboot is expected to boot into the
provisioning OS package, which provisions the system to network boot a
debian OS package. On second boot, stboot is expected to boot into
debian. The script also checks that stboot falls back to the
provisioning OS package when normal boot fails, and that the boot
process gets interrupted when the user presses Ctrl-C.

## Testing on real hardware

Testing on real hardware is more or less a manual process: reuse the
QEMU scripts to get `.img` and `.iso` files, then make them available to
the hardware under test to see if the login prompt is reached or not.

The `.img` files are made available to a [test server][] by plugging in
a physical USB stick.  The `.iso` files are made available to the server
by mounting them as virtual CD ROMs via the server's BMC interface.  The
BMC interface is used for other interactions too, like selecting boot
media in the UEFI menu, restarting the server to boot an image, etc.

To see log messages during boot, delete the `console=ttyS0,115200n8`
argument where the scripts run the `stmgr` tool.  In other words, the
kernel command-line would then be `-cmdline=' -- --loglevel=debug'`.

### Testing boot from local disk (.img)

First run the `integration/qemu-boot-from-disk.sh` script.  This
verifies that building and booting works locally in QEMU.

Edit the stmgr console-setting in the script and rerun. Copy the
resulting image file to a USB stick.  For example, use `dd
if=integration/out/disk.img of=/dev/sdX`.

Insert the USB stick into the test server (requires physical presence),
then reboot and select to boot from the USB stick.  You may need to
enter the UEFI menu settings to select the right boot media.  This and
other input/output is done via the [test server][]'s BMC interface.

The expected result is to get to the OS package's login prompt.  No
network connectivity is needed to reach the login prompt here.

### Testing boot from local disk (.iso)

Use the same procedure as the above `.img` instructions, but instead
produce an ISO image by running this after having invoked the script:

```
(cd integration && ./bin/stmgr uki create \
	-format iso -out out/stboot.iso \
	-kernel=cache/stboot-vmlinuz -initramfs=out/initramfs \
	-cmdline=' -- --loglevel=debug')
```

Make the resulting `integration/out/stboot.iso` available to the test
machine by having the BMC mount it as a virtual CD ROM.  See the [test
server][] instructions for details on how to do that in Glasklar's lab.
Finally, reboot the machine and expect to see the login prompt again.

### Testing network boot (.iso)

Run the `integration/qemu-iso-boot-from-net.sh` script.  This verifies
that building and booting works locally in QEMU.

Edit the console setting in the script and rerun.  Make the resulting
`integration/out/stboot.iso` available to the test server via its BMC.
Finally, restart the machine and boot from the virtual CD ROM.  Expect
to see the login prompt again, but this time as the result of loading
the OS package over a DHCP-configured network.  (The Glasklar lab is
configured to work with DHCP, no need to do additional setup here.)

### Testing network boot with bonding

The script `integration/supermicro-x11scl-bond-iso.sh` creates an ISO
image suitable for booting on the [test server][] on Glasklar's lab
network.  Run it to get an ISO image, then proceed as in the other tests to
mount it via the BMC interface, reboot, etc.  Expect to reach the login
prompt, but this time as a result of network-booting with a static
network configuration that incorporates 802.3ad bonding.  (The Glasklar
lab is configured to work with the static bonding configuration here.)

Note: only the 802.3ad bonding configuration is tested on real hardware,
and there are no tests whatsoever for bonding as part of stboot's CI.

### Testing provisioning mode

The script `integration/supermicro-x11scl-provision-iso.sh` creates an ISO
image without host config but including a provisioning OS package,
similar to the `integration/qemu-provisioning.sh` for QEMU tests.
The script can be run with the `--qemu` options to tweak images
to fit the QEMU environment and run QEMU tests.

Then re-run with no options, to create an image suitable for the [test
server][]. This scripts produces two files that have to be transferred
to the test environment (also listed on the terminal at the end of the
script):

The `out/stboot.iso` file is the ISO image to boot from, to be mounted
via the BMC interface.

The `out/www/debian-package.json` is an OS package descriptor file,
referencing the OS package archive at
<https://st.glasklar.is/st/qa/qa-debian-bookworm-amd64.zip>. The
provisioning done by this test expects this file to be available at
the URL <http://tee.sigsum.org:8000/debian-package.json>. To arrange
this, login on tee, create an otherwise empty directory, and copy the
file there. It can then be served using `python3 -m http.server -d
$DIR`.  Ensure port 8000 is reachable from the test server.

Boot the server, it will attempt to fetch an OS package according to
whatever host config was found in EFI NVRAM. Since the test uses a
newly generated key as ST trust root, boot likely fails, and displays the
prompt asking if you want to enter provisioning mode. Type ENTER on
the console, to start provisioning.

The provisioning OS package writes EFI NVRAM, using the stprov command
with some hard-coded arguments, and then reboots. Reboot is expected to
successfully boot the OS package via the descriptor served by your
web server on tee. Expected result is that the systems boots to a
debian login prompt.

To verify that boot interrupt works, reboot once more, and press
Ctrl-C when stboot says so, to interrupt it during network
configuration or OS package download. Expected result is to get to the
prompt asking about provisioning mode.

[test server]: https://git.glasklar.is/glasklar/services/bootlab/-/blob/main/stime.md
