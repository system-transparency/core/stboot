package stlog

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strings"
	"testing"
)

func TestWithClientTrace(t *testing.T) {
	// NOTE: This test depends on the test host having both IPv4
	// and IPv6 enabled on localhost. If that turns out to be an
	// issue, we could arrange so that the test is skipped if
	// net.LookupHost("localhost") doesn't return the expected
	// addresses.
	listener, err := net.Listen("tcp4", "127.0.0.1:")
	if err != nil {
		t.Fatalf("failed to listen on IPv4 localhost: %v", err)
	}
	defer listener.Close()
	_, port, err := net.SplitHostPort(listener.Addr().String())
	if err != nil {
		t.Fatalf("failed to extract port: %v", err)
	}

	go http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		http.Error(w, "no service", http.StatusServiceUnavailable)
	}))

	buf := divertLog(t, InfoLevel)

	req, err := http.NewRequestWithContext(WithClientTrace(context.Background()), http.MethodGet, fmt.Sprintf("http://localhost:%s/foo.txt", port), nil)
	if err != nil {
		t.Fatalf("failed to create request: %v", err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatalf("failed doing get request: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusServiceUnavailable {
		t.Errorf("unexpected response status code: %s", resp.Status)
	}
	expectedMsgs := []string{
		"Looking up address(es) for",
		"Got addresses:",
		"Attempting to connect to [::1]:" + port,
		"Connecting to [::1]:" + port + " failed",
		"Attempting to connect to 127.0.0.1:" + port,
		"Connected to 127.0.0.1:" + port,
	}
	for i, line := range strings.Split(buf.String(), "\n") {
		if i < len(expectedMsgs) && !strings.Contains(line, expectedMsgs[i]) {
			t.Errorf("missing message %q in log line %d: %q",
				expectedMsgs[i], i, line)
		}
	}
}
