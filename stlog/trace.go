package stlog

import (
	"context"
	"net"
	"net/http/httptrace"
	"strings"
)

// Wrapper for httptrace.WithClientTrace, to add INFO logging of used IP addresses.
func WithClientTrace(ctx context.Context) context.Context {
	if Level() < InfoLevel {
		return ctx
	}
	return httptrace.WithClientTrace(ctx,
		&httptrace.ClientTrace{
			DNSStart: func(info httptrace.DNSStartInfo) {
				Info("Looking up address(es) for %q", info.Host)
			},
			DNSDone: func(info httptrace.DNSDoneInfo) {
				if info.Err != nil {
					Info("DNS lookup failed: %v", info.Err)
				} else {
					Info("Got addresses: %s", joinAddrs(info.Addrs, ", "))
				}
			},
			ConnectStart: func(network, addr string) {
				Info("Attempting to connect to %s", addr)
			},
			ConnectDone: func(network, addr string, err error) {
				if err != nil {
					Info("Connecting to %s failed: %v", addr, err)
				}
			},
			GotConn: func(info httptrace.GotConnInfo) {
				if !info.Reused {
					Info("Connected to %s", info.Conn.RemoteAddr())
				}
			},
		})
}

func joinAddrs(s []net.IPAddr, sep string) string {
	strs := make([]string, len(s))
	for i, x := range s {
		strs[i] = x.String()
	}
	return strings.Join(strs, sep)
}
