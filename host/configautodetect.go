package host

import (
	"context"
	"errors"
	"fmt"
	"io"

	"encoding/json"

	di "system-transparency.org/stboot/internal/dependency"
	"system-transparency.org/stboot/stlog"
)

// Errors which may be raised and wrapped in this package.
var (
	ErrConfigNotFound = errors.New("no host configuration found")
)

// Sources used by ConfigAutodetect.
const (
	HostConfigInitrdPath = "/etc/host_configuration.json"
	HostConfigEFIVarName = "STHostConfig-f401f2c1-b005-4be0-8cee-f2e5945bcbe7"
)

type configLoader interface {
	probe(context.Context) (io.Reader, error)
	info() string
}

// ConfigAutodetect looks for a known host configuration name in following order:
// - inside the initramfs at HostConfigInitrdPath
// - at the efivar filesystem for HostConfigEFIVarName
//
// On success, returns the Config. Returns ErrConfigNotFound if no host configuration is found.
func ConfigAutodetect(ctx context.Context) (Config, error) {
	var loadingOrder = []configLoader{
		&initramfs{},
		&efivar{},
	}

	stlog.Debug("Host configuration autodetect")

	for _, loader := range loadingOrder {
		stlog.Debug(loader.info())

		r, err := loader.probe(ctx)
		if err != nil {
			stlog.Debug(err.Error())

			continue
		}

		var hostCfg Config
		if err := json.NewDecoder(r).Decode(&hostCfg); err != nil {
			return Config{}, err
		}
		return hostCfg, nil
	}

	return Config{}, ErrConfigNotFound
}

type initramfs struct{}

var _ configLoader = &initramfs{}

func (i *initramfs) probe(ctx context.Context) (io.Reader, error) {
	fsys := di.DefaultFilesystem(ctx)

	ret, err := fsys.Open(HostConfigInitrdPath)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

func (i *initramfs) info() string {
	return fmt.Sprintf("Probing initramfs at %s", HostConfigInitrdPath)
}

type efivar struct{}

var _ configLoader = &efivar{}

func (e *efivar) probe(ctx context.Context) (io.Reader, error) {
	ret, err := readEFIVar(ctx, HostConfigEFIVarName)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

func (e *efivar) info() string {
	return fmt.Sprintf("Probing EFI variable %s", HostConfigEFIVarName)
}
