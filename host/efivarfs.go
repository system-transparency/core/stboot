package host

import (
	"bytes"
	"context"
	"fmt"

	"github.com/u-root/u-root/pkg/efivarfs"
	di "system-transparency.org/stboot/internal/dependency"
)

func readEFIVar(ctx context.Context, name string) (*bytes.Reader, error) {
	efiVars, err := di.DefaultEFIVar(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to open efivar fs: %w", err)
	}

	_, r, err := efivarfs.SimpleReadVariable(efiVars, name)
	if err != nil {
		return nil, fmt.Errorf("failed to read efi variable %q: %w", name, err)
	}

	return r, nil
}
