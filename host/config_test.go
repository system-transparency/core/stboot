// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package host exposes functionality to interact with the host mashine.
package host

import (
	"encoding/json"
	"errors"
	"net"
	"net/url"
	"reflect"
	"strings"
	"testing"

	"github.com/vishvananda/netlink"
)

func TestIPAddrModeString(t *testing.T) {
	tests := []struct {
		name string
		mode IPAddrMode
		want string
	}{
		{
			name: "String for default value",
			mode: IPUnset,
			want: "unset",
		},
		{
			name: "String for 'IPStatic'",
			mode: IPStatic,
			want: "static",
		},
		{
			name: "String for 'IPDynamic'",
			mode: IPDynamic,
			want: "dhcp",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.mode.String()
			assert(t, nil, nil, got, tt.want)
		})
	}
}

func TestIPAddrModeMarshal(t *testing.T) {
	tests := []struct {
		name string
		mode IPAddrMode
		want string
	}{
		{
			name: "IPUnset",
			mode: IPUnset,
			want: `null`,
		},
		{
			name: "IPStatic",
			mode: IPStatic,
			want: `"static"`,
		},
		{
			name: "IPDynamic",
			mode: IPDynamic,
			want: `"dhcp"`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.mode.MarshalJSON()
			assert(t, err, nil, string(got), tt.want)
		})
	}
}

func TestIPAddrModeUnmarshal(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    IPAddrMode
		errType error
	}{
		{
			name:    Null,
			json:    `null`,
			want:    IPUnset,
			errType: nil,
		},
		{
			name:    "static",
			json:    `"static"`,
			want:    IPStatic,
			errType: nil,
		},
		{
			name:    "dhcp",
			json:    `"dhcp"`,
			want:    IPDynamic,
			errType: nil,
		},
		{
			name:    "wrong type",
			json:    `1`,
			want:    IPUnset,
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "invalid string",
			json:    `"foo"`,
			want:    IPUnset,
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "invalid empty string",
			json:    `""`,
			want:    IPUnset,
			errType: &json.UnmarshalTypeError{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got IPAddrMode
			err := got.UnmarshalJSON([]byte(tt.json))
			assert(t, err, tt.errType, got, tt.want)
		})
	}
}

func TestNetworkInterfaceUnmarshalJSON(t *testing.T) {
	allSet := *(*s2sNetworkInterfaces(t, [][2]string{{"eth0", "aa:bb:cc:dd:ee:ff"}}))[0]
	for _, table := range []struct {
		name    string
		ni      NetworkInterface
		json    string
		wantErr error
	}{
		{"invalid: only ifname", NetworkInterface{}, `{"interface_name":"eth0","mac_address":null}`, errors.New("")},
		{"invalid: only ifaddr", NetworkInterface{}, `{"interface_name":null,"mac_address":"aa:bb:cc:dd:ee:ff"}`, errors.New("")},
		{"valid", allSet, `{"interface_name":"eth0","mac_address":"aa:bb:cc:dd:ee:ff"}`, nil},
	} {
		t.Run(table.name, func(t *testing.T) {
			var ni NetworkInterface
			err := json.Unmarshal([]byte(table.json), &ni)
			assert(t, err, table.wantErr, ni, table.ni)
		})
	}
}

func TestNetworkInterfaceMarshalJSON(t *testing.T) {
	t.Run("have nulls", func(t *testing.T) {
		got, err := json.Marshal(NetworkInterface{})
		assert(t, err, nil, `{"interface_name":null,"mac_address":null}`, string(got))
	})
	t.Run("all set", func(t *testing.T) {
		got, err := json.Marshal(*(*s2sNetworkInterfaces(t, [][2]string{{"eth0", "aa:bb:cc:dd:ee:ff"}}))[0])
		assert(t, err, nil, `{"interface_name":"eth0","mac_address":"aa:bb:cc:dd:ee:ff"}`, string(got))
	})
}

func TestConfigMarshalJSON(t *testing.T) {
	tests := []struct {
		name string
		c    Config
		want string
	}{
		{
			name: "Unset Config",
			c:    Config{},
			want: `{
				"network_mode":null,
				"host_ip":null,
				"gateway":null,
				"dns":null,
				"network_interfaces":null,
				"ospkg_pointer":null,
				"bonding_mode":null,
				"bond_name":null
				}`,
		},
		{
			name: "All fields set",
			c: Config{
				IPAddrMode:        ipam2ipam(t, IPStatic),
				HostIP:            s2cidr(t, "127.0.0.1/24"),
				DefaultGateway:    s2ip(t, "127.0.0.1"),
				DNSServer:         s2ipArray(t, "127.0.0.1"),
				OSPkgPointer:      s2s(t, "http://foo.com/bar,https://foo.com/bar"),
				NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"eth0", "00:00:5e:00:53:01"}}),
				BondingMode:       BondingBalanceRR,
				BondName:          s2s(t, "bond0"),
				Description:       s2s(t, "example"),
			},
			want: `{
				"network_mode":"static",
				"host_ip":"127.0.0.1/24",
				"gateway":"127.0.0.1",
				"dns":["127.0.0.1"],
				"network_interfaces":[{"interface_name": "eth0", "mac_address": "00:00:5e:00:53:01" }],
				"ospkg_pointer":"http://foo.com/bar,https://foo.com/bar",
				"bonding_mode":"balance-rr",
				"bond_name":"bond0",
                                "description":"example"
				}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.c.MarshalJSON()
			want := strings.Join(strings.Fields(tt.want), "")
			assert(t, err, nil, string(got), want)
		})
	}
}

func TestConfigUnmarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    Config
		errType error
	}{
		// Valid configurations
		{
			name: "Minimal for dhcp",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com"
			}`,
			want: Config{
				IPAddrMode:   ipam2ipam(t, IPDynamic),
				OSPkgPointer: s2s(t, "http://server.com"),
			},
			errType: nil,
		},
		{
			name: "Minimal for static IP",
			json: `{
				"network_mode":"static",
				"host_ip":"127.0.0.1/24",
				"gateway":"127.0.0.1",
				"ospkg_pointer":"http://server.com"
			}`,
			want: Config{
				IPAddrMode:     ipam2ipam(t, IPStatic),
				HostIP:         s2cidr(t, "127.0.0.1/24"),
				DefaultGateway: s2ip(t, "127.0.0.1"),
				OSPkgPointer:   s2s(t, "http://server.com"),
			},
			errType: nil,
		},
		{
			name: "All set",
			json: `{
				"network_mode":"static",
				"host_ip":"127.0.0.1/24",
				"gateway":"127.0.0.1",
				"dns":["127.0.0.1","127.0.0.2"],
				"ospkg_pointer":"http://server-a.com,https://server-b.com",
				"network_interfaces":[{"interface_name": "eth0", "mac_address": "00:00:5e:00:53:01"},{"interface_name": "eth1", "mac_address": "00:00:5e:00:53:02"}],
				"bonding_mode":"balance-rr",
				"bond_name":"bond0"
			}`,
			want: Config{
				IPAddrMode:        ipam2ipam(t, IPStatic),
				HostIP:            s2cidr(t, "127.0.0.1/24"),
				DefaultGateway:    s2ip(t, "127.0.0.1"),
				OSPkgPointer:      s2s(t, "http://server-a.com,https://server-b.com"),
				DNSServer:         s2ipArray(t, "127.0.0.1", "127.0.0.2"),
				NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"eth0", "00:00:5e:00:53:01"}, {"eth1", "00:00:5e:00:53:02"}}),
				BondName:          s2s(t, "bond0"),
				BondingMode:       BondingBalanceRR,
			},
			errType: nil,
		},
		{
			name: "Backwards-compatibility: dns, provisioning_urls, network_interface",
			json: `{
				"network_mode":"dhcp",
				"provisioning_urls":["http://foo.example.org","http://bar.example.org"],
				"dns":"127.0.0.1",
				"network_interface":"aa:bb:cc:dd:ee:ff"
			}`,
			want: Config{
				IPAddrMode:        ipam2ipam(t, IPDynamic),
				OSPkgPointer:      s2s(t, "http://foo.example.org,http://bar.example.org"),
				DNSServer:         s2ipArray(t, "127.0.0.1"),
				NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"", "aa:bb:cc:dd:ee:ff"}}),
			},
			errType: nil,
		},
		{
			name: "Backwards-compatibility: network_interfaces",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://example.org",
				"network_interfaces":["eth0","eth1"],
				"bonding_mode":"balance-rr",
				"bond_name":"bond0"
			}`,
			want: Config{
				IPAddrMode:        ipam2ipam(t, IPDynamic),
				OSPkgPointer:      s2s(t, "http://example.org"),
				NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"eth0", ""}, {"eth1", ""}}),
				BondingMode:       BondingBalanceRR,
				BondName:          s2s(t, "bond0"),
			},
			errType: nil,
		},
		// Invalid configurations
		{
			name:    "Bad JSON",
			json:    `bad json`,
			want:    Config{},
			errType: &json.SyntaxError{},
		},
		{
			name: "Missing network mode",
			json: `{
				"ospkg_pointer":"http://server.com"
			}`,
			want:    Config{},
			errType: errors.New(""),
		},
		{
			name: "Missing OSPkgPointer",
			json: `{
				"network_mode":"dhcp"
			}`,
			want:    Config{},
			errType: errors.New(""),
		},
		{
			name: "Empty OSPkgPointer",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":""
			}`,
			want:    Config{},
			errType: errors.New(""),
		},
		{
			name: "IPAddrMode=static but unset HostIP",
			json: `{
				"network_mode":"static",
				"gateway":"127.0.0.1",
				"ospkg_pointer":"http://server.com"
			}`,
			want:    Config{},
			errType: errors.New(""),
		},
		{
			name: "IPAddrMode=static but unset DefaultGateway",
			json: `{
				"network_mode":"static",
				"host_ip":"127.0.0.1/24",
				"ospkg_pointer":"http://server.com"
			}`,
			want:    Config{},
			errType: errors.New(""),
		},
		{
			name: "Invalid DNS",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"dns":123,
			}`,
			want:    Config{},
			errType: ErrMissingNetworkInterfaces,
		},
		{
			name: "Bad bonding mode 1",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"network_interfaces":[{"interface_name": "eth0", "mac_address": "00:00:5e:00:53:01"},{"interface_name": "eth1", "mac_address": "00:00:5e:00:53:02"}],
				"bonding_mode":"invalid"
			}`,
			want:    Config{},
			errType: ErrInvalidBondMode,
		},
		{
			name: "Bad bonding mode 2",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"network_interfaces":[{"interface_name": "eth0", "mac_address": "00:00:5e:00:53:01"},{"interface_name": "eth1", "mac_address": "00:00:5e:00:53:02"}],
				"bonding_mode":123
			}`,
			want:    Config{},
			errType: ErrInvalidBondMode,
		},
		{
			name: "Missing bond name field",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"network_interfaces":[{"interface_name": "eth0", "mac_address": "00:00:5e:00:53:01"},{"interface_name": "eth1", "mac_address": "00:00:5e:00:53:02"}],
				"bonding_mode":"balance-rr"
			}`,
			want:    Config{},
			errType: ErrMissingBondName,
		},
		{
			name: "No network interfaces to bond",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"bonding_mode":"balance-rr",
				"bond_name":"bond0"
			}`,
			want:    Config{},
			errType: ErrMissingNetworkInterfaces,
		},
		{
			name: "Invalid network interface type",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"network_interfaces":[1,2]
			}`,
			want:    Config{},
			errType: ErrMissingNetworkInterfaces,
		},
		{
			name: "No interface",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"network_interfaces":[]
			}`,
			want:    Config{},
			errType: ErrEmptyNetworkInterfaces,
		},
		{
			name: "Invalid MAC address",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"network_interfaces":[{"interface_name": "eth0", "mac_address": "00;00:5e:00:53:01"},{"interface_name": "eth1", "mac_address": "00:00:5e:00:53:02"}]
			}`,
			want:    Config{},
			errType: ErrMissingNetworkInterfaces,
		},
		{
			name: "Fail valid configuration due to invalid compat provisioning URLs",
			json: `{
				"network_mode":"dhcp",
				"ospkg_pointer":"http://server.com",
				"provisioning_urls":123
			}`,
			errType: &json.SyntaxError{},
		},
		{
			name: "Fail to fallback on compat provisioning URLs containing comma",
			json: `{
				"network_mode":"dhcp",
				"provisioning_urls":["http://foo.example.org,http://bar.example.org"]
			}`,
			errType: errors.New(""),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got Config
			err := got.UnmarshalJSON([]byte(tt.json))

			assert(t, err, tt.errType, got, tt.want)
		})
	}
}

func TestInvalidBondingMode(t *testing.T) {
	cfg := Config{
		IPAddrMode:        ipam2ipam(t, IPDynamic),
		OSPkgPointer:      s2s(t, "http://server.com"),
		BondName:          s2s(t, "bond0"),
		BondingMode:       BondingUnknown,
		NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"eth0", "00:00:5e:00:53:01"}, {"eth1", "00:00:5e:00:53:02"}}),
	}

	err := checkBonding(&cfg)
	if !errors.Is(err, ErrInvalidBondMode) {
		t.Fatalf("Expected ErrInvalidBondMode, got %v", err)
	}
}

func TestNetlinkAddrModeMarshal(t *testing.T) {
	tests := []struct {
		name string
		addr netlinkAddr
		want string
	}{
		{
			name: "Valid IPv4 CIDR",
			addr: netlinkAddr(*s2cidr(t, "192.0.2.0/24")),
			want: `"192.0.2.0/24"`,
		},
		{
			name: "Valid IPv6 CIDR ",
			addr: netlinkAddr(*s2cidr(t, "2001:db8::/32")),
			want: `"2001:db8::/32"`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.addr.MarshalJSON()
			assert(t, err, nil, string(got), tt.want)
		})
	}
}

func TestNetlinkAddrUnmarshal(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    netlinkAddr
		errType error
	}{
		{
			name:    Null,
			json:    `null`,
			want:    netlinkAddr{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "IPv4",
			json:    `"192.0.2.0/24"`,
			want:    netlinkAddr(*s2cidr(t, "192.0.2.0/24")),
			errType: nil,
		},
		{
			name:    "IPv6",
			json:    `"2001:db8::/32"`,
			want:    netlinkAddr(*s2cidr(t, "2001:db8::/32")),
			errType: nil,
		},
		{
			name:    "Missing mask",
			json:    `"192.0.2.0"`,
			want:    netlinkAddr{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Invalid type",
			json:    `192`,
			want:    netlinkAddr{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Invalid string",
			json:    `"192-0-2-0"`,
			want:    netlinkAddr{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Empty string",
			json:    `""`,
			want:    netlinkAddr{},
			errType: &json.UnmarshalTypeError{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got netlinkAddr
			err := got.UnmarshalJSON([]byte(tt.json))
			assert(t, err, tt.errType, got, tt.want)
		})
	}
}

func TestNetIPMarshal(t *testing.T) {
	tests := []struct {
		name string
		ip   netIP
		want string
	}{
		{
			name: "IPv4",
			ip:   netIP(*s2ip(t, "192.0.2.0")),
			want: `"192.0.2.0"`,
		},
		{
			name: "IPv6",
			ip:   netIP(*s2ip(t, "2001:db8::")),
			want: `"2001:db8::"`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.ip.MarshalJSON()
			assert(t, err, nil, string(got), tt.want)
		})
	}
}

func TestNetIPUnmarshal(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    netIP
		errType error
	}{
		{
			name:    Null,
			json:    `null`,
			want:    nil,
			errType: nil,
		},
		{
			name:    "IPv4",
			json:    `"192.0.2.0"`,
			want:    netIP(*s2ip(t, "192.0.2.0")),
			errType: nil,
		},
		{
			name:    "IPv6",
			json:    `"2001:db8::"`,
			want:    netIP(*s2ip(t, "2001:db8::")),
			errType: nil,
		},
		{
			name:    "Invalid type",
			json:    `192`,
			want:    nil,
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Invalid string",
			json:    `"192-0-2-0"`,
			want:    nil,
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Empty string",
			json:    `""`,
			want:    nil,
			errType: &json.UnmarshalTypeError{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got netIP
			err := got.UnmarshalJSON([]byte(tt.json))
			assert(t, err, tt.errType, got, tt.want)
		})
	}
}

func TestNetHardwareAddrMarshal(t *testing.T) {
	tests := []struct {
		name string
		addr netHardwareAddr
		want string
	}{
		{
			name: "Valid",
			addr: netHardwareAddr(*s2mac(t, "00:00:5e:00:53:01")),
			want: `"00:00:5e:00:53:01"`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.addr.MarshalJSON()
			assert(t, err, nil, string(got), tt.want)
		})
	}
}

func TestNetHardwareAddrUnmarshal(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    netHardwareAddr
		errType error
	}{
		{
			name:    Null,
			json:    `null`,
			want:    nil,
			errType: nil,
		},
		{
			name:    "Valid colon",
			json:    `"00:00:5e:00:53:01"`,
			want:    netHardwareAddr(*s2mac(t, "00:00:5e:00:53:01")),
			errType: nil,
		},
		{
			name:    "Valid dash",
			json:    `"00-00-5e-00-53-01"`,
			want:    netHardwareAddr(*s2mac(t, "00-00-5e-00-53-01")),
			errType: nil,
		},
		{
			name:    "Invalid type",
			json:    `0`,
			want:    nil,
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Invalid string",
			json:    `"00005e005301"`,
			want:    nil,
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Empty string",
			json:    `""`,
			want:    nil,
			errType: &json.UnmarshalTypeError{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got netHardwareAddr
			err := got.UnmarshalJSON([]byte(tt.json))
			assert(t, err, tt.errType, got, tt.want)
		})
	}
}

func TestUrlURLMarshal(t *testing.T) {
	tests := []struct {
		name string
		u    urlURL
		want string
	}{
		{
			name: "Valid",
			u:    urlURL(*s2url(t, "http://server.com")),
			want: `"http://server.com"`,
		},
		{
			name: "Valid with variable",
			u:    urlURL(*s2url(t, "http://server.com/$ID/foo")),
			want: `"http://server.com/$ID/foo"`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.u.MarshalJSON()
			assert(t, err, nil, string(got), tt.want)
		})
	}
}

func TestUrlURLUnmarshal(t *testing.T) {
	tests := []struct {
		name    string
		json    string
		want    urlURL
		errType error
	}{
		{
			name:    Null,
			json:    `null`,
			want:    urlURL{},
			errType: nil,
		},
		{
			name:    "Valid",
			json:    `"http://server.com"`,
			want:    urlURL(*s2url(t, "http://server.com")),
			errType: nil,
		},
		{
			name:    "Valid with variable",
			json:    `"http://server.com/$ID/foo"`,
			want:    urlURL(*s2url(t, "http://server.com/$ID/foo")),
			errType: nil,
		},
		{
			name:    "Missing scheme",
			json:    `"server.com"`,
			want:    urlURL{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Invalid type",
			json:    `0`,
			want:    urlURL{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Invalid string",
			json:    `"00005e005301"`,
			want:    urlURL{},
			errType: &json.UnmarshalTypeError{},
		},
		{
			name:    "Empty string",
			json:    `""`,
			want:    urlURL{},
			errType: &json.UnmarshalTypeError{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got urlURL
			err := got.UnmarshalJSON([]byte(tt.json))
			assert(t, err, tt.errType, got, tt.want)
		})
	}
}

func TestChecktNetworkInterfaces(t *testing.T) {
	t.Run("no interfaces", func(t *testing.T) {
		cfg := Config{NetworkInterfaces: s2sNetworkInterfaces(t, nil)}
		assert(t, checkNetworkInterfaces(&cfg), ErrEmptyNetworkInterfaces, cfg, cfg)
	})
	t.Run("assert invalid compat interfaces", func(t *testing.T) {
		cfg := Config{NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"eth0", ""}})}
		assert(t, checkNetworkInterfaces(&cfg), errors.New(""), cfg, cfg)
	})
	t.Run("valid", func(t *testing.T) {
		cfg := Config{NetworkInterfaces: s2sNetworkInterfaces(t, [][2]string{{"eth0", "aa:bb:cc:dd:ee:ff"}})}
		assert(t, checkNetworkInterfaces(&cfg), nil, cfg, cfg)
	})
}

func assert(t *testing.T, gotErr error, wantErrType, got, want interface{}) {
	t.Helper()

	if wantErrType != nil {
		if gotErr == nil {
			t.Fatal("expect an error")
		}

		ok := errors.As(gotErr, &wantErrType)
		if !ok {
			t.Fatalf("%+v does not wrap expected %+v", gotErr, wantErrType)
		}
	} else if gotErr != nil {
		t.Fatalf("unexpected error: %v", gotErr)
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %+v, want %+v", got, want)
	}
}

func s2cidr(t *testing.T, s string) *netlink.Addr {
	t.Helper()

	ip, err := netlink.ParseAddr(s)
	if err != nil {
		t.Fatal(err)
	}

	return ip
}

func s2ip(t *testing.T, s string) *net.IP {
	t.Helper()

	ip := net.ParseIP(s)
	if ip == nil {
		t.Fatalf("error parsing %s to net.IP", s)
	}

	return &ip
}

func s2ipArray(t *testing.T, s ...string) *[]*net.IP {
	t.Helper()

	a := []*net.IP{}

	if len(s) == 0 {
		t.Fatal("no string provided")
	}

	for _, str := range s {
		i := s2ip(t, str)
		a = append(a, i)
	}

	return &a
}

func s2netIPArray(t *testing.T, s ...string) *[]*netIP {
	t.Helper()

	var ips []*netIP
	for _, ip := range *s2ipArray(t, s...) {
		ips = append(ips, (*netIP)(ip))
	}
	return &ips
}

func s2mac(t *testing.T, s string) *net.HardwareAddr {
	t.Helper()

	mac, err := net.ParseMAC(s)
	if err != nil {
		t.Fatal(err)
	}

	return &mac
}

func s2url(t *testing.T, s string) *url.URL {
	t.Helper()

	u, err := url.Parse(s)
	if err != nil {
		t.Fatal(err)
	}

	return u
}

func s2s(t *testing.T, s string) *string {
	t.Helper()

	return &s
}

func ipam2ipam(t *testing.T, m IPAddrMode) *IPAddrMode {
	t.Helper()

	return &m
}

func s2sNetworkInterfaces(t *testing.T, s [][2]string) *[]*NetworkInterface {
	t.Helper()

	var n []*NetworkInterface
	for _, i := range s {
		var ni NetworkInterface
		if name := i[0]; name != "" {
			ni.InterfaceName = s2s(t, name)
		}
		if mac := i[1]; mac != "" {
			ni.MACAddress = s2mac(t, mac)
		}
		n = append(n, &ni)
	}

	return &n
}
