package host

//
// This file provides types, methods, and functions that help us manage the
// complexity of providing backwards-compatibility in our host configuration.
//

import (
	"encoding/json"
	"fmt"
	"net"
	"strings"
)

// compatDNSServers is a list of IP addresses.  A separate type is needed to
// keep JSON unmarshalling backwards-compatible by first trying to unmarshal as
// list of strings (e.g., ["1.2.3.4", "2.3.4.5"]) and if that fails fallback on
// unmarshalling as a string (e.g., "3.4.5.6" to get ["3.4.5.6"]).
//
// See git-commit 8d38269e88242a714edb1fd8623950900c40205c for more details on
// the old "dns" field that we want to have backwards compatibility with.
type compatDNSServers []*netIP

func (d *compatDNSServers) UnmarshalJSON(data []byte) error {
	// The below type cast is needed to avoid recursively calling this method.
	// It also needs to be a pointer for json.Unmarshal to function correctly.
	if err := json.Unmarshal(data, (*[]*netIP)(d)); err != nil {
		var ip netIP
		if errInner := json.Unmarshal(data, &ip); errInner != nil {
			return err // always use the original error if fallback fails
		}
		*d = compatDNSServers{&ip}
	}
	return nil
}

// maybeCompatProvisioningURLs checks whether it is reasonable to fallback on
// the obsolete "provisioning_urls" field instead of today's "ospkg_pointer".
// For example, `"provisioning_urls":["foo","bar"]` would result in the same
// behavior as if `"ospkg_pointer":"foo,bar"` had been found while parsing.
//
// See git-commit 9b8fc85fd7f041663c7a76b9ffe211375ef96b1b for more details on
// the old "provisioning_urls" field that we want backwards-compatibility with.
func (cfg *config) maybeCompatProvisioningURLs() error {
	if cfg.OSPkgPointer != nil {
		return nil // always prefer a correctly configured "ospkg_pointer"
	}
	if len(cfg.CompatProvisiongURLs) == 0 {
		return nil // prefer missing "ospkg_pointer" errror over missing "provisioning_urls"
	}

	for _, url := range cfg.CompatProvisiongURLs {
		if strings.Contains(url, ",") {
			return fmt.Errorf("failed to fallback on \"provisioning_urls\": comma is not allowed in \"ospkg_pointer\"")
		}
	}
	ospkgPointer := strings.Join(cfg.CompatProvisiongURLs, ",")
	cfg.OSPkgPointer = &ospkgPointer
	return nil
}

// compatNetworkInterfaces is a list of interfaces.  A separate type is needed
// to keep JSON unmarshalling backwards-compatible by translating the old format
// ["eth0"] to the new format [{"interface_name":"eth0","mac_address":null}].
//
// See git-commit 334d268333f0ed7a4bda02cff561c07c7ed3c269 for more details on
// the old "network_interfaces" field we want backwards-compatibility with.
type compatNetworkInterfaces []*NetworkInterface

func (nics *compatNetworkInterfaces) UnmarshalJSON(data []byte) error {
	// The below type cast is needed to avoid recursively calling this method.
	// It also needs to be a pointer for json.Unmarshal to function correctly.
	if err := json.Unmarshal(data, (*[]*NetworkInterface)(nics)); err != nil {
		var ifnames []string
		if errInner := json.Unmarshal(data, &ifnames); errInner != nil {
			return err // always output the original error if fallback fails
		}
		var interfaces []*NetworkInterface
		for _, ifname := range ifnames {
			ifname := ifname // ensure we get a new ptr addr every iteration
			interfaces = append(interfaces, &NetworkInterface{InterfaceName: &ifname})
		}
		*nics = interfaces
	}
	return nil
}

// maybeCompatNetworkInterface checks whether it is reasonable to fallback on
// the obsolete "network_interface" field.  The newer "network_interfaces" field
// always takes presedence if explicitly set to a non-null value.
func (cfg *config) maybeCompatNetworkInterface() {
	if cfg.NetworkInterfaces != nil {
		return // "network_interfaces" always takes presedence
	}
	if cfg.CompatNetworkInterface == nil {
		return // nothing to fallback on
	}

	mac := net.HardwareAddr(cfg.CompatNetworkInterface)
	nic := NetworkInterface{MACAddress: &mac}
	cfg.NetworkInterfaces = &compatNetworkInterfaces{&nic}
}

// checkCompatNetworkInterfaces checks that bonding configurations have
// interface names, and that non-bonding configurations have MAC addresses.
func (cfg *Config) checkCompatNetworkInterfaces() error {
	for _, nic := range *cfg.NetworkInterfaces {
		if cfg.BondingMode == BondingUnset && nic.MACAddress == nil {
			return fmt.Errorf("failed to fallback on obsolete \"network_interfaces\" with bonding disabled")
		}
		if cfg.BondingMode != BondingUnset && nic.InterfaceName == nil {
			return fmt.Errorf("failed to fallback on \"network_interface\" with bonding enabled")
		}
	}
	return nil
}
