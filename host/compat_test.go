package host

import (
	"encoding/json"
	"errors"
	"net"
	"testing"
)

func TestCompatDNSServersUnmarshal(t *testing.T) {
	for _, table := range []struct {
		name    string
		json    string
		want    compatDNSServers
		errType error
	}{
		{"invalid", `"foo"`, nil, &json.UnmarshalTypeError{}},
		{"valid: backwards-compatible", `"10.0.2.20"`, *s2netIPArray(t, "10.0.2.20"), nil},
		{"valid: current specification", `["10.0.2.20","10.0.2.21"]`, *s2netIPArray(t, "10.0.2.20", "10.0.2.21"), nil},
	} {
		t.Run(table.name, func(t *testing.T) {
			var got compatDNSServers
			err := json.Unmarshal([]byte(table.json), &got)
			assert(t, err, table.errType, got, table.want)
		})
	}
}

func TestMaybeCompatProvisioningURLs(t *testing.T) {
	for _, table := range []struct {
		name                 string
		cfg                  config
		wantOSPackagePointer *string
		err                  error
	}{
		{"invalid: malformed URLs", config{CompatProvisiongURLs: []string{"foo,bar"}}, nil, errors.New("")},
		{"valid: prefer ospkg pointer", config{OSPkgPointer: s2s(t, "foo"), CompatProvisiongURLs: []string{"bar"}}, s2s(t, "foo"), nil},
		{"valid: unable to fallback", config{}, nil, nil},
		{"valid: able to fallback",
			config{CompatProvisiongURLs: []string{"http://foo.example.org", "http://bar.example.org"}},
			s2s(t, "http://foo.example.org,http://bar.example.org"), nil},
	} {
		t.Run(table.name, func(t *testing.T) {
			err := table.cfg.maybeCompatProvisioningURLs()
			assert(t, err, table.err, table.cfg.OSPkgPointer, table.wantOSPackagePointer)
		})
	}
}

func TestCompatNetworkInterfacesUnmarshal(t *testing.T) {
	for _, table := range []struct {
		name    string
		json    string
		want    compatNetworkInterfaces
		errType error
	}{
		{"invalid: neither old nor new", `123`, nil, &json.UnmarshalTypeError{}},
		{"valid: empty list", `[]`, compatNetworkInterfaces{}, nil},
		{"valid: backwards-compatible", `["eth1","eth2"]`, *s2sNetworkInterfaces(t, [][2]string{{"eth1", ""}, {"eth2", ""}}), nil},
		{"valid: current specification", `[{"interface_name":"eth1","mac_address":"aa:bb:cc:dd:ee:ff"}]`, *s2sNetworkInterfaces(t, [][2]string{{"eth1", "aa:bb:cc:dd:ee:ff"}}), nil},
	} {
		t.Run(table.name, func(t *testing.T) {
			var got compatNetworkInterfaces
			err := json.Unmarshal([]byte(table.json), &got)
			assert(t, err, table.errType, got, table.want)
		})
	}
}

func TestMaybeCompatNetworkInterface(t *testing.T) {
	addr1 := net.HardwareAddr{0x11, 0x11, 0x11, 0x11, 0x11, 0x11}
	addr2 := net.HardwareAddr{0x22, 0x22, 0x22, 0x22, 0x22, 0x22}
	nics1 := compatNetworkInterfaces{&NetworkInterface{MACAddress: &addr1}}
	nics2 := compatNetworkInterfaces{&NetworkInterface{MACAddress: &addr2}}
	for _, table := range []struct {
		name                  string
		cfg                   config
		wantNetworkInterfaces *compatNetworkInterfaces
	}{
		{"valid: don't fallback", config{NetworkInterfaces: &nics1, CompatNetworkInterface: netHardwareAddr(addr2)}, &nics1},
		{"valid: nothing to fallback on", config{}, nil},
		{"valid: fallback", config{CompatNetworkInterface: netHardwareAddr(addr2)}, &nics2},
	} {
		t.Run(table.name, func(t *testing.T) {
			table.cfg.maybeCompatNetworkInterface()
			assert(t, nil, nil, table.cfg.NetworkInterfaces, table.wantNetworkInterfaces)
		})
	}
}

func TestCheckCompatNetworkInterfaces(t *testing.T) {
	ifnameOnly := s2sNetworkInterfaces(t, [][2]string{{"eth0", ""}})
	ifaddrOnly := s2sNetworkInterfaces(t, [][2]string{{"", "aa:bb:cc:dd:ee:ff"}})
	for _, table := range []struct {
		name    string
		cfg     Config
		errType error
	}{
		{"invalid: no bonding", Config{NetworkInterfaces: ifnameOnly}, errors.New("")},
		{"invalid: bonding", Config{NetworkInterfaces: ifaddrOnly, BondingMode: Bonding8023AD}, errors.New("")},
		{"valid: no bonding", Config{NetworkInterfaces: ifaddrOnly}, nil},
		{"valid: bonding", Config{NetworkInterfaces: ifnameOnly, BondingMode: Bonding8023AD}, nil},
	} {
		t.Run(table.name, func(t *testing.T) {
			err := table.cfg.checkCompatNetworkInterfaces()
			assert(t, err, table.errType, nil, nil)
		})
	}
}
