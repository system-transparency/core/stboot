package host

import (
	"context"
	"encoding/json"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/google/uuid"
	"github.com/u-root/u-root/pkg/efivarfs"
	di "system-transparency.org/stboot/internal/dependency"
	"system-transparency.org/stboot/internal/test"
)

func setupEnv(t *testing.T, efivar, initramfs string) context.Context {
	t.Helper()

	ipAddrMode := IPDynamic
	emptyFs := new(fstest.MapFS)
	emptyEfiVar := new(di.FakeEfiVars)

	ctx := context.Background()
	ctx = di.WithFilesystem(ctx, emptyFs)
	ctx = di.WithEFIVar(ctx, emptyEfiVar)

	if efivar != "" {
		p := strings.SplitN(HostConfigEFIVarName, "-", 2)
		desc := efivarfs.VariableDescriptor{
			GUID: uuid.MustParse(p[1]),
			Name: p[0],
		}

		cfg := Config{
			IPAddrMode:   &ipAddrMode,
			OSPkgPointer: &efivar,
		}

		cfgbuf, err := json.Marshal(cfg)
		if err != nil {
			t.Errorf("json.Marshal(cfg) = %v, want nil", err)
		}

		mockEfiVar := &di.FakeEfiVars{
			Content: map[efivarfs.VariableDescriptor][]byte{
				desc: cfgbuf,
			},
			Attributes: map[efivarfs.VariableDescriptor]efivarfs.VariableAttributes{
				desc: efivarfs.VariableAttributes(0),
			},
		}

		ctx = di.WithEFIVar(ctx, mockEfiVar)
	}

	if initramfs != "" {
		cfg := Config{
			IPAddrMode:   &ipAddrMode,
			OSPkgPointer: &initramfs,
		}

		cfgbuf, err := json.Marshal(cfg)
		if err != nil {
			t.Errorf("json.Marshal(cfg) = %v, want nil", err)
		}

		mockFsys := test.NewMockFS()

		err = mockFsys.Add(HostConfigInitrdPath, &fstest.MapFile{Data: cfgbuf, Mode: 0644})
		if err != nil {
			t.Errorf("mockFsys.Add(HostConfigInitrdPath, &fstest.MapFile{Data: cfgbuf, Mode: 0644}) = %v, want nil", err)
		}

		ctx = di.WithFilesystem(ctx, mockFsys)
	}

	return ctx
}

func TestAutodetectNothing(t *testing.T) {
	ctx := setupEnv(t, "", "")

	_, err := ConfigAutodetect(ctx)
	if err != ErrConfigNotFound {
		t.Errorf("ConfigAutodetect(ctx) = %v, want ErrConfigNotFound", err)
	}
}

func TestAutodetectInitramfs(t *testing.T) {
	ctx := setupEnv(t, "", "initramfs.zip")

	cfg, err := ConfigAutodetect(ctx)
	if err != nil {
		t.Errorf("ConfigAutodetect(ctx) = %v, want nil", err)
	}

	if got, want := *cfg.OSPkgPointer, "initramfs.zip"; got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}

func TestAutodetectEfivar(t *testing.T) {
	ctx := setupEnv(t, "efivar.zip", "")

	cfg, err := ConfigAutodetect(ctx)
	if err != nil {
		t.Errorf("ConfigAutodetect(ctx) = %v, want nil", err)
	}

	if got, want := *cfg.OSPkgPointer, "efivar.zip"; got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}

func TestAutodetectInitramfsOverEfivars(t *testing.T) {
	ctx := setupEnv(t, "efivar.zip", "initramfs.zip")

	cfg, err := ConfigAutodetect(ctx)
	if err != nil {
		t.Errorf("ConfigAutodetect(ctx) = %v, want nil", err)
	}

	if got, want := *cfg.OSPkgPointer, "initramfs.zip"; got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}
