package network

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"filippo.io/age"
	"github.com/u-root/uio/uio"
	"system-transparency.org/stboot/internal/wctx"
	"system-transparency.org/stboot/stlog"
)

type HTTPClient struct {
	HTTPClient http.Client
	Retries    int
	RetryWait  time.Duration
	decryptIds []age.Identity
}

type HTTPClientOption func(*HTTPClient)

func WithDecryption(decryptionIdentities []age.Identity) HTTPClientOption {
	return func(h *HTTPClient) {
		h.decryptIds = decryptionIdentities
	}
}

func NewHTTPClient(httpsRoots []*x509.Certificate, insecure bool, opts ...HTTPClientOption) HTTPClient {
	const (
		timeout             = 30 * time.Second
		keepAlive           = 30 * time.Second
		maxIdleConns        = 100
		idleConnTimeout     = 90 * time.Second
		tlsHandshakeTimeout = 10 * time.Second
	)

	const (
		retries   = 8
		retryWait = 1 * time.Second
	)

	roots := x509.NewCertPool()
	for _, cert := range httpsRoots {
		roots.AddCert(cert)
	}

	tls := &tls.Config{
		RootCAs: roots,
	}
	if insecure {
		tls.InsecureSkipVerify = true
	}

	// setup client with values taken from http.DefaultTransport + RootCAs
	h := HTTPClient{
		Retries:   retries,
		RetryWait: retryWait,
		HTTPClient: http.Client{
			Transport: (&http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   timeout,
					KeepAlive: keepAlive,
					DualStack: true,
				}).DialContext,
				MaxIdleConns:          maxIdleConns,
				IdleConnTimeout:       idleConnTimeout,
				TLSHandshakeTimeout:   tlsHandshakeTimeout,
				ExpectContinueTimeout: 1 * time.Second,
				TLSClientConfig:       tls,
			}),
		},
	}
	for _, opt := range opts {
		opt(&h)
	}
	return h
}

var (
	ErrDownloadTimeout = errors.New("hit download timeout")
	ErrRetriesLimit    = errors.New("hit retries limit")
	ErrBadHTTPStatus   = errors.New("bad HTTP status")
	ErrEmptyBody       = errors.New("HTTP response body is empty")
)

// Wrapper for downloadObject to deal with retries.
func (h *HTTPClient) Download(ctx context.Context, url *url.URL) ([]byte, error) {
	stlog.Info("Downloading %q", url.String())

	iter := 0

	ctx = stlog.WithClientTrace(ctx)

	for {
		ret, err := downloadObject(ctx, h.HTTPClient, url, h.decryptIds)
		if err == nil {
			return ret, nil
		}

		if errors.Is(err, context.DeadlineExceeded) {
			return nil, ErrDownloadTimeout
		}
		stlog.Warn("download of %q failed: %v", url, err)

		iter++
		if iter >= h.Retries {
			return nil, ErrRetriesLimit
		}

		if err := wctx.Sleep(ctx, h.RetryWait); err != nil {
			return nil, err
		}
	}
}

func downloadObject(ctx context.Context, client http.Client, url *url.URL, decryptionIdentities []age.Identity) ([]byte, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		stlog.Error("Bad HTTP status: %s", resp.Status)

		return nil, ErrBadHTTPStatus
	}

	stlog.Info("Content-length: %d, Content-type: %q", resp.ContentLength, resp.Header.Get("content-type"))

	if stlog.Level() >= stlog.InfoLevel {
		const interval = 5 * 1024 * 1024

		resp.Body = &uio.ProgressReadCloser{
			RC:       resp.Body,
			Symbol:   ".",
			Interval: interval,
			W:        os.Stderr,
		}
	}

	var ret []byte
	if len(decryptionIdentities) != 0 {
		stlog.Info("Decrypting response body")
		reader, err := age.Decrypt(resp.Body, decryptionIdentities...)
		if err != nil {
			return nil, fmt.Errorf("decrypting failed: %w", err)
		}
		ret, err = wctx.ReadAll(ctx, reader)
		if err != nil {
			return nil, fmt.Errorf("decrypting download failed: %w", err)
		}
	} else {
		ret, err = wctx.ReadAll(ctx, resp.Body)
		if err != nil {
			return nil, fmt.Errorf("download failed: %w", err)
		}
	}

	if len(ret) == 0 {
		return nil, ErrEmptyBody
	}

	stlog.Debug("Content appears to be of type: %s", http.DetectContentType(ret))

	return ret, nil
}
