// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package network

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/u-root/u-root/pkg/dhclient"
	"github.com/vishvananda/netlink"
	"system-transparency.org/stboot/host"
	"system-transparency.org/stboot/stlog"
)

// Errors which may be raised and wrapped in this package.
var (
	ErrFailedForAllInterfaces = errors.New("IP configuration failed for all interfaces")
	ErrFoundNoInterfaces      = errors.New("found no interfaces")
)

const (
	entropyAvail       = "/proc/sys/kernel/random/entropy_avail"
	resolvConf         = "/etc/resolv.conf"
	dhcpRequestTimeout = 6 * time.Second
	dhcpRequestRetries = 4
	linkUpTimeout      = 30 * time.Second
)

func SetupNetworkInterface(ctx context.Context, cfg *host.Config) error {
	switch *cfg.IPAddrMode {
	case host.IPStatic:
		if err := configureStatic(cfg); err != nil {
			return err
		}
	case host.IPDynamic:
		if err := configureDHCP(ctx, cfg); err != nil {
			return err
		}
	case host.IPUnset:
	default:
		return fmt.Errorf("network configuration failed: IP addr mode is not set")
	}

	if cfg.DNSServer != nil {
		stlog.Info("Set DNS Server")

		for _, ip := range *cfg.DNSServer {
			stlog.Info("- %s", ip.String())
		}

		if err := setDNSServer(*cfg.DNSServer, resolvConf); err != nil {
			return fmt.Errorf("set DNS Server: %w", err)
		}
	}

	return nil
}

func ConfigureBondInterface(cfg *host.Config) (*netlink.Bond, error) {
	bond, err := SetupBondInterface(*cfg.BondName, netlink.StringToBondMode(cfg.BondingMode.String()))
	if err != nil {
		return nil, err
	}
	if err := SetBonded(bond, *cfg.NetworkInterfaces); err != nil {
		return nil, err
	}
	return bond, nil
}

func getLinks(cfg *host.Config) ([]netlink.Link, error) {
	if cfg.BondingMode != host.BondingUnset {
		bond, err := ConfigureBondInterface(cfg)
		if err != nil {
			return nil, fmt.Errorf("configuring bonding failed: %w", err)
		}
		// If we use the bond interface we don't know the MAC address the kernel gives it
		// ignore the original device and replace with our bonding interface
		return []netlink.Link{bond}, nil
	}
	links, err := allNonLoopbackLinks()
	if err != nil {
		return nil, err
	}

	if cfg.NetworkInterfaces == nil {
		return links, nil
	}

	if found := filterLinksByMAC(links, *cfg.NetworkInterfaces); len(found) > 0 {
		if len(found) != len(*cfg.NetworkInterfaces) {
			stlog.Warn("found only %d out of the %d configured MAC adresses",
				len(found), len(*cfg.NetworkInterfaces))
		}
		return found, nil
	}
	stlog.Warn("found no interfaces with the specified MACs, falling back to using all available interfaces")
	return links, nil
}

func configureStatic(cfg *host.Config) error {
	stlog.Info("Setup network interface with static IP: " + cfg.HostIP.String())

	links, err := getLinks(cfg)
	if err != nil {
		return err
	}

	for _, link := range links {
		if err := netlink.AddrAdd(link, cfg.HostIP); err != nil {
			stlog.Debug("%s: IP config failed: %v", link.Attrs().Name, err)

			continue
		}

		// dhclient.IfUp is a helper for bringing the interface up and ensuring
		// it is really up.  In other words, this won't do any dhcp things.
		if _, err := dhclient.IfUp(link.Attrs().Name, linkUpTimeout); err != nil {
			stlog.Debug("%s: IP config failed: %v", link.Attrs().Name, err)

			continue
		}

		r := &netlink.Route{
			LinkIndex: link.Attrs().Index,
			Gw:        *cfg.DefaultGateway,
		}
		if err := netlink.RouteAdd(r); err != nil {
			stlog.Debug("%s: IP config failed: %v", link.Attrs().Name, err)

			continue
		}

		stlog.Info("%s: IP configuration successful", link.Attrs().Name)

		return nil
	}

	return ErrFailedForAllInterfaces
}

func configureDHCP(ctx context.Context, cfg *host.Config) error {
	stlog.Info("Configure network interface using DHCP")

	links, err := getLinks(cfg)
	if err != nil {
		return err
	}

	var level dhclient.LogLevel
	if stlog.Level() != stlog.InfoLevel {
		level = dhclient.LogSummary
	} else {
		level = dhclient.LogInfo
	}

	config := dhclient.Config{
		Timeout:  dhcpRequestTimeout,
		Retries:  dhcpRequestRetries,
		LogLevel: level,
	}

	r := dhclient.SendRequests(ctx, links, true, false, config, linkUpTimeout)
	for result := range r {
		if result.Err != nil {
			stlog.Debug("%s: DHCP response error: %v", result.Interface.Attrs().Name, result.Err)

			continue
		}

		err = result.Lease.Configure()
		if err != nil {
			stlog.Debug("%s: DHCP configuration error: %v", result.Interface.Attrs().Name, err)
		} else {
			stlog.Info("DHCP successful - %s", result.Interface.Attrs().Name)

			return nil
		}
	}

	return ErrFailedForAllInterfaces
}

// SetDNSServer writes adresses to /etc/resolv.conf file.
func SetDNSServer(addresses []*net.IP) error {
	return setDNSServer(addresses, "/etc/resolv.conf")
}

func setDNSServer(addresses []*net.IP, out string) error {
	var resolvconf string

	for _, addr := range addresses {
		resolvconf += fmt.Sprintf("nameserver %s\n", addr.String())
	}

	const perm = 0644
	if err := os.WriteFile(out, []byte(resolvconf), perm); err != nil {
		return fmt.Errorf("failed to write to %q: %w", out, err)
	}

	return nil
}
func allNonLoopbackLinks() ([]netlink.Link, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	if len(interfaces) == 0 {
		return nil, ErrFoundNoInterfaces
	}

	links := make([]netlink.Link, 0, len(interfaces))

	for _, iface := range interfaces {
		stlog.Debug("Found interface %s", iface.Name)
		stlog.Debug("    MTU: %d Hardware Addr: %s", iface.MTU, iface.HardwareAddr.String())
		stlog.Debug("    Flags: %v", iface.Flags)
		// skip loopback
		if iface.Flags&net.FlagLoopback != 0 || len(iface.HardwareAddr) == 0 {
			continue
		}

		link, err := netlink.LinkByName(iface.Name)
		if err != nil {
			stlog.Debug("%v", err)
			continue
		}

		links = append(links, link)
	}

	if len(links) == 0 {
		return nil, ErrFoundNoInterfaces
	}

	return links, nil
}

// Return a list of links with MAC addresses matching configured
// interfaces, in order.
func filterLinksByMAC(links []netlink.Link, interfaces []*host.NetworkInterface) []netlink.Link {
	found := make([]netlink.Link, 0, len(interfaces))
	for _, iface := range interfaces {
		for _, link := range links {
			if bytes.Equal(*iface.MACAddress, link.Attrs().HardwareAddr) {
				found = append(found, link)
				break
			}
		}
	}
	return found
}

func SetupBondInterface(ifaceName string, mode netlink.BondMode) (*netlink.Bond, error) {
	if link, err := netlink.LinkByName(ifaceName); err == nil {
		if err := netlink.LinkDel(link); err != nil {
			return nil, fmt.Errorf("%s: delete link: %w", link, err)
		}
	}

	la := netlink.NewLinkAttrs()
	la.Name = ifaceName
	bond := netlink.NewLinkBond(la)
	bond.Mode = mode

	bond.Miimon = 100

	if bond.Mode == netlink.BOND_MODE_802_3AD {
		bond.LacpRate = netlink.BOND_LACP_RATE_FAST
	}

	if err := netlink.LinkAdd(bond); err != nil {
		return nil, fmt.Errorf("%v: create: %w", bond, err)
	}

	return bond, nil
}

func SetBonded(bond *netlink.Bond, toBondNames []*host.NetworkInterface) error {
	if len(toBondNames) == 0 {
		return fmt.Errorf("no bonded interfaces supplied")
	}

	stlog.Debug("bonding the following interfaces into %s: %v",
		bond.Attrs().Name,
		func(l []*host.NetworkInterface) []string {
			var acc []string
			for _, e := range l {
				acc = append(acc, *e.InterfaceName)
			}

			return acc
		}(toBondNames))

	for _, iface := range toBondNames {
		link, err := netlink.LinkByName(*iface.InterfaceName)
		if err != nil {
			return fmt.Errorf("%s: to be bonded not found: %w", *iface.InterfaceName, err)
		}

		if err := netlink.LinkSetDown(link); err != nil {
			return fmt.Errorf("%v: link down: %w", link, err)
		}

		if err := netlink.LinkSetBondSlave(link, bond); err != nil {
			return fmt.Errorf("%v: bonding into %v: %w", link, bond, err)
		}
	}

	// dhclient.IfUp is a helper for bringing the interface up and ensuring it
	// is really up.  In other words, this won't do any dhcp things.
	ifaceName := bond.Attrs().Name
	if _, err := dhclient.IfUp(ifaceName, linkUpTimeout); err != nil {
		return fmt.Errorf("%s: interface bring up failed: %w", ifaceName, err)
	}
	return nil
}
