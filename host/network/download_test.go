package network

import (
	"bytes"
	"context"
	"crypto/x509"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"filippo.io/age"
	"github.com/stretchr/testify/assert"
	"system-transparency.org/stboot/stlog"
)

func mkURL(s string) *url.URL {
	u, _ := url.Parse(s)

	return u
}

func mkServer(t *testing.T, code int, body []byte) *httptest.Server {
	t.Helper()

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(code)
		_, err := w.Write(body)
		assert.NoError(t, err)
	}))

	t.Cleanup(srv.Close)

	return srv
}

func TestHTTPClient(t *testing.T) {
	if !testing.Verbose() {
		stlog.SetLevel(stlog.ErrorLevel)
	}

	expected := "test response"
	svr := mkServer(t, http.StatusOK, []byte(expected))

	var roots []*x509.Certificate
	client := NewHTTPClient(roots, false)

	b, err := client.Download(context.Background(), mkURL(svr.URL))
	if err != nil {
		t.Fatal(err)
	}

	s := string(b)
	if s != expected {
		t.Fatal(err)
	}
}

func TestHTTPClientEncrypted(t *testing.T) {
	if !testing.Verbose() {
		stlog.SetLevel(stlog.ErrorLevel)
	}

	id, err := age.GenerateX25519Identity()
	assert.NoError(t, err)

	expected := "test response"
	var buf bytes.Buffer
	wc, err := age.Encrypt(&buf, id.Recipient())
	assert.NoError(t, err)
	_, err = wc.Write([]byte(expected))
	assert.NoError(t, err)
	wc.Close()
	assert.NoError(t, err)

	svr := mkServer(t, http.StatusOK, buf.Bytes())

	var roots []*x509.Certificate
	client := NewHTTPClient(roots, false, WithDecryption([]age.Identity{id}))

	b, err := client.Download(context.Background(), mkURL(svr.URL))
	if err != nil {
		t.Fatal(err)
	}

	s := string(b)
	if s != expected {
		t.Fatal(err)
	}
}

func TestHTTPClientCancel(t *testing.T) {
	if !testing.Verbose() {
		stlog.SetLevel(stlog.ErrorLevel)
	}

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(5 * time.Second)
		fmt.Fprint(w, "")
	}))

	defer svr.Close()

	var roots []*x509.Certificate
	client := NewHTTPClient(roots, false)

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	_, err := client.Download(ctx, mkURL(svr.URL))
	if !errors.Is(err, ErrDownloadTimeout) {
		t.Fatal(err)
	}
}

func Test500(t *testing.T) {
	ctx := context.Background()
	svr := mkServer(t, http.StatusInternalServerError, nil)

	var roots []*x509.Certificate
	roots = append(roots, &x509.Certificate{})
	client := NewHTTPClient(roots, true)

	_, err := client.Download(ctx, mkURL(svr.URL))
	if !errors.Is(err, ErrRetriesLimit) {
		t.Fatal(err)
	}
}

func TestDownloadObject500(t *testing.T) {
	ctx := context.Background()
	expected := "test response"
	srv := mkServer(t, http.StatusInternalServerError, []byte(expected))
	client := srv.Client()

	_, err := downloadObject(ctx, *client, mkURL(srv.URL), nil)
	if !errors.Is(err, ErrBadHTTPStatus) {
		t.Fatal("expected error, got", err)
	}
}

func TestDownloadObjectEmptyResp(t *testing.T) {
	ctx := context.Background()
	srv := mkServer(t, http.StatusOK, nil)
	client := srv.Client()

	_, err := downloadObject(ctx, *client, mkURL(srv.URL), nil)
	if err == nil {
		t.Fatal("expected error")
	}
}
