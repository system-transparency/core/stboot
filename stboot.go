// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"context"
	"crypto/x509"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"runtime/debug"
	"strings"
	"time"

	"golang.org/x/sys/unix"

	"filippo.io/age"
	"github.com/u-root/u-root/pkg/boot"
	"github.com/u-root/u-root/pkg/libinit"

	"system-transparency.org/stboot/host"
	"system-transparency.org/stboot/host/network"
	di "system-transparency.org/stboot/internal/dependency"
	"system-transparency.org/stboot/internal/ui"
	"system-transparency.org/stboot/internal/wctx"
	"system-transparency.org/stboot/opts"
	"system-transparency.org/stboot/ospkg"
	"system-transparency.org/stboot/stlog"
	"system-transparency.org/stboot/trust"
)

const (
	logLevelHelp = "Log level: e 'errors' w 'warn', i 'info', d 'debug'."
	dryRunHelp   = "Stop before kexec-ing into the loaded OS kernel"
	deadlineHelp = "Timeout in minutes for download operations (default: 20)"
)

// Files at initramfs.
const (
	trustPolicyFile = "/etc/trust_policy/trust_policy.json"
	signingRootFile = "/etc/trust_policy/ospkg_signing_root.pem"
	httpsRootsFile  = "/etc/trust_policy/tls_roots.pem"
	// Age decryption identities (optional).
	decryptionIdentitiesFile = "/etc/trust_policy/decryption_identities"

	promptTimeout    = 30 * time.Second
	interruptTimeout = 5 * time.Second

	// OS package files (optional).
	HostConfigProvisionOSPKGName = "provision.zip"
)

const banner = `
  _____ _______   _____   ____   ____________
 / ____|__   __|  |  _ \ / __ \ / __ \__   __|
| (___    | |     | |_) | |  | | |  | | | |
 \___ \   | |     |  _ <| |  | | |  | | | |
 ____) |  | |     | |_) | |__| | |__| | | |
|_____/   |_|     |____/ \____/ \____/  |_|

`

const check = `
           //\\
verified  //  \\
OS       //   //
        //   //
 //\\  //   //
//  \\//   //
\\        //
 \\      //
  \\    //
   \\__//
`

// Can be configured at link time using
//
//	-ldflags="-X main.ConfiguredVersion=..."
//
// to override the use of debug.ReadBuildInfo() for the version
// string.
var ConfiguredVersion = ""

func getVersion() string {
	if ConfiguredVersion != "" {
		return ConfiguredVersion
	}
	info, ok := debug.ReadBuildInfo()
	if !ok {
		return "unknown"
	}

	// When built, e.g., using go install .../stboot@vX.Y.Z.
	version := info.Main.Version
	if version != "(devel)" {
		return version
	}

	// Use git commit, if available. The vcs.* fields are
	// populated when running "go build" in a git checkout,
	// *without* listing specific source files like "stboot.go" on
	// the commandline.
	m := make(map[string]string)
	for _, setting := range info.Settings {
		m[setting.Key] = setting.Value
	}
	if revision, ok := m["vcs.revision"]; ok {
		if t, ok := m["vcs.time"]; ok {
			revision += " " + t
		}
		// Note that any untracked file (if not listed in
		// .gitignore) counts as a local modification. Which
		// makes sense, since the go toolchain determines what
		// to do automatically, based on which files exist.
		// For this flag to be reliable, avoid adding patterns
		// in .gitignore that could match files that have
		// meaning to the go toolchain.
		if m["vcs.modified"] != "false" {
			revision += " (with local changes)"
		}
		return revision
	}
	return version
}

var errRecover = errors.New("boot failed")
var errDownload = errors.New("download failed")

func main() {
	ctx := context.Background()

	logLevel := flag.String("loglevel", "info", logLevelHelp)
	dryRun := flag.Bool("dryrun", false, dryRunHelp)
	deadline := flag.Int("deadline", 20, deadlineHelp)
	versionFlag := flag.Bool("version", false, "Display stboot version and exit")

	log.SetPrefix("stboot: ")

	flag.Parse()

	if *versionFlag {
		fmt.Printf("stboot version: %s\n", getVersion())
		return
	}
	switch *logLevel {
	case "e", "error":
		stlog.SetLevel(stlog.ErrorLevel)
	case "w", "warm":
		stlog.SetLevel(stlog.WarnLevel)
	case "i", "info":
		stlog.SetLevel(stlog.InfoLevel)
	case "d", "debug":
		stlog.SetLevel(stlog.DebugLevel)
	default:
		stlog.SetLevel(stlog.InfoLevel)
	}

	flag.Visit(func(f *flag.Flag) {
		stlog.Debug("-%s %s", f.Name, f.Value)
	})

	// Are we running as the init process?
	if os.Getpid() == 1 {
		// As the init process, it's our responsibility to
		// reap orphans. However, we never spawn any
		// additional processes.
		stlog.Info("Running as pid 1")

		// Load modules first, since they may be needed, e.g.,
		// for efivarfs. Attempt to continue booting, even if
		// modules fail to load.
		if err := libinit.InstallAllModules(); err != nil {
			stlog.Error("module loading failed: %v", err)
		}

		// Among other things, mounts efivarfs. The TPM access
		// also depends on this initialization. Note that
		// CreateRootfs examines uroot.initflags in
		// /proc/cmdline, for detection of systemd.
		libinit.CreateRootfs()

		// Attempts to bring up loopback interface.
		libinit.NetInit()

		// Attach to a controlling terminal, to enable
		// terminal-related signals like SIGINT and SIGHUP.
		// Initially, we expect pgid == 0 and sid == 0. Then
		// setsid should succeed in making us process group
		// and session leader with pgid == 1 and sid == 1.
		sid, err := unix.Setsid()
		if err != nil {
			stlog.Error("Failed to create new session, setsid failed: %v", err)
			sid, _ = unix.Getsid(0)
		}
		stlog.Debug("Running with pgid: %d, sid: %d", unix.Getpgrp(), sid)

		// See man page ioctl_tty(2). First zero argument is
		// the stdin fd, identifying the terminal we want to
		// use. The other zero argument means no stealing
		// (which could otherwise happen if it the terminal
		// belongs to a different session group and we are
		// privileged enough).
		if err := unix.IoctlSetInt(0, unix.TIOCSCTTY, 0); err != nil {
			stlog.Error("Failed to attach stdin as controlling terminal: %v", err)
		}
	}

	stlog.Info(banner)
	stlog.Info("version: %s", getVersion())

	///////////////////////////////////////
	// Setup of trust policy and cert roots
	///////////////////////////////////////
	trustPolicy, err := opts.ReadTrustPolicy(trustPolicyFile)
	if err != nil {
		stlog.Error("trust policy: %v", err)
		host.Recover()
	}

	now := time.Now()
	certs, err := opts.ReadCertsFile(signingRootFile, now)
	if err != nil {
		stlog.Error("signing root certificate: %v", err)
		host.Recover()
	}
	if got := len(certs); got != 1 {
		stlog.Error("exactly one root certificate is expected, got %d", got)
		host.Recover()
	}
	signingRootCert := certs[0]

	httpsRoots, err := opts.ReadOptionalCertsFile(httpsRootsFile, now)
	if err != nil {
		stlog.Error("https root certificates: %v", err)
		host.Recover()
	}

	decryptionIdentities, err := opts.ReadDecryptionIdentities(decryptionIdentitiesFile)
	if err != nil {
		stlog.Error("decryption identities: %v", err)
		host.Recover()
	}

	deadlineDuration := time.Duration(*deadline) * time.Minute

	// The boot*Image functions are not expected to return,
	// and they return with a nil error only in dryrun mode,
	// i.e., getting a nil error is the exception.
	err = bootConfiguredImage(ctx, trustPolicy, signingRootCert, httpsRoots, decryptionIdentities, deadlineDuration, *dryRun, hasProvisionImage())
	if err == nil {
		return
	}
	stlog.Error("booting configured OS package failed: %v", err)
	if err == host.ErrConfigNotFound || userWantsProvision() {
		stlog.Info("PROVISION MODE! Attempting to boot /ospkg/provision.{jzon,zip}")
		err := bootProvisionImage(ctx, trustPolicy, signingRootCert, *dryRun)
		if err == nil {
			return
		}
		stlog.Error("booting provisioning OS package failed: %v", err)
	}

	host.Recover()
}

func bootConfiguredImage(ctx context.Context, trustPolicy *trust.Policy, signingRootCert *x509.Certificate, httpsRoots []*x509.Certificate, decryptionIdentities []age.Identity, deadline time.Duration, dryRun, enableInterrupt bool) error {
	//////////////////
	// Get host config
	//////////////////
	hostCfg, err := host.ConfigAutodetect(ctx)
	if err != nil {
		return err
	}
	if hostCfg.Description != nil && *hostCfg.Description != "" {
		stlog.Info("host config description: %s\n",
			cleanString(*hostCfg.Description))
	}
	var timer <-chan time.Time

	if enableInterrupt {
		var cancel context.CancelFunc
		ctx, cancel = signal.NotifyContext(ctx, os.Interrupt)
		// TODO: The cancel call restores default SIGINT
		// behavior, i.e., SIGINT terminates the process.
		// Maybe we keep ignore SIGINT for a second or two
		// after displaying the error messages, to prevent
		// accidental killing of stboot at the wrong time?
		defer cancel()
		timer = time.After(interruptTimeout)

		stlog.Info("Press Ctrl-C to interrupt normal boot")
	}

	// System appears provisioned, apply host config.
	if trustPolicy.FetchMethod == ospkg.FetchFromNetwork {
		// It's possible to have a http url for the ospkg_pointer, but a https
		// url in the downloaded descriptor. We don't detect that case here, but
		// it will fail later if no HTTPS roots are configured.
		if needsHTTPS(*hostCfg.OSPkgPointer) && len(httpsRoots) == 0 {
			return fmt.Errorf("network boot with HTTPS is configured, but HTTPS root certificates are missing")
		}

		err := network.SetupNetworkInterface(ctx, &hostCfg)
		if err != nil {
			return fmt.Errorf("failed to setup network interfaces: %v", err)
		}
	}
	return getAndBootImage(ctx, trustPolicy, signingRootCert, httpsRoots, decryptionIdentities, hostCfg, deadline, dryRun, timer)
}

func bootProvisionImage(ctx context.Context, trustPolicy *trust.Policy, signingRootCert *x509.Certificate, dryRun bool) error {
	provTrustPolicy := *trustPolicy
	provTrustPolicy.FetchMethod = ospkg.FetchFromInitramfs
	return getAndBootImage(ctx, &provTrustPolicy, signingRootCert, nil, nil, provisionHostConfig(), 0, dryRun, nil)
}

func getAndBootImage(ctx context.Context, trustPolicy *trust.Policy, signingRootCert *x509.Certificate, httpsRoots []*x509.Certificate, decryptionIdentities []age.Identity, hostCfg host.Config, deadline time.Duration, dryRun bool, timer <-chan time.Time) error {
	img, err := getImage(ctx, trustPolicy, signingRootCert, httpsRoots, decryptionIdentities, hostCfg, deadline)
	if err != nil {
		return err
	}
	if timerIsPending(timer) {
		stlog.Info("Last chance to press Ctrl-C to interrupt normal boot")
		if err := wctx.SleepOn(ctx, timer); err != nil {
			return err
		}
	}
	return bootImage(img, dryRun)
}

func userWantsProvision() bool {
	if !hasProvisionImage() {
		return false
	}
	fmt.Printf("Enter provisioning mode? (Y/n)\n")

	response, err := ui.HackReadLineWithTimeout(os.Stdin, promptTimeout)
	if err != nil {
		if err != ui.ErrTimeout {
			stlog.Error("%v", err)
		}
		return false
	}
	return len(response) == 0 || response[0] == 'y' || response[0] == 'Y'
}

// Fetch, verify, unpack, and measure an OS package, specified by the host config.
func getImage(ctx context.Context, trustPolicy *trust.Policy, signingRootCert *x509.Certificate, httpsRoots []*x509.Certificate, decryptionIdentities []age.Identity, hostCfg host.Config, deadline time.Duration) (*boot.LinuxImage, error) {
	fsys := di.DefaultFilesystem(ctx)

	//////////////////
	// Load OS package
	//////////////////

	var osp *ospkg.OSPackage

	switch trustPolicy.FetchMethod {
	case ospkg.FetchFromNetwork:
		stlog.Info("Loading OS package via network")

		client := network.NewHTTPClient(httpsRoots, false, network.WithDecryption(decryptionIdentities))

		stlog.Debug("OS package pointer: %s", *hostCfg.OSPkgPointer)

		ctx, cancel := context.WithTimeout(ctx, deadline)
		defer cancel()

		var err error
		osp, err = fetchOspkgNetwork(ctx, client, &hostCfg)
		if err != nil {
			stlog.Error("fetching OS package via network failed: %v", err)

			return nil, errRecover
		}
	case ospkg.FetchFromInitramfs:
		stlog.Info("Loading OS package from initramfs")

		var err error
		osp, err = fetchOspkgInitramfs(&hostCfg, fsys)
		if err != nil {
			stlog.Error("fetching OS package from initramfs failed: %v", err)

			return nil, errRecover
		}
	default:
		stlog.Error("unknown OS package fetch method %q", trustPolicy.FetchMethod)

		return nil, errRecover
	}

	////////////////////
	// Verify OS package
	////////////////////

	// TODO: write ospkg.info method for debug output

	numSig, valid, err := osp.Verify(signingRootCert, time.Now())
	if err != nil {
		stlog.Error("Verifying OS package: %v", err)

		return nil, errRecover
	}

	threshold := trustPolicy.SignatureThreshold
	if valid < threshold {
		stlog.Error("Not enough valid signatures: %d found, %d valid, %d required", numSig, valid, threshold)

		return nil, errRecover
	}

	stlog.Debug("Signatures: %d found, %d valid, %d required", numSig, valid, threshold)
	stlog.Info("OS package passed verification")
	stlog.Info(check)

	/////////////
	// Extract OS
	/////////////
	linuxImg, err := osp.LinuxImage()
	if err != nil {
		stlog.Error("Get boot image: %v", err)

		return nil, errRecover
	}

	if linuxImg.Kernel == nil {
		stlog.Error("No kernel, image not usable")

		return nil, errRecover
	}

	stlog.Debug("Boot image:\n %s", linuxImg.String())

	return &linuxImg, nil
}

// Attempt to boot image. On success, never returns, except in dryrun
// mode, in which case it returns nil before attempting kexec.
func bootImage(img *boot.LinuxImage, dryRun bool) error {
	stlog.Info("Loading boot image into memory")

	if err := img.Load(); err != nil {
		return err
	}

	//////////
	// Boot OS
	//////////
	if dryRun {
		stlog.Info("Dryrun mode: will not boot, exiting")
		return nil
	}

	stlog.Info("Handing over control - kexec")

	err := boot.Execute()
	return fmt.Errorf("unexpected return from kexec: %v", err)
}

// get an ospkg from the initramfs.
func fetchOspkgInitramfs(hostCfg *host.Config, fsys fs.FS) (*ospkg.OSPackage, error) {
	return _fetchOspkgInitramfs(hostCfg, "/ospkg", fsys)
}

func _fetchOspkgInitramfs(hostCfg *host.Config, dir string, fsys fs.FS) (*ospkg.OSPackage, error) {
	var (
		descriptorFile, archiveFile string
	)

	descriptorFile, archiveFile = ospkgFiles(hostCfg)

	descriptor, err := fsys.Open(filepath.Join(dir, descriptorFile))
	if err != nil {
		return nil, err
	}
	descriptorBytes, err := io.ReadAll(descriptor)
	if err != nil {
		return nil, err
	}

	archive, err := fsys.Open(filepath.Join(dir, archiveFile))
	if err != nil {
		return nil, err
	}
	archiveBytes, err := io.ReadAll(archive)
	if err != nil {
		return nil, err
	}

	return ospkg.NewOSPackage(archiveBytes, descriptorBytes)
}

func ospkgFiles(cfg *host.Config) (descriptor, archive string) {
	if cfg.OSPkgPointer == nil {
		return "", ""
	}

	osPkgPtr := *cfg.OSPkgPointer
	ext := filepath.Ext(osPkgPtr)
	name := strings.TrimSuffix(osPkgPtr, ext)

	return name + ".json", name + ".zip"
}

// get an ospkg via the network.
func fetchOspkgNetwork(ctx context.Context, client network.HTTPClient, hostCfg *host.Config) (*ospkg.OSPackage, error) {
	urls := ospkgURLs(hostCfg)
	if len(urls) == 0 {
		return nil, errors.New("no valid URLs in OS package pointer")
	}

	for _, url := range urls {
		// Error handling below logs and discards errors, to
		// continue with next URL. But in case the error was a
		// canceled context, give up and exit immediately.
		if err := ctx.Err(); err != nil {
			return nil, err
		}
		stlog.Debug("Downloading %s", url.String())

		descriptorURL := url

		dBytes, err := client.Download(ctx, &descriptorURL)
		if err != nil {
			stlog.Debug("Skip %s: %v", url.String(), err)

			continue
		}

		descriptor, err := readOspkg(dBytes)
		if err != nil {
			stlog.Debug("Skip %s: %v", url.String(), err)

			continue
		}

		stlog.Debug("Parsing OS package URL from descriptor")

		pkgURL, err := descriptor.GetPkgURL(&descriptorURL)

		if err != nil {
			stlog.Warn("Skip %q: invalid package URL: %s", url.String(), err)
			continue
		}

		stlog.Debug("Downloading %s", pkgURL.String())

		pkgBytes, err := client.Download(ctx, pkgURL)
		if err != nil {
			stlog.Debug("Skip %s: %v", url.String(), err)
			continue
		}

		return ospkg.NewOSPackageWithDescriptor(descriptor, pkgBytes)
	}

	stlog.Debug("all provisioning URLs failed")

	return nil, errDownload
}

func ospkgURLs(cfg *host.Config) []url.URL {
	urls := make([]url.URL, 0)

	if cfg.OSPkgPointer == nil {
		return urls
	}

	strs := strings.Split(*cfg.OSPkgPointer, ",")

	for _, s := range strs {
		addr, err := url.Parse(s)
		if err != nil {
			stlog.Warn("skip %q: %v", s, err)

			break
		}

		s := addr.Scheme
		if s != "http" && s != "https" {
			stlog.Warn("skip %q: unsupported scheme, want http or https", s)

			break
		}

		urls = append(urls, *addr)
	}

	return urls
}

func readOspkg(b []byte) (*ospkg.Descriptor, error) {
	stlog.Debug("Parsing descriptor")

	descriptor, err := ospkg.DescriptorFromBytes(b)
	if err != nil {
		return nil, err
	}

	stlog.Debug("Package descriptor:")
	stlog.Debug("  Version: %d", descriptor.Version)
	stlog.Debug("  Package URL: %s", descriptor.PkgURL)
	stlog.Debug("  %d signature(s)", len(descriptor.Signatures))
	stlog.Debug("  %d certificate(s)", len(descriptor.Certificates))
	stlog.Info("Validating descriptor")

	if err = descriptor.Validate(); err != nil {
		return nil, err
	}

	return descriptor, nil
}

func needsHTTPS(osPkgPtr string) bool {
	for _, u := range strings.Split(osPkgPtr, ",") {
		URL, err := url.Parse(u)
		if err != nil {
			stlog.Warn("bad url in ospkg_pointer: %v", err)
			continue
		}
		if URL.Scheme == "https" {
			return true
		}
	}
	return false
}

func hasProvisionImage() bool {
	st, err := os.Stat(filepath.Join("/ospkg", HostConfigProvisionOSPKGName))
	return err == nil && st.Mode().IsRegular()
}

func provisionHostConfig() host.Config {
	ipAddrMode := host.IPDynamic
	osPkgPtr := HostConfigProvisionOSPKGName
	return host.Config{
		IPAddrMode:   &ipAddrMode,
		OSPkgPointer: &osPkgPtr,
	}
}

func timerIsPending(timer <-chan time.Time) bool {
	if timer == nil {
		return false
	}
	select {
	case <-timer:
		return false
	default:
		return true
	}
}

// Discard everything that isn't an ascii non-control character.
func cleanString(s string) string {
	builder := strings.Builder{}
	for i := 0; i < len(s); i++ {
		var c byte = s[i]
		if c >= 32 && c <= 126 {
			builder.WriteByte(c)
		}
	}
	return builder.String()
}
