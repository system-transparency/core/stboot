package ospkg

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestManifestParsing(t *testing.T) {
	m, err := OSManifestFromBytes([]byte(`{
    "version": 1,
    "label": "test",
    "kernel": "vmlinuz",
    "initramfs": "initramfs",
    "cmdline": "console=ttyS0,115200n8"
  }`))

	require.NoError(t, err)
	require.Equal(t, 1, m.Version)
	require.Equal(t, "test", m.Label)
	require.Equal(t, "vmlinuz", m.KernelPath)
	require.Equal(t, "initramfs", m.InitramfsPath)
	require.Equal(t, "console=ttyS0,115200n8", m.Cmdline)

	_, err = m.Bytes()
	require.NoError(t, err)

	_, err = OSManifestFromBytes([]byte(`{blub}`))
	require.Error(t, err)
}

func TestManifestValidation(t *testing.T) {
	tmpl := OSManifest{
		Version:       ManifestVersion,
		Label:         "test",
		KernelPath:    "vmlinuz",
		InitramfsPath: "initramfs",
		Cmdline:       "console=ttyS0,115200n8",
	}

	t.Run("Wrong Version", func(t *testing.T) {
		d := tmpl
		d.Version = 0
		err := d.Validate()
		require.Error(t, err)
	})
	t.Run("No kernel", func(t *testing.T) {
		d := tmpl
		d.KernelPath = ""
		err := d.Validate()
		require.Error(t, err)
	})
	t.Run("No initramfs", func(t *testing.T) {
		d := tmpl
		d.InitramfsPath = ""
		err := d.Validate()
		require.Error(t, err)
	})
	t.Run("Valid", func(t *testing.T) {
		d := tmpl
		err := d.Validate()
		require.NoError(t, err)
	})
}
