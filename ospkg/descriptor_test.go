// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package ospkg

import (
	"testing"

	"net/url"

	"github.com/stretchr/testify/require"
)

func TestBytes(t *testing.T) {
	var d = Descriptor{
		PkgURL:       "test.com",
		Certificates: [][]byte{{1, 2, 3}, {1, 2, 3}},
		Signatures:   [][]byte{{'a', 'b', 'c'}, {'a', 'b', 'c'}},
	}

	b, err := d.Bytes()
	t.Log(b)
	require.NoError(t, err)
}

func TestDescriptorFromBytes(t *testing.T) {
	var b = []byte{
		123, 34, 111, 115, 95, 112, 107, 103, 95, 117, 114, 108,
		34, 58, 34, 116, 101, 115, 116, 46, 99, 111, 109, 34, 44,
		34, 99, 101, 114, 116, 105, 102, 105, 99, 97, 116, 101, 115,
		34, 58, 91, 34, 65, 81, 73, 68, 34, 44, 34, 65, 81, 73, 68,
		34, 93, 44, 34, 115, 105, 103, 110, 97, 116, 117, 114, 101,
		115, 34, 58, 91, 34, 89, 87, 74, 106, 34, 44, 34, 89, 87, 74,
		106, 34, 93, 125,
	}

	d, err := DescriptorFromBytes(b)
	t.Log(d)
	require.NoError(t, err)

	_, err = DescriptorFromBytes([]byte("blub"))
	require.Error(t, err)
}

func TestValidation(t *testing.T) {
	tmpl := Descriptor{
		Version:      DescriptorVersion,
		PkgURL:       "https://test.com/blub.zip",
		Certificates: [][]byte{{1, 2, 3}, {1, 2, 3}},
		Signatures:   [][]byte{{'a', 'b', 'c'}, {'a', 'b', 'c'}},
	}

	t.Run("Wrong Version", func(t *testing.T) {
		d := tmpl
		d.Version = 0
		err := d.Validate()
		require.Error(t, err)
	})
	t.Run("Valid", func(t *testing.T) {
		err := tmpl.Validate()
		require.NoError(t, err)
	})
}

func TestGetPkgURL(t *testing.T) {
	example := "https://example.org/dir/pkgs/foo.json"

	for _, tt := range []struct {
		base string // Empty for no base
		url  string
		want string // Empty if error is expected
	}{
		{"", "http://example.org/dir/foo.zip", "http://example.org/dir/foo.zip"},
		{example, "http://elsewhere.org/foo.zip", "http://elsewhere.org/foo.zip"},
		{example, "//elsewhere.org/foo.zip", "https://elsewhere.org/foo.zip"},
		{example, "/foo.zip", "https://example.org/foo.zip"},
		{example, "foo.zip", "https://example.org/dir/pkgs/foo.zip"},
		{example, "./foo.zip", "https://example.org/dir/pkgs/foo.zip"},
		{example, "../foo.zip", "https://example.org/dir/foo.zip"},
		{"", "", ""},
		{"", "/foo.zip", ""},
		{"", ":foo.zip", ""},
		{example, "gopher:foo.zip", ""},
		{"", "gopher:foo.zip", ""},
	} {
		var base *url.URL
		if tt.base != "" {
			var err error
			base, err = url.Parse(tt.base)
			if err != nil {
				t.Fatal(err)
			}
		}
		descriptor := Descriptor{PkgURL: tt.url}
		url, err := descriptor.GetPkgURL(base)
		if tt.want == "" {
			if err == nil {
				t.Errorf("unexpected GetPkgURL success for %q, %q", tt.base, tt.url)
			}
		} else if err != nil {
			t.Errorf("unexpected GetPkgURL failure for %q, %q: %s", tt.base, tt.url, err)
		} else if got := url.String(); got != tt.want {
			t.Errorf("unexpected GetPkgURL result for %q, %q: got %q, want %q ", tt.base, tt.url, got, tt.want)
		}
	}
}
