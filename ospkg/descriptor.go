// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package ospkg

import (
	"bytes"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"net/url"

	"system-transparency.org/stboot/stlog"
)

const (
	DescriptorVersion int = 1
	// DescriptorExt is the file extension of OS package descriptor file.
	DescriptorExt string = ".json"
)

// Descriptor represents the descriptor JSON file of an OS package.
type Descriptor struct {
	Version int    `json:"version"`
	PkgURL  string `json:"os_pkg_url"`

	Certificates [][]byte `json:"certificates"`
	Signatures   [][]byte `json:"signatures"`
}

// DescriptorFromBytes parses a manifest from a byte slice.
func DescriptorFromBytes(data []byte) (*Descriptor, error) {
	var d Descriptor
	if err := json.Unmarshal(data, &d); err != nil {
		return nil, fmt.Errorf("parsing descriptor failed: %w", err)
	}

	return &d, nil
}

// Bytes serializes a manifest stuct into a byte slice.
func (d *Descriptor) Bytes() ([]byte, error) {
	buf, err := json.MarshalIndent(d, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("serializing descriptor failed: %w", err)
	}

	return buf, nil
}

// Validate returns true if d has valid content.
func (d *Descriptor) Validate() error {
	// Version
	if d.Version != DescriptorVersion {
		stlog.Debug("descriptor: invalid version %d. Want %d", d.Version, DescriptorVersion)

		return fmt.Errorf("invalid descriptor version: %d, expected %d", d.Version, DescriptorVersion)
	}

	return nil
}

func (d *Descriptor) GetPkgURL(base *url.URL) (*url.URL, error) {
	if d.PkgURL == "" {
		return nil, fmt.Errorf("no OS package URL provided in descriptor")
	}

	url, err := url.Parse(d.PkgURL)
	if err != nil {
		return nil, err
	}
	if base != nil {
		url = base.ResolveReference(url)
	}
	if s := url.Scheme; s != "http" && s != "https" {
		return nil, fmt.Errorf("unsupported scheme in pkg url: %q", s)
	}
	return url, nil
}

func (d *Descriptor) AddSignature(certDER []byte, sig []byte) error {
	cert, err := x509.ParseCertificate(certDER)
	if err != nil {
		return fmt.Errorf("invalid x509 certificate: %w", err)
	}
	// Check for duplicates.
	for _, pemBytes := range d.Certificates {
		storedCert, err := parseCert(pemBytes)

		if err != nil {
			return fmt.Errorf("invalid stored x509 certificate: %w", err)
		}

		if bytes.Equal(storedCert.RawSubjectPublicKeyInfo, cert.RawSubjectPublicKeyInfo) {
			return fmt.Errorf("certified key has already been used")
		}
	}
	d.Certificates = append(d.Certificates, pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER}))
	d.Signatures = append(d.Signatures, sig)

	return nil
}
