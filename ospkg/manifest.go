// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package ospkg

import (
	"encoding/json"
	"fmt"

	"system-transparency.org/stboot/stlog"
)

const (
	ManifestVersion int = 1
	// ManifestName is the name of OS packages' internal configuration file.
	ManifestName string = "manifest.json"
)

// OSManifest describes the content and configuration of an OS package
// loaded by stboot.
type OSManifest struct {
	Version int    `json:"version"`
	Label   string `json:"label"`

	KernelPath    string `json:"kernel"`
	InitramfsPath string `json:"initramfs"`
	Cmdline       string `json:"cmdline"`
}

// OSManifestFromBytes parses a manifest from a byte slice.
func OSManifestFromBytes(data []byte) (*OSManifest, error) {
	var m OSManifest
	if err := json.Unmarshal(data, &m); err != nil {
		return nil, fmt.Errorf("parsing manifest failed: %w", err)
	}

	return &m, nil
}

// Bytes serializes a manifest stuct into a byte slice.
func (m *OSManifest) Bytes() ([]byte, error) {
	buf, err := json.MarshalIndent(m, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("serializing manifest failed: %w", err)
	}

	return buf, nil
}

// Validate returns no.
func (m *OSManifest) Validate() error {
	// Version
	if m.Version != ManifestVersion {
		stlog.Debug("manifest: invalid version %d. Want %d", m.Version, ManifestVersion)

		return fmt.Errorf("invalid manifest version: %d, expected %d", m.Version, ManifestVersion)
	}
	// Kernel path is mandatory
	if m.KernelPath == "" {
		stlog.Debug("manifest: missing kernel path")

		return fmt.Errorf("missing %v path", "kernel")
	}
	// Initramfs path is mandatory
	if m.InitramfsPath == "" {
		stlog.Debug("manifest: missing initramfs path")

		return fmt.Errorf("missing %v path", "initramfs")
	}

	return nil
}
