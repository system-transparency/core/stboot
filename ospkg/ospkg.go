// Copyright 2021 the System Transparency Authors. All rights reserved
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package ospkg

import (
	"archive/zip"
	"bytes"
	"context"
	"crypto"
	"crypto/ed25519"
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/fs"
	"net/url"
	"path/filepath"
	"time"

	"github.com/u-root/u-root/pkg/boot"
	di "system-transparency.org/stboot/internal/dependency"
	"system-transparency.org/stboot/stlog"
)

const (
	// DefaultOSPackageName is the file name of the archive, which is expected to contain
	// the stboot configuration file along with the corresponding files.
	DefaultOSPackageName string = "ospkg.zip"
	// OSPackageExt is the file extension of OS packages.
	OSPackageExt string = ".zip"

	bootfilesDir string = "boot"
)

// OSPackage represents an OS package ZIP archive and related data.
type OSPackage struct {
	raw        []byte
	descriptor *Descriptor
	hash       [32]byte
	manifest   *OSManifest
	kernel     []byte
	initramfs  []byte
	isVerified bool
}

// CreateOSPackage constructs a OSPackage from the passed files.
func CreateOSPackage(label, pkgURL, kernel, initramfs, cmdline string) (*OSPackage, error) {
	return CreateOSPackageCtx(context.Background(), label, pkgURL, kernel, initramfs, cmdline)
}

func CreateOSPackageCtx(ctx context.Context, label, pkgURL, kernel, initramfs, cmdline string) (*OSPackage, error) {
	var manifest = &OSManifest{
		Version: ManifestVersion,
		Label:   label,
		Cmdline: cmdline,
	}

	var descriptor = &Descriptor{
		Version: DescriptorVersion,
	}

	var osp = &OSPackage{
		descriptor: descriptor,
		manifest:   manifest,
		isVerified: false,
	}

	fsys := di.DefaultFilesystem(ctx)

	var err error
	if pkgURL != "" {
		uri, err := url.Parse(pkgURL)
		if err != nil {
			return nil, fmt.Errorf("invalid package URL %q, %w", pkgURL, err)
		}

		if uri.IsAbs() && (uri.Scheme != "http" && uri.Scheme != "https") {
			stlog.Debug("os package: OS package URL: unsupported scheme in %s", pkgURL)

			return nil, fmt.Errorf("invalid package URL %q, unsupported scheme", pkgURL)
		}

		osp.descriptor.PkgURL = pkgURL
	}

	if kernel != "" {
		osp.kernel, err = fs.ReadFile(fsys, kernel)
		if err != nil {
			return nil, fmt.Errorf("failed to read kernel from %q: %w", kernel, err)
		}

		osp.manifest.KernelPath = filepath.Join(bootfilesDir, filepath.Base(kernel))
	}

	if initramfs != "" {
		osp.initramfs, err = fs.ReadFile(fsys, initramfs)
		if err != nil {
			return nil, fmt.Errorf("failed to read initramfs from %q: %w", initramfs, err)
		}

		osp.manifest.InitramfsPath = filepath.Join(bootfilesDir, filepath.Base(initramfs))
	}

	if err := osp.validate(); err != nil {
		return nil, fmt.Errorf("OS package failed to validate: %w", err)
	}

	return osp, nil
}

// NewOSPackage constructs a new OSPackage initialized with raw bytes
// and valid internal state.
func NewOSPackage(archiveZIP, descriptorJSON []byte) (*OSPackage, error) {
	// check descriptor
	descriptor, err := DescriptorFromBytes(descriptorJSON)
	if err != nil {
		return nil, fmt.Errorf("invalid descriptor data: %w", err)
	}

	if err = descriptor.Validate(); err != nil {
		return nil, fmt.Errorf("invalid descriptor contents: %w", err)
	}
	return NewOSPackageWithDescriptor(descriptor, archiveZIP)
}

func NewOSPackageWithDescriptor(descriptor *Descriptor, archiveZIP []byte) (*OSPackage, error) {
	// check archive
	_, err := zip.NewReader(bytes.NewReader(archiveZIP), int64(len(archiveZIP)))
	if err != nil {
		return nil, fmt.Errorf("invalid zip data: %w", err)
	}

	osp := OSPackage{
		raw:        archiveZIP,
		descriptor: descriptor,
		isVerified: false,
	}

	osp.hash, err = calculateHash(osp.raw)
	if err != nil {
		return nil, err
	}

	return &osp, nil
}

func (osp *OSPackage) ArchiveHash() [32]byte {
	return osp.hash
}

func (osp *OSPackage) validate() error {
	// manifest
	if osp.manifest == nil {
		stlog.Debug("missing manifest data")

		return fmt.Errorf("missing manifest")
	} else if err := osp.manifest.Validate(); err != nil {
		return err
	}
	// descriptor
	if osp.descriptor == nil {
		stlog.Debug("missing descriptor data")

		return fmt.Errorf("missing descriptor")
	} else if err := osp.descriptor.Validate(); err != nil {
		return err
	}
	// kernel is mandatory
	if len(osp.kernel) == 0 {
		stlog.Debug("missing kernel")

		return fmt.Errorf("missing kernel")
	}
	// initrmafs is mandatory
	if len(osp.initramfs) == 0 {
		stlog.Debug("missing initramfs")

		return fmt.Errorf("missing initramfs")
	}

	return nil
}

// ArchiveBytes returns the zip compressed archive part of osp.
func (osp *OSPackage) ArchiveBytes() ([]byte, error) {
	if len(osp.raw) == 0 {
		if err := osp.zip(); err != nil {
			return nil, fmt.Errorf("zipping failed: %w", err)
		}
	}

	return osp.raw, nil
}

func (osp *OSPackage) Descriptor() *Descriptor {
	return osp.descriptor
}

// DescriptorBytes returns the descriptor part of osp as serialized bytes.
func (osp *OSPackage) DescriptorBytes() ([]byte, error) {
	b, err := osp.descriptor.Bytes()
	if err != nil {
		return nil, err
	}

	return b, nil
}

// zip packs the content stored in osp and replaces osp.raw.
func (osp *OSPackage) zip() error {
	buf := new(bytes.Buffer)
	zipWriter := zip.NewWriter(buf)

	// directories
	if err := zipDir(zipWriter, bootfilesDir); err != nil {
		return fmt.Errorf("failed to add zip directory %q: %w", bootfilesDir, err)
	}
	// kernel
	name := osp.manifest.KernelPath
	if err := zipFile(zipWriter, name, osp.kernel); err != nil {
		return fmt.Errorf("failed to add kernel zip file %q: %w", name, err)
	}
	// initramfs
	if len(osp.initramfs) > 0 {
		name = osp.manifest.InitramfsPath
		if err := zipFile(zipWriter, name, osp.initramfs); err != nil {
			return fmt.Errorf("failed to add initramfs zip file %q: %w", name, err)
		}
	}
	// manifest
	mbytes, err := osp.manifest.Bytes()
	if err != nil {
		return err
	}

	if err := zipFile(zipWriter, ManifestName, mbytes); err != nil {
		return fmt.Errorf("failed to add manifest zip file %q: %w", ManifestName, err)
	}

	if err := zipWriter.Close(); err != nil {
		return fmt.Errorf("closing zip file failed: %w", err)
	}

	osp.raw = buf.Bytes()

	return nil
}

func (osp *OSPackage) unzip() error {
	if len(osp.raw) == 0 {
		return fmt.Errorf("missing raw")
	}

	reader := bytes.NewReader(osp.raw)
	size := int64(len(osp.raw))

	archive, err := zip.NewReader(reader, size)
	if err != nil {
		return fmt.Errorf("unzip failed: %w", err)
	}
	// manifest
	m, err := unzipFile(archive, ManifestName)
	if err != nil {
		return fmt.Errorf("unzipping manifest failed: %w", err)
	}

	osp.manifest, err = OSManifestFromBytes(m)
	if err != nil {
		return fmt.Errorf("invalid manifest: %w", err)
	}
	// kernel
	osp.kernel, err = unzipFile(archive, osp.manifest.KernelPath)
	if err != nil {
		return fmt.Errorf("unzipping kernel failed: %w", err)
	}
	// initramfs
	osp.initramfs, err = unzipFile(archive, osp.manifest.InitramfsPath)
	if err != nil {
		return fmt.Errorf("unzipping initramfs failed: %w", err)
	}

	osp.raw = nil

	return nil
}

// Signs osp.HashValue.
// Both the signature and the certificate are stored into the OSPackage.
func (osp *OSPackage) Sign(signer crypto.Signer, certDER []byte) error {
	hash, err := calculateHash(osp.raw)
	if err != nil {
		return err
	}

	osp.hash = hash

	sig, err := signer.Sign(nil, osp.hash[:], crypto.Hash(0))
	if err != nil {
		return err
	}

	return osp.descriptor.AddSignature(certDER, sig)
}

// Verify determines the number of unique certificates that chain up to (or is
// the) root certificate and which also produced valid OS package signatures.
func (osp *OSPackage) Verify(rootCert *x509.Certificate, now time.Time) (found, valid int, err error) {
	found = 0
	valid = 0

	certsUsed := make([]*x509.Certificate, 0, len(osp.descriptor.Signatures))

	for iter, sig := range osp.descriptor.Signatures {
		found++

		cert, err := parseCert(osp.descriptor.Certificates[iter])

		if err != nil {
			return 0, 0, fmt.Errorf("could not parse cert %d: %w", iter+1, err)
		}

		roots := x509.NewCertPool()
		roots.AddCert(rootCert)

		opts := x509.VerifyOptions{
			Roots:       roots,
			CurrentTime: now,
		}

		if _, err = cert.Verify(opts); err != nil {
			stlog.Debug("skip signature %d: invalid certificate: %v", iter+1, err)

			continue
		}
		if now.Add(7 * 24 * time.Hour).After(cert.NotAfter) {
			stlog.Warn("cert #%d expires in less than a week, at: %s", iter+1, cert.NotAfter)
		} else {
			stlog.Info("cert #%d expires at: %s", iter+1, cert.NotAfter)
		}

		var duplicate bool

		for _, c := range certsUsed {
			if bytes.Equal(c.RawSubjectPublicKeyInfo, cert.RawSubjectPublicKeyInfo) {
				duplicate = true

				break
			}
		}

		// Note that we don't have to take into consideration if the signature
		// scheme is malleable or not.  The duplicate check is based on the
		// signed leaf certificate's public key, not the signature itself.  That
		// the public key is signed by (or is) a trust anchor also ensures that
		// an attacker cannot tweak its representation for double counting.
		if duplicate {
			stlog.Debug("skip signature %d: duplicate", iter+1)

			continue
		}

		certsUsed = append(certsUsed, cert)

		publicKey, ok := cert.PublicKey.(ed25519.PublicKey)
		if !ok {
			stlog.Debug("invalid public key type: %T, skip signature %d", cert.PublicKey, iter+1)
			continue
		}
		if !ed25519.Verify(publicKey, osp.hash[:], sig) {
			stlog.Debug("skip signature %d: verification failed", iter+1)
			continue
		}
		valid++
	}

	osp.isVerified = true

	return found, valid, nil
}

// OSImage returns a LinuxImage from osp. LinuxImage implements boot.
func (osp *OSPackage) LinuxImage() (boot.LinuxImage, error) {
	if !osp.isVerified {
		stlog.Debug("os package: content not verified")

		return boot.LinuxImage{}, fmt.Errorf("content is not verified")
	}

	if err := osp.unzip(); err != nil {
		return boot.LinuxImage{}, err
	}

	if err := osp.validate(); err != nil {
		return boot.LinuxImage{}, err
	}

	// linuxboot image
	return boot.LinuxImage{
		Name:    osp.manifest.Label,
		Kernel:  bytes.NewReader(osp.kernel),
		Initrd:  bytes.NewReader(osp.initramfs),
		Cmdline: osp.manifest.Cmdline,
	}, nil
}

func parseCert(certData []byte) (*x509.Certificate, error) {
	var block *pem.Block

	block, _ = pem.Decode(certData)

	if block == nil {
		return nil, fmt.Errorf("invalid certificate PEM file")
	}

	if block.Type != "CERTIFICATE" || len(block.Headers) != 0 {
		return nil, fmt.Errorf("encoded data is not a certificate, got: %q", block.Type)
	}

	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("invalid certificate: %w", err)
	}

	return cert, nil
}

func calculateHash(data []byte) ([32]byte, error) {
	if len(data) == 0 {
		return [32]byte{}, fmt.Errorf("empty data")
	}

	return sha256.Sum256(data), nil
}
