package ospkg

import (
	"archive/zip"
	"bytes"
	"context"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"strings"
	"testing"
	"testing/fstest"
	"time"

	"github.com/stretchr/testify/require"

	"system-transparency.org/stboot/stlog"

	di "system-transparency.org/stboot/internal/dependency"
	"system-transparency.org/stboot/internal/test"
)

func setup(t *testing.T) *OSPackage {
	t.Helper()

	mockFsys := test.NewMockFS()
	err := mockFsys.Add("/vmlinuz", &fstest.MapFile{Data: []byte("i'm a kernel"), Mode: 0644})
	require.NoError(t, err)
	err = mockFsys.Add("/initramfs", &fstest.MapFile{Data: []byte("i'm an initial ram fs"), Mode: 0644})
	require.NoError(t, err)

	ctx := context.Background()
	ctx = di.WithFilesystem(ctx, mockFsys)

	osp, err := CreateOSPackageCtx(
		ctx,
		"test-label",
		"https://example.com/ospkg.zip",
		"/vmlinuz",
		"/initramfs",
		"console=ttyS0,115200n8",
	)
	require.NoError(t, err)

	return osp
}

func TestCreateOsPkg(t *testing.T) {
	osp := setup(t)

	descbuf, err := osp.DescriptorBytes()
	require.NoError(t, err)
	archivebuf, err := osp.ArchiveBytes()
	require.NoError(t, err)

	osp, err = NewOSPackage(archivebuf, descbuf)
	require.NoError(t, err)

	_, _, err = osp.Verify(nil, time.Now())
	require.NoError(t, err)

	img, err := osp.LinuxImage()
	require.NoError(t, err)

	kernel := make([]byte, 12)
	_, err = img.Kernel.ReadAt(kernel, 0)
	require.NoError(t, err)
	require.Equal(t, []byte("i'm a kernel"), kernel)

	initramfs := make([]byte, 21)
	_, err = img.Initrd.ReadAt(initramfs, 0)
	require.NoError(t, err)
	require.Equal(t, []byte("i'm an initial ram fs"), initramfs)

	require.Equal(t, "console=ttyS0,115200n8", img.Cmdline)
}

func TestCreateOsPkgErrors(t *testing.T) {
	mockFsys := test.NewMockFS()
	err := mockFsys.Add("/vmlinuz", &fstest.MapFile{Data: []byte("i'm a kernel"), Mode: 0644})
	require.NoError(t, err)
	err = mockFsys.Add("/initramfs", &fstest.MapFile{Data: []byte("i'm an initial ram fs"), Mode: 0644})
	require.NoError(t, err)
	err = mockFsys.Add("/empty-vmlinuz", &fstest.MapFile{Data: nil, Mode: 0644})
	require.NoError(t, err)
	err = mockFsys.Add("/empty-initramfs", &fstest.MapFile{Data: nil, Mode: 0644})
	require.NoError(t, err)

	ctx := context.Background()
	ctx = di.WithFilesystem(ctx, mockFsys)

	t.Run("Invalid URLs", func(t *testing.T) {
		for _, sch := range []string{"", "file", "blub"} {
			t.Run(sch, func(t *testing.T) {
				_, err := CreateOSPackageCtx(
					ctx,
					"test-label",
					fmt.Sprintf("%s://example.com/ospkg.zip", sch),
					"/vmlinuz",
					"/initramfs",
					"console=ttyS0,115200n8",
				)
				require.Error(t, err)
			})
		}
	})

	t.Run("Invalid kernel", func(t *testing.T) {
		for _, kernel := range []string{"", "/vmlinux", "/empty-vmlinuz"} {
			t.Run(kernel, func(t *testing.T) {
				_, err := CreateOSPackageCtx(
					ctx,
					"test-label",
					"https://example.com/ospkg.zip",
					kernel,
					"/initramfs",
					"console=ttyS0,115200n8",
				)
				require.Error(t, err)
			})
		}
	})

	t.Run("Invalid initramfs", func(t *testing.T) {
		for _, initramfs := range []string{"", ".initramf", ".empty-initramfs"} {
			t.Run(initramfs, func(t *testing.T) {
				_, err := CreateOSPackageCtx(
					ctx,
					"test-label",
					"https://example.com/ospkg.zip",
					".vmlinuz",
					initramfs,
					"console=ttyS0,115200n8",
				)
				require.Error(t, err)
			})
		}
	})
}

func TestNewOSPackage(t *testing.T) {
	osp := setup(t)

	descbuf, err := osp.DescriptorBytes()
	require.NoError(t, err)
	archivebuf, err := osp.ArchiveBytes()
	require.NoError(t, err)

	_, err = NewOSPackage(nil, nil)
	require.Error(t, err)

	_, err = NewOSPackage(archivebuf, nil)
	require.Error(t, err)

	_, err = NewOSPackage(nil, descbuf)
	require.Error(t, err)

	_, err = NewOSPackage(archivebuf, []byte(strings.ReplaceAll(string(descbuf), "1", "2")))
	require.Error(t, err)
}

func mkArchive(t *testing.T, manifest, kernel, initramfs []byte) []byte {
	t.Helper()

	buf := new(bytes.Buffer)
	zipWriter := zip.NewWriter(buf)

	if kernel != nil {
		err := zipFile(zipWriter, "vmlinuz", kernel)
		require.NoError(t, err)
	}

	if initramfs != nil {
		err := zipFile(zipWriter, "initramfs", initramfs)
		require.NoError(t, err)
	}

	if manifest != nil {
		err := zipFile(zipWriter, "manifest.json", manifest)
		require.NoError(t, err)
	}

	err := zipWriter.Close()
	require.NoError(t, err)

	return buf.Bytes()
}

func TestOSPackageArchive(t *testing.T) {
	m := OSManifest{
		Version: ManifestVersion,
		Label:   "test",

		KernelPath:    "vmlinuz",
		InitramfsPath: "initramfs",
		Cmdline:       "console=ttyS0,115200n8",
	}
	manifestbuf, err := m.Bytes()
	require.NoError(t, err)

	desc := Descriptor{
		Version: DescriptorVersion,
		PkgURL:  "https://example.com/ospkg.zip",
	}
	descbuf, err := desc.Bytes()
	require.NoError(t, err)

	validate := func(archivebuf []byte) {
		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		_, _, err = osp.Verify(nil, time.Now())
		require.NoError(t, err)

		_, err = osp.LinuxImage()
		require.Error(t, err)
	}

	kernelbuf := []byte("i'm a kernel")
	initramfsbuf := []byte("i'm an initial ram fs")

	t.Run("No initramfs", func(t *testing.T) {
		archivebuf := mkArchive(t, manifestbuf, kernelbuf, nil)
		validate(archivebuf)
	})
	t.Run("No kernel", func(t *testing.T) {
		archivebuf := mkArchive(t, manifestbuf, nil, initramfsbuf)
		validate(archivebuf)
	})
	t.Run("Empty initramfs", func(t *testing.T) {
		archivebuf := mkArchive(t, manifestbuf, kernelbuf, []byte{})
		validate(archivebuf)
	})
	t.Run("Empty kernel", func(t *testing.T) {
		archivebuf := mkArchive(t, manifestbuf, []byte{}, initramfsbuf)
		validate(archivebuf)
	})
	t.Run("No manifest", func(t *testing.T) {
		archivebuf := mkArchive(t, nil, kernelbuf, initramfsbuf)
		validate(archivebuf)
	})
	t.Run("Invalid manifest", func(t *testing.T) {
		archivebuf := mkArchive(t, kernelbuf, kernelbuf, initramfsbuf)
		validate(archivebuf)
	})
}

func TestEnforceValidate(t *testing.T) {
	osp := setup(t)

	descbuf, err := osp.DescriptorBytes()
	require.NoError(t, err)
	archivebuf, err := osp.ArchiveBytes()
	require.NoError(t, err)

	osp, err = NewOSPackage(archivebuf, descbuf)
	require.NoError(t, err)

	_, err = osp.LinuxImage()
	require.Error(t, err)

	_, _, err = osp.Verify(nil, time.Now())
	require.NoError(t, err)

	_, err = osp.LinuxImage()
	require.NoError(t, err)
}

func TestSigning(t *testing.T) {
	osp := setup(t)
	_, err := osp.ArchiveBytes()
	require.NoError(t, err)

	pub, priv, err := ed25519.GenerateKey(rand.Reader)
	require.NoError(t, err)

	tmpl := x509.Certificate{
		SignatureAlgorithm: x509.PureEd25519,
		SerialNumber:       big.NewInt(1),
		PublicKey:          pub,
		Subject:            pkix.Name{CommonName: "test"},
		NotAfter:           time.Now().Add(24 * time.Hour),
		NotBefore:          time.Now(),
	}

	certDER, err := x509.CreateCertificate(rand.Reader, &tmpl, &tmpl, pub, priv)
	require.NoError(t, err)
	cert, err := x509.ParseCertificate(certDER)
	require.NoError(t, err)

	t.Run("Successful signing", func(t *testing.T) {
		err = osp.Sign(priv, certDER)
		require.NoError(t, err)
		// Check that signature is recognized.
		found, valid, err := osp.Verify(cert, time.Now())
		require.NoError(t, err)
		if found != 1 {
			t.Errorf("Expected 1 signature, got %d", found)
		}
		if valid != 1 {
			t.Errorf("Expected 1 valid signature, got %d", valid)
		}
	})

	t.Run("Reuse cert", func(t *testing.T) {
		// reuse same cert
		err = osp.Sign(priv, certDER)
		require.Error(t, err)
		osp = setup(t)
		_, err = osp.ArchiveBytes()
		require.NoError(t, err)
	})

	t.Run("Invalid cert", func(t *testing.T) {
		err := osp.Sign(priv, certDER[1:])
		require.Error(t, err)
	})
}

func TestVerification(t *testing.T) {
	osp := setup(t)

	descbuf, err := osp.DescriptorBytes()
	require.NoError(t, err)

	archivebuf, err := osp.ArchiveBytes()
	require.NoError(t, err)

	archivehash := sha256.Sum256(archivebuf)

	osp, err = NewOSPackage(archivebuf, descbuf)
	require.NoError(t, err)

	root1 := test.MkKey(t, nil)
	root2 := test.MkKey(t, nil)

	keys := make([]test.Key, 0, 6)
	for i := 0; i < 3; i++ {
		keys = append(keys, test.MkKey(t, &root1))
	}

	for i := 0; i < 3; i++ {
		keys = append(keys, test.MkKey(t, &root2))
	}

	t.Run("No signatures", func(t *testing.T) {
		num, good, err := osp.Verify(nil, time.Now())
		require.NoError(t, err)
		require.Equal(t, 0, num)
		require.Equal(t, 0, good)
	})

	f := func(t *testing.T, key test.Key) {
		t.Helper()
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = [][]byte{pem.EncodeToMemory(key.Certpem)}
		desc.Signatures = [][]byte{ed25519.Sign(key.Private, archivehash[:])}
		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(key.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 1, num)
		require.Equal(t, 1, good)
	}
	t.Run("Sign OS package directly with root CA key", func(t *testing.T) { f(t, root1) })
	t.Run("Sign OS package directly with non-CA root key", func(t *testing.T) { f(t, keys[0]) })

	t.Run("Single signature", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = [][]byte{pem.EncodeToMemory(keys[0].Certpem)}
		desc.Signatures = [][]byte{ed25519.Sign(keys[0].Private, archivehash[:])}
		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 1, num)
		require.Equal(t, 1, good)
	})

	t.Run("3 of 3 signatures", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = nil
		desc.Signatures = nil

		for i := 0; i < 3; i++ {
			desc.Certificates = append(desc.Certificates, pem.EncodeToMemory(keys[i].Certpem))
			desc.Signatures = append(desc.Signatures, ed25519.Sign(keys[i].Private, archivehash[:]))
		}

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 3, num)
		require.Equal(t, 3, good)
	})

	t.Run("one dup signature", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		// Two distinct keys and signatures
		desc.Signatures = [][]byte{ed25519.Sign(keys[0].Private, archivehash[:]), ed25519.Sign(keys[1].Private, archivehash[:])}
		desc.Certificates = [][]byte{pem.EncodeToMemory(keys[0].Certpem), pem.EncodeToMemory(keys[1].Certpem)}

		// Duplicate signature from the first key with a different certificate
		desc.Signatures = append(desc.Signatures, desc.Signatures[0])
		tmpl := x509.Certificate{
			SignatureAlgorithm: x509.PureEd25519,
			SerialNumber:       big.NewInt(123),
			PublicKey:          keys[0].Public,
			NotBefore:          time.Now().Add(-123 * time.Hour),
			NotAfter:           time.Now().Add(123 * time.Minute),
		}
		crt, err := x509.CreateCertificate(rand.Reader, &tmpl, root1.Cert, keys[0].Public, root1.Private)
		if err != nil {
			t.Fatal(err)
		}
		crtPEM := &pem.Block{Type: "CERTIFICATE", Bytes: crt}
		desc.Certificates = append(desc.Certificates, pem.EncodeToMemory(crtPEM))

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 3, num)
		require.Equal(t, 2, good)
	})

	t.Run("non ca'd signature", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = nil
		desc.Signatures = nil

		for i := 0; i < 6; i++ {
			desc.Certificates = append(desc.Certificates, pem.EncodeToMemory(keys[i].Certpem))
			desc.Signatures = append(desc.Signatures, ed25519.Sign(keys[i].Private, archivehash[:]))
		}

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 6, num)
		require.Equal(t, 3, good)
	})

	t.Run("invalid signatures", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = nil
		desc.Signatures = nil

		for i := 0; i < 3; i++ {
			desc.Certificates = append(desc.Certificates, pem.EncodeToMemory(keys[i].Certpem))
			desc.Signatures = append(desc.Signatures, ed25519.Sign(keys[i].Private, archivehash[:]))
		}
		desc.Signatures[0][0] ^= 1

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 3, num)
		require.Equal(t, 2, good)
	})

	t.Run("invalid certifiacte", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = nil
		desc.Signatures = nil

		for i := 0; i < 2; i++ {
			desc.Certificates = append(desc.Certificates, pem.EncodeToMemory(keys[i].Certpem))
			desc.Signatures = append(desc.Signatures, ed25519.Sign(keys[i].Private, archivehash[:]))
		}
		desc.Certificates[0] = pem.EncodeToMemory(keys[2].Certpem)

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 2, num)
		require.Equal(t, 1, good)
	})

	t.Run("Check that expired certificate is rejected", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		tmpl := x509.Certificate{
			SignatureAlgorithm: x509.PureEd25519,
			SerialNumber:       big.NewInt(1),
			PublicKey:          keys[0].Public,
			NotBefore:          time.Now().Add(-1 * time.Hour),
			NotAfter:           time.Now().Add(-1 * time.Minute),
		}
		crt, err := x509.CreateCertificate(rand.Reader, &tmpl, root1.Cert, keys[0].Public, root1.Private)
		if err != nil {
			t.Fatal(err)
		}
		crtPEM := &pem.Block{Type: "CERTIFICATE", Bytes: crt}

		desc.Certificates = [][]byte{pem.EncodeToMemory(crtPEM)}
		desc.Signatures = [][]byte{ed25519.Sign(keys[0].Private, archivehash[:])}
		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		num, good, err := osp.Verify(root1.Cert, time.Now())
		require.NoError(t, err)
		require.Equal(t, 1, num)
		require.Equal(t, 0, good)
	})
}

func TestCertParsing(t *testing.T) {
	osp := setup(t)

	descbuf, err := osp.DescriptorBytes()
	require.NoError(t, err)

	archivebuf, err := osp.ArchiveBytes()
	require.NoError(t, err)

	archivehash := sha256.Sum256(archivebuf)

	root := test.MkKey(t, nil)

	var keys []test.Key
	for i := 0; i < 3; i++ {
		keys = append(keys, test.MkKey(t, &root))
	}

	t.Run("no pem block", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		desc.Certificates = [][]byte{pem.EncodeToMemory(keys[0].Certpem)}
		desc.Signatures = [][]byte{ed25519.Sign(keys[0].Private, archivehash[:])}
		desc.Certificates[0][20] = ' '

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		_, _, err = osp.Verify(root.Cert, time.Now())
		require.Error(t, err)
	})

	t.Run("no cert type", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		pemcert := keys[0].Certpem
		pemcert.Type = "CERTIFICAT"

		desc.Certificates = [][]byte{pem.EncodeToMemory(pemcert)}
		desc.Signatures = [][]byte{ed25519.Sign(keys[0].Private, archivehash[:])}

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		_, _, err = osp.Verify(root.Cert, time.Now())
		require.Error(t, err)
	})

	t.Run("not a cert", func(t *testing.T) {
		desc, err := DescriptorFromBytes(descbuf)
		require.NoError(t, err)

		pemcert := keys[0].Certpem
		pemcert.Type = "CERTIFICATE"
		pemcert.Bytes = []byte("blub")

		desc.Certificates = [][]byte{pem.EncodeToMemory(pemcert)}
		desc.Signatures = [][]byte{ed25519.Sign(keys[0].Private, archivehash[:])}

		descbuf, err := desc.Bytes()
		require.NoError(t, err)

		osp, err := NewOSPackage(archivebuf, descbuf)
		require.NoError(t, err)

		_, _, err = osp.Verify(root.Cert, time.Now())
		require.Error(t, err)
	})
}

func TestCertLogging(t *testing.T) {
	buf := divertLog(t, stlog.InfoLevel)

	osp := setup(t)
	_, err := osp.ArchiveBytes()
	require.NoError(t, err)

	now := time.Now()
	root := test.MkKeyWithExpiry(t, nil, now, now.Add(30*24*time.Hour))

	keys := []test.Key{
		test.MkKeyWithExpiry(t, &root, now, now.Add(10*time.Minute)),
		test.MkKeyWithExpiry(t, &root, now, now.Add(2*24*time.Hour)),
		test.MkKeyWithExpiry(t, &root, now, now.Add(30*24*time.Hour)),
	}
	for _, key := range keys {
		err := osp.Sign(key.Private, key.Certpem.Bytes)
		require.NoError(t, err)
	}
	num, good, err := osp.Verify(root.Cert, now.Add(24*time.Hour))
	require.NoError(t, err)
	require.Equal(t, 3, num)
	require.Equal(t, 2, good)

	// Check expectations on logged messages.
	expectedMsgs := []string{
		"[WARN] cert #2 expires in less than a week, at:",
		"[INFO] cert #3 expires at:",
	}

	logLines := strings.Split(buf.String(), "\n")
	if len(logLines) == 0 || logLines[len(logLines)-1] != "" {
		t.Fatalf("failed to split log into proper lines, got %v", logLines)
	}
	logLines = logLines[:len(logLines)-1]
	for i, line := range logLines {
		if i < len(expectedMsgs) && !strings.Contains(line, expectedMsgs[i]) {
			t.Errorf("missing message %q in log line %d: %q",
				expectedMsgs[i], i, line)
		}
		if i >= len(expectedMsgs) {
			t.Errorf("unexpected message in log line %d: %q", i, line)
		}
	}
}

// Divert logging to a buffer, for the duration of current test.
func divertLog(t *testing.T, level stlog.LogLevel) *bytes.Buffer {
	oldWriter := log.Writer()
	oldLevel := stlog.Level()
	buf := bytes.Buffer{}
	log.SetOutput(&buf)
	stlog.SetLevel(level)
	t.Cleanup(func() {
		log.SetOutput(oldWriter)
		stlog.SetLevel(oldLevel)
	})
	return &buf
}
